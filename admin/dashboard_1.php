<?php
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;
?>
<?php require_once('../Connections/air2013.php');
ini_set('max_execution_time',0);
ini_set('memory_limit','-1');
 ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;



$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;


if(isset($_GET["fromdate"]))
{
	
	$adwhere="update_time between '". $fromdate . "' AND '". $todate . "' AND";
}
else
{
	
	$adwhere="";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multichoice SCRIPT</title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="jquery2/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="jquery2/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery2/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="jquery2/js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>

<!--<script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="jquery2/js/jquery.highchartTable.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart')
  .bind('highchartTable.beforeRender', function (event, highChartConfig) {
        console.log(highChartConfig);

        highChartConfig.chart.events = {
            load: function () {
                var data = this.series[0].data,
                    dLen = data.length,
                    i = 0;

                while (dLen > i) {
                    var point = data[i];
                    if (point.sliced) {
                        $report.html('<h2>' + point.name + '</h2><p>' + point.percentage.toFixed(1) + '%</p>');
                        i = dLen;
                    }
                    i++;
                }

            }
        }

        highChartConfig.plotOptions = {
            pie: {
                cursor: 'pointer',
                size: '60%', // size of pieChart
                stickyTracking: false, // if false, events per slice/obj
                innerSize: '35%', // innerSize dount ring
                //allowPointSelect: true,
                dataLabels: {
                    distance: -50
                },
                center: ['35%', '50%'],
                showInLegend: true,
                slicedOffset: 15,
                shadow: 0,
                point: {
                    events: {
                        mouseOver: function (event) {
                            $report.html('<h2>' + this.name + '</h2><p>' + this.percentage.toFixed(1) + '%</p>');
                            this.select(true);
                        },
                        mouseOut: function (event) {
                            this.select(false);
                        },
                        legendItemClick: function (event) {
                            this.select(null);
                            $report.html('<h2>' + this.name + '</h2><p>' + this.percentage.toFixed(1) + '%</p>');

                            return false;
                        }
                    }
                }
            }
        },
        highChartConfig.tooltip = {
            enabled: true
        },
        highChartConfig.legend = {
            cursor: 'pointer',
            floating: true,
           
            borderColor: null,
            verticalAlign: 'top',
            x: 0,
            y: 20,
            width:400,
            itemWidth: 180
        };
    })
  .highchartTable();

});


</script>-->
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	
            <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='dashboard.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' required value="<?php echo $fromdt; ?>" type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' required value="<?php echo $todt;?>" type='textbox' class="dateselection" id="todt" size='20'/></td>
                    
                    <td align="center">Days:</td>
                    <td align="center">
                    
                    <select name="mdays">
                     <option value="<?php echo $_GET['mdays'];?>"><?php echo $_GET['mdays'];?></option>
                    <option value="30">30 days</option>
                    <option value="60">60 Days</option>
                    <option value="90">90 Days</option>
                   
                    </select>
                    
                    </td>
                    <td align="center">Inactive_with_Credit</td>
                    <td align="center">
                    
                   <select name="inactive"  style="width:100px">
                     <option value="<?php echo $_GET['inactive'];?>"><?php echo $_GET['inactive'];?></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                   
					 <option value="">All</option>
                   
                    </select>
                    
                    </td>
                    <td align="center"><input type="submit" name="submit" value="GET DSTV DASHBOARD REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
           
            </td>
     	</tr>
  	</table>
</div>
<br>
<form>
<?php include "menu2.php";?>
</form>
<?php if(isset($_GET["submit"])){?>
   <div class="content">
 

	<div style="float:left; width:40%; margin-left:10px;margin-right:10px;padding:10px">
	<!-- <form action="getCSVDisp.php" method ="post" > 
				<input type="hidden" name="csv_text" id="csv_text">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>-->
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
<table id="csvdownload">
<tr><td>	
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Contact result - DSTV </caption><br />
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">Total  Contacted</td>
   
  <td class="tblRD" align="center">
	
	
<?php /*?>	<?php 
	
	if(isset($_GET['mdays']) && !empty($_GET['mdays']))
	{
		$dy=$_GET['mdays'];
		if($dy==30)
			$mqr='AND leads.No_Days_Disconnected>0 AND leads.No_Days_Disconnected<='.$dy.'';
		
			if($dy==60)
				$mqr='AND leads.No_Days_Disconnected>30 AND leads.No_Days_Disconnected<='.$dy.'';
			
				if($dy==90)
					$mqr='AND leads.No_Days_Disconnected>60 AND leads.No_Days_Disconnected<='.$dy.'';
	}
	else 
	{
		$mqr='';
	}
	
	if(isset($_GET['inactive']) && !empty($_GET['inactive']))
	{
		if($_GET['inactive']=='Yes')
		{
			//check cleints with negative balance
			$inactive="AND (leads.Outstanding_Balance>0 OR leads.Outstanding_Balance<0)";
		}
		else if($_GET['inactive']=='No')
		{
			//check clinets with 0 or less
			$inactive="AND (leads.Outstanding_Balance='0' OR leads.Outstanding_Balance='')";
		}
			else{
				$inactive="";
			}
	}
	else
	{
		$inactive="";
	}
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE 
	$adwhere (disposation='Language Barrier' 
	OR disposation='Call Back' 
	OR disposation='Complete Survey' 
	OR disposation='Customer Hanged Up' 
	OR disposation='Already Contacted' 
	OR disposation='Not Interested' 
	OR disposation='Partial Survey' 
	OR disposation='Phone busy' 
	OR disposation='Voice mail' 
	OR disposation='Number out of Service' 
	OR disposation='Switched Off' 
	OR disposation='No answer' 
	OR disposation='Language Barrier' 
	OR disposation='Complete Call' 
	OR disposation='Contact details are not for the right customer' 
	OR disposation='Decision-maker was not available'  
	OR disposation='Customer Hanged Up')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $allcalls = $row_called['TotalCount'];
		?><?php */?>
        
        
        
        
          	<?php 
	$query_called = "
	
	
		 SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere 
		 (
		 
		 
q1 = 'Call was complete' 
OR q1 = 'Call was incomplete' 
OR q1 = 'Decision-maker was not available' 
OR q1 = 'Customer has already paid and is active' 
OR q1 = 'Contact details are not for the right customer'  
OR q1='Customer is unreachable'
		 
		 
		 )
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		 
		 

		 
		 
		 
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $succcalls = $row_called['TotalCount'];
		$allcalls = $succcalls ;
		?>
	</td>
	
	 <td class="tblRD" align="center">
	
	</td>
  	</tr>

<tr>
  <td class="tblRD">Customer is unreachable </td><td class="tblRD" align="center">
    
    <?php 
		$query_called = "
		 SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere 
		 (q1='Customer is unreachable')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		 
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $cpaid = $row_called['TotalCount'];
		?>
    
    </td>
  <td class="tblRD" align="center">
    <?php echo round(($cpaid/$allcalls)*100,2)?>
  </td></tr>
<tr>
  <td class="tblRD">Contact details are not for the right customer</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "
		 SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere 
		 (q1='Contact details are not for the right customer')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		 
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $cpaid = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($cpaid/$allcalls)*100,2)?></td>
</tr>
<tr>
  <td class="tblRD">Customer has already paid and is active </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "
		 SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere 
		 (q1='Customer has already paid and is active')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		 
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $cpaid = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($cpaid/$allcalls)*100,2)?></td>
</tr>
<tr>
  <td class="tblRD">Decision-maker was not available</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "
		 SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere 
		 (q1='Decision-maker was not available')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		 
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $cpaid = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($cpaid/$allcalls)*100,2)?></td>
</tr>
<tr>
  <td class="tblRD">Call was incomplete </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "
		 SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere 
		 (q1='Call was incomplete')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		 
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $cpaid = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($cpaid/$allcalls)*100,2)?></td>
</tr>
<tr>
  <td class="tblRD">Call was complete </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "
		 SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere 
		 (q1='Call was complete')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		 
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $cpaid = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($cpaid/$allcalls)*100,2)?></td>
</tr>
	 </tbody>
	</table> 
	
	<!--  Q2. If you have not renewed your subscription with DSTV what are you currently watching? -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>
	 If you have not renewed your subscription with DSTV what are you currently watching?
	 </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
  <th class="tblRBD">%</th>

    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">Bought a new smartcard/decoder of the same brand </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q2 LIKE '%Bought a new smartcard/decoder of the same brand%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $expensive = $row_called['TotalCount'];
		?>
	</td>
	
	<td class="tblRD" align="center">
	<?php echo round(($expensive/$succcalls)*100,2);?>
  </td>
  	</tr>
  	<tr><td class="tblRD">Not watching TV </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q2 LIKE '%Not watching TV%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $hasmoney = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($hasmoney/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">Watching only free-to-air channels</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q2 LIKE '%Watching only free-to-air channels%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $schoolfee= $row_called['TotalCount'];
		?>

</td>
<td class="tblRD" align="center">
<?php echo round(($schoolfee/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">Switched to another pay TV service</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q2 LIKE '%Switched to another pay TV service%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $hasnojob= $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($hasnojob/$succcalls)*100,2);?>
  </td></tr>



	 </tbody>
	</table> 
	
	<!-- Q3. Which other pay TV service are they using?  -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Which other pay TV service are they using? </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
DSTV
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%DSTV%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr><td class="tblRD">Startimes</td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Startimes%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">StarSat</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%StarSat%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">Zuku over fiber</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Zuku over fiber%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">Zuku over satellite</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Zuku over satellite%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Azam TV</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Azam TV%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Zap over satellite</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Zap over satellite%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Zap over TV Cabo</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Zap over TV Cabo%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">TV Cabo (not Zap or DSTV)</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%TV Cabo (not Zap or DSTV)%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">CanalSat</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%CanalSat%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Bein Sports</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Bein Sports%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Pirated services</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Pirated services%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Others</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q3 LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table> 
    
    
    
    
    
  
  	<!-- Q4.a) Main (Primary) reason for having disconnected: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Main (Primary) reason for having disconnected:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">Payment reasons </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Payment reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">Financial reasons /cost is too high </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Financial reasons /cost is too high%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">External influences </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%External influences%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">Billing reasons </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Billing reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">Technical reasons </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Technical reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Prefers a competitor service instead </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Prefers a competitor service instead%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Customer service </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Customer service%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Electricity issues /Fuel scarcity </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Electricity issues /Fuel scarcity%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Value /Content relevance </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Value /Content relevance%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Other </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4a LIKE '%Other%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table> 
    
    

  	<!-- Q4.b) Secondary reason for having disconnected: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Secondary reason for having disconnected:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Payment reasons 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Payment reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Financial reasons /cost is too high
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Financial reasons /cost is too high%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    External influences </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%External influences%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Billing reasons </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Billing reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Technical reasons</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Technical reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Prefers a competitor service instead </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Prefers a competitor service instead%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Customer service </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Customer service%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Electricity issues /Fuel scarcity </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Electricity issues /Fuel scarcity%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Value /Content relevance </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Value /Content relevance%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Other reasons </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q4b LIKE '%Other%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table> 
    
    
    
  	<!-- Q5. Payment reasons: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Payment reasons:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     No time to make payment
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q5 LIKE '%No time to make payment%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Inconvenient to make payment
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q5 LIKE '%Inconvenient to make payment%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Did not know the disconnect date</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q5 LIKE '%Did not know the disconnect date%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Forgot to pay</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q5 LIKE '%Forgot to pay%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Prefers subscription to finish before paying </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q5 LIKE '%Prefers subscription to finish before paying%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q5 LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table> 

    
  	<!-- Q6. Financial reasons/Cost: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Financial reasons/Cost: </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Too expensive – luxury I cannot afford 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q6 LIKE '%Too expensive – luxury I cannot afford%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Have other financial responsibility I need to prioritize 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q6 LIKE '%Have other financial responsibility I need to prioritize%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Lost his job / lower income</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q6 LIKE '%Lost his job / lower income%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    I have not been paid yet </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q6 LIKE '%I have not been paid yet%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Too expensive / service is not worth the cost </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q6 LIKE '%Too expensive / service is not worth the cost%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Installation cost was too high after I moved and not worth it </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q6 LIKE '%Installation cost was too high after I moved and not worth it%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Was paying school fees or rent</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q6 LIKE '%Was paying school fees or rent%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>   
  

  	<!-- Q7. Did they consider downgrading before making a decision to disconnect the service (Do not ask Access)? -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Did they consider downgrading before making a decision to disconnect the service (Do not ask Access)?</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Did not consider – was not aware of other packs
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q7 LIKE '%Did not consider – was not aware of other packs%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Considered – but key channels not available on lower packs
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q7 LIKE '%Considered – but key channels not available on lower packs%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Considered – but was not aware how</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q7 LIKE '%Considered – but was not aware how%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Considered – but haven&rsquo;t yet downgraded</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q7 LIKE '%Considered – but havent yet downgraded%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
	 </tbody>
	</table>   
  

  	<!-- Q8. External influences -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>External influences</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Had to travel for a while  
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q8 LIKE '%Had to travel for a while%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    No time to watch / watching too little
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q8 LIKE '%No time to watch / watching too little%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Experience high level of power outages </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q8 LIKE '%Experience high level of power outages%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Moved homes and don&rsquo;t need anymore</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q8 LIKE '%Moved homes and dont need anymore%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    No longer wants to watch TV</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q8 LIKE '%No longer wants to watch TV%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Lifestyle change (Kids not at home, death, divorce, got married, job requirement etc.…)</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q8 LIKE '%Lifestyle change%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q8 LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
    
  
  
  	<!-- Q9. Billing reasons: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Billing reasons:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Incorrect disconnection 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q9 LIKE '%Incorrect disconnection%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Paid, but not reconnected and resulted in having to view for less than 30 days 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q9 LIKE '%Paid, but not reconnected and resulted in having to view for less than 30 days%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Paid, but not reflected in the account- </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q9 LIKE '%Paid, but not reflected in the account%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Paid, but was an incorrect amount - </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q9 LIKE '%Paid, but was an incorrect amount%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q9 LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
    
  
  	<!-- Q10. Technical reasons: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Technical reasons:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Poor signal due to weather conditions 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Poor signal due to weather conditions%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Poor signal in my area 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Poor signal in my area%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Decoder/accessories/etc. not working</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Decoder/accessories/etc. not working%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Could not clear error codes/channels missing</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Could not clear error codes/channels missing%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
  
  <tr>
  <td class="tblRD">
    Swapped decoders</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Swapped decoders%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>

  <tr>
  <td class="tblRD">
    Faulty Decoders</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Faulty Decoders%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>

  <tr>
  <td class="tblRD">
    Decoders sold</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Decoders sold%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
  
  <tr>
  <td class="tblRD">
    Phased out Decoders </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Phased out Decoders %')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
  
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q10 LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
    
  
  
   

	</td></tr></table>
	</div>
	
	
	<div style="float:right; width:40%; margin-left:10px;margin-right:10px;padding:10px">
        
        
  
  	<!-- Q11. Reason for preferring a competitor service instead  -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption> Reason for preferring a competitor service instead</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
    <td class="tblRD">
     More affordable
    </td>
    <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q11 LIKE '%More affordable%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?></td>
    <td class="tblRD" align="center"><?php echo round(($travelled/$succcalls)*100,2)?></td>
    </tr>
  <tr>
    <td class="tblRD">
     Flexible payment method
    </td>
    <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q11 LIKE '%Flexible payment method%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?></td>
    <td class="tblRD" align="center"><?php echo round(($travelled/$succcalls)*100,2)?></td>
    </tr>
  <tr>
    <td class="tblRD">
     More local content 
    </td>
    <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q11 LIKE '%More local content%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?></td>
    <td class="tblRD" align="center"><?php echo round(($travelled/$succcalls)*100,2)?></td>
    </tr>
  <tr>
    <td class="tblRD">
     Best sports coverage 
    </td>
    <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q11 LIKE '%Best sports coverage%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?></td>
    <td class="tblRD" align="center"><?php echo round(($travelled/$succcalls)*100,2)?></td>
    </tr>
  <tr>
    <td class="tblRD">
     Offered content that suited my needs/everyone in the home (e.g.; internet bundled offer 
    </td>
    <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q11 LIKE '%Offered content that suited my needs/everyone in the home%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?></td>
    <td class="tblRD" align="center"><?php echo round(($travelled/$succcalls)*100,2)?></td>
    </tr>
  <tr>
    <td class="tblRD">
     More FTA channels 
      </td>
    <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q11 LIKE '%More FTA channels%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?></td>
    <td class="tblRD" align="center"><?php echo round(($travelled/$succcalls)*100,2)?></td>
  </tr>
  <tr>
    <td class="tblRD">Others</td>
    <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q11 LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?></td>
    <td class="tblRD" align="center"><?php echo round(($travelled/$succcalls)*100,2)?></td>
    </tr>
	 </tbody>
	</table>     
        
        
        	

  	<!-- Q12. Customer service  -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption> Customer service</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">Issues with customer support when we had a query</td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q12 LIKE '%Issues with customer support when we had a query%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Couldn&rsquo;t get hold of customer service when I had a billing/technical query
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q12 LIKE '%Couldnt get hold of customer service when I had a billing/technical query%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Difficult to get support from DSTV/GOTV customer service </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q12 LIKE '%Difficult to get support from DSTV/GOTV customer service%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Not enough flexibility in terms of payment terms/fractional payments </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q12 LIKE '%Not enough flexibility in terms of payment terms/fractional payments%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    I paid and was not reconnected immediately waited for 2/3 days was never reimbursed 
    <div>
      <div> </div>
    </div></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q12 LIKE '%I paid and was not reconnected immediately waited for 2/3 days was never reimbursed%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    No easy way to pay for monthly subscription </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q12 LIKE '%No easy way to pay for monthly subscription%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q12 LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
  
  
  	<!-- Q13. Reasons for preferring Value/Content Value  -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Reasons for preferring Value/Content Value</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Not watching enough to justify the cost 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Not watching enough to justify the cost%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Subscribed for a while to watch a specific sporting event 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Subscribed for a while to watch a specific sporting event%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Subscribed for a while to watch a specific programme or channel</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Subscribed for a while to watch a specific programme or channel%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Can get the same programme from another provider for less money </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Can get the same programme from another provider for less money%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Too many repeated programmes</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Too many repeated programmes%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Too many old movies, series etc…</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Too many old movies, series etc%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Favourite TV programme no longer available </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Favourite TV programme no longer available%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD"><ol>
    Not enough interesting local sports 
  </ol></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Not enough interesting local sports%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD"><ol>
    Not enough interesting international sports 
  </ol></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Not enough interesting international sports%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD"><ol>
    Not enough local content
  </ol></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%Not enough local content%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD"><ol>
    No rewards for being loyal 
  </ol></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%No rewards for being loyal%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">No rebate is offered when specific channels or programmes are no longer available  </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q13 LIKE '%No rebate is offered when specific channels or programmes are no longer available%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>       
  
  
   	<!-- Q14 a. DBC - Alternative contact number checked? -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>DBC - Alternative contact number checked?</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Confirmed e-mail
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q14a LIKE '%Confirmed e-mail%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Modified/added e-mail
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q14a LIKE '%Modified/added e-mail%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Does not have e-mail</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q14a LIKE '%Does not have e-mail%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Did not check </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q14a LIKE '%Did not check%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
	 </tbody>
	</table>       
  
  
   	<!-- Q14 b. DBC - E-mail checked? -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>DBC - E-mail checked?</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Confirmed mobile
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q14b LIKE '%Confirmed mobile%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Modified/added mobile
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q14b LIKE '%Modified/added mobile%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Did not check</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q14b LIKE '%Did not check%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>
	 </tbody>
	</table>       
   

  
   	<!-- Q15. Expected outcome of the call: the customer was… -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Expected outcome of the call: the customer was</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Not happy with the call and is <u>unlikely to re-connect</u>
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q15 LIKE '%Not happy with the call and is unlikely to re-connect%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = $row_called['TotalCount'];
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Happy with the call but it&rsquo;s <u>unclear if they will re-connect</u>
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q15 LIKE '%Happy with the call but its unclear if they will re-connect%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = $row_called['TotalCount'];
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Happy with the call and is expected to <u>re-connect immediately</u></td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q15 LIKE '%Happy with the call and is expected to re-connect immediately%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Happy with the call and is expected to <u>re-connect eventually</u></td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q15 LIKE '%Happy with the call and is expected to re-connect eventually%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Don&rsquo;t know/not sure </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (q15 LIKE '%Dont know/not sure%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = $row_called['TotalCount'];
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     

	<!-- Escalated -->
	
	<?php $array=array('Yes',
'No'
	);?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Escalated - DSTV </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>
<?php foreach($array as $arrayval)
{?>
  <tr>
   <td class="tblRD">
<?php echo $arrayval;?>
   </td>
   
  <td class="tblRD" align="center">
	<?php 
	$query_called = "SELECT COUNT(id) AS TotalCount FROM survey WHERE $adwhere (escalated ='$arrayval')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $escalated = $row_called['TotalCount'];?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($escalated/$succcalls)*100,2);?>
	</td>
	</tr>
	<?php } ?>
	
	</tbody>
	</table>

	<!-- Reconnected -->
	
	
	<?php $array=array('Yes',
'No'
	);?>

	<!--DSTV Promo-->
	
	
	<!--DSTV Promo-->
	
	<!--Package Promo-->
	
	
	
	<!--End Package Promo-->
	</div>
	

	<!--  <div style="float:right; width:50%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="right">
	 <caption>Reachable Contact Status</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		 /*$full= "SELECT disposation FROM survey WHERE  $adwhere (disposation='Call Back' OR disposation='Customer Hanged Up' OR disposation='Complete Survey' OR  disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey') GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1  WHERE $adwhere (disposation='Call Back' OR disposation='Customer Hanged Up' OR disposation='Complete Survey' OR  disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey')  ";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1  WHERE $adwhere disposation='".$value."'  ";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div> -->
	<!-- Start all Diposations -->
	<!-- <div style="float:left; width:60%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="left">
	 <caption>All Diposations</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		/* $full= "SELECT disposation FROM survey WHERE  $adwhere  disposation<>'' GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1  WHERE $adwhere  disposation<>''  ";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1  WHERE $adwhere disposation='".$value."'  ";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div>-->
	<!-- Start Unreachables -->
	<!--<div style="float:right; width:30%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="right">
	 <caption>Unreachable Contact Status</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		/* $full= "SELECT disposation FROM survey WHERE  $adwhere (disposation='Wrong Number' OR disposation='Voice Mail' OR disposation='Switched Off' OR disposation='No Answer' OR disposation='Invalid' OR disposation='Language Barrier' OR disposation='Hang up' OR disposation='Call abandoned' OR disposation='Busy' OR disposation='Number out of Service') GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1 WHERE $adwhere (disposation='Wrong Number' OR disposation='Switched Off' OR disposation='Voice mail' OR disposation='Wrong Number' OR disposation='Call Abandoned' OR disposation='Hanged up' OR disposation='Language Barrier' OR disposation='No answer' OR disposation='Number out of service' OR disposation='Phone busy')  ";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1 WHERE $adwhere disposation='".$value."'  ";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div>-->
  <!-- end .content --></div>
<?php }?>
</body>
</html>
<?php
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);
echo 'Page generated in '.$total_time.' seconds.'; ?>