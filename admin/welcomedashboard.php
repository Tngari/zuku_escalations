<?php
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;
?>
<?php require_once('../Connections/air2013.php');
 ini_set('max_execution_time',0);
ini_set('memory_limit','-1');?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;



$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;


if(isset($_GET["fromdate"]))
{
	
	$adwhere="update_time between '". $fromdate . "' AND '". $todate . "' AND";
}
else
{
	
	$adwhere="";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multichoice SCRIPT</title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="jquery2/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="jquery2/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery2/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="jquery2/js/table2CSV.js" ></script>
<script type="text/javascript" src="js/jquery.pleaseWait.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>

<!--<script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="jquery2/js/jquery.highchartTable.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart')
  .bind('highchartTable.beforeRender', function (event, highChartConfig) {
        console.log(highChartConfig);

        highChartConfig.chart.events = {
            load: function () {
                var data = this.series[0].data,
                    dLen = data.length,
                    i = 0;

                while (dLen > i) {
                    var point = data[i];
                    if (point.sliced) {
                        $report.html('<h2>' + point.name + '</h2><p>' + point.percentage.toFixed(1) + '%</p>');
                        i = dLen;
                    }
                    i++;
                }

            }
        }

        highChartConfig.plotOptions = {
            pie: {
                cursor: 'pointer',
                size: '60%', // size of pieChart
                stickyTracking: false, // if false, events per slice/obj
                innerSize: '35%', // innerSize dount ring
                //allowPointSelect: true,
                dataLabels: {
                    distance: -50
                },
                center: ['35%', '50%'],
                showInLegend: true,
                slicedOffset: 15,
                shadow: 0,
                point: {
                    events: {
                        mouseOver: function (event) {
                            $report.html('<h2>' + this.name + '</h2><p>' + this.percentage.toFixed(1) + '%</p>');
                            this.select(true);
                        },
                        mouseOut: function (event) {
                            this.select(false);
                        },
                        legendItemClick: function (event) {
                            this.select(null);
                            $report.html('<h2>' + this.name + '</h2><p>' + this.percentage.toFixed(1) + '%</p>');

                            return false;
                        }
                    }
                }
            }
        },
        highChartConfig.tooltip = {
            enabled: true
        },
        highChartConfig.legend = {
            cursor: 'pointer',
            floating: true,
           
            borderColor: null,
            verticalAlign: 'top',
            x: 0,
            y: 20,
            width:400,
            itemWidth: 180
        };
    })
  .highchartTable();

});


</script>-->
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	
            <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id="frmsearchbar" action="welcomedashboard.php" method="GET">
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' required value="<?php echo $fromdt; ?>" type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' required value="<?php echo $todt;?>" type='textbox' class="dateselection" id="todt" size='20'/></td>
                    
                    <td align="center">Category:</td>
                    <td align="center">
                    
                    <select name="category">
                     <option value="<?php echo $_GET['category'];?>"><?php echo $_GET['category'];?></option>
                    <option value="GoTV">GoTV</option>
                    <option value="DSTV">DSTV</option>
                   
                   
                    </select>
                    
                    </td>
                    
                    <td align="center"><input  onclick="$('.content').pleaseWait();"  type="submit" name="submit" value="GET WELCOME DASHBOARD REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
           
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>
<br>
<form>
<?php include "menu2.php";?>
</form>
<?php if(isset($_GET["submit"])){?>
   <div class="content">
	<script>
       $('.content').pleaseWait();
    </script>

	<div style="float:left; width:40%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Campaign Name  - WELCOME </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
   Data Dialled
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	
	if(isset($_GET['category']) && !empty($_GET['category']))
	{
		$dy=$_GET['category'];
		
				$mqr='AND leads.category="$dy"';
			
			
	}
	else 
	{
		$mqr='';
	}
$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (disposation='Language Barrier' OR disposation='Call Back' OR disposation='Complete Survey' OR disposation='Customer Hanged Up' OR disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey')
		 AND disposation<>'' AND cmpaign='Welcome' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $allcalls = mysql_num_rows($called_reports);
		?>
	</td>
	
	 <td class="tblRD" align="center">
	
	</td>
  	</tr>
  	<tr><td class="tblRD">No of successful calls</td><td class="tblRD" align="center">
  	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (disposation='Complete Survey')
		 AND disposation<>'' AND cmpaign='Welcome' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $succcalls = mysql_num_rows($called_reports);
		?>
  	</td>
  	
  	 <td class="tblRD" align="center">
	<?php echo round(($succcalls/$allcalls)*100,2);?>
	</td>
  	
  	</tr>


	 </tbody>
	</table> 
	
	<!-- Funancials -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Financial Reasons - WELCOME </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
  <th class="tblRBD">%</th>

    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
  Expensive
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_7b LIKE '%Expensive%')
		 AND disposation<>'' AND cmpaign='Welcome' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $expensive = mysql_num_rows($called_reports);
		?>
	</td>
	
	<td class="tblRD" align="center">
	<?php echo round(($expensive/$succcalls)*100,2);?>
  </td>
  	</tr>
  	<tr><td class="tblRD">Prices</td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_7b LIKE '%Prices%')
		 AND disposation<>'' AND cmpaign='WELCOME' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $hasmoney = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($hasmoney/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">Has no money</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_7b LIKE '%Has no money%')
		 AND disposation<>'' AND cmpaign='WELCOME' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $schoolfee= mysql_num_rows($called_reports);
		?>

</td>
<td class="tblRD" align="center">
<?php echo round(($schoolfee/$succcalls)*100,2);?>
  </td></tr>


<tr><td class="tblRD">Others</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_7b LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='WELCOME' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $others = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($others/$succcalls)*100,2);?>
  </td></tr>



	 </tbody>
	</table> 
	

<!-- Technical Reasons -->

<?php $array=array('Poor Signal',
'Faulty decoder',
'Electricity/No Power',
'Faulty Accessories- TV/LNB',
'Relocated/Shifted',
'Delayed Installation','Others');?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Technical Reasons - WELCOME </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
  <th class="tblRBD">%</th>

    
  </tr>
    </thead>
	 <tbody>
<?php foreach($array as $arrayval)
{?>
  <tr>
   <td class="tblRD">
<?php echo $arrayval;?>
   </td>
   
  <td class="tblRD" align="center">
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_7d LIKE '%$arrayval%')
		 AND disposation<>'' AND cmpaign='WELCOME' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $techrs = mysql_num_rows($called_reports);?>
	</td>
	
	<td class="tblRD" align="center">
	
	<?php echo round(($techrs/$succcalls)*100,2);?>
	
	</td>
	</tr>
	<?php } ?>
	
	</tbody>
	</table>
	</div>
	
	
		<div style="float:right; width:40%; margin-left:10px;margin-right:10px;padding:10px">	
	
	<?php $array=array('Mobile Payment failure',
'Payment options','Doesnt know prices','Others');?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Payment  - WELCOME </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 

     <th class="tblRBD">%</th>
  </tr>
    </thead>
	 <tbody>
<?php foreach($array as $arrayval)
{?>
  <tr>
   <td class="tblRD">
<?php echo $arrayval;?>
   </td>
   
  <td class="tblRD" align="center">
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (welcome_payment LIKE '%$arrayval%')
		 AND disposation<>'' AND cmpaign='Welcome' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $watching= mysql_num_rows($called_reports);?>
	</td>
	<td class="tblRD" align="center"><?php echo round(($watching/$succcalls)*100,2);?></td>
	</tr>
	<?php } ?>
	
	</tbody>
	</table>
	
	<!-- content -->
	<?php $array=array('Similar content',
'Repeated content',
'Poor content',
'Others');?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Content  - WELCOME </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>
<?php foreach($array as $arrayval)
{?>
  <tr>
   <td class="tblRD">
<?php echo $arrayval;?>
   </td>
   
  <td class="tblRD" align="center">
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (welcome_content LIKE '%$arrayval%')
		 AND disposation<>'' AND cmpaign='Welcome' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $billreason = mysql_num_rows($called_reports);?>
	</td>
	<td class="tblRD" align="center"><?php echo round(($billreason/$succcalls)*100,2); ?></td>
	</tr>
	<?php } ?>
	
	</tbody>
	</table>
	
	
	<!-- Others -->
	
	<?php $array=array(
'Others');?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Others - WELCOME </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
 <th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>
<?php foreach($array as $arrayval)
{?>
  <tr>
   <td class="tblRD">
<?php echo $arrayval;?>
   </td>
   
  <td class="tblRD" align="center">
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_7g LIKE '%$arrayval%')
		 AND disposation<>'' AND cmpaign='Welcome' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $otherrson = mysql_num_rows($called_reports);?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($otherrson/$succcalls)*100,2);?>
	</td>
	</tr>
	<?php } ?>
	
	</tbody>
	</table>
	
	<!-- Escalated -->
	
	<?php $array=array('Good',
'Bad'
	);?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>How you find the service  - WELCOME </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>
<?php foreach($array as $arrayval)
{?>
  <tr>
   <td class="tblRD">
<?php echo $arrayval;?>
   </td>
   
  <td class="tblRD" align="center">
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (welcome_purchase ='$arrayval')
		 AND disposation<>'' AND cmpaign='Welcome' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $escalated = mysql_num_rows($called_reports);?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($escalated/$succcalls)*100,2);?>
	</td>
	</tr>
	<?php } ?>
	
	</tbody>
	</table>
	
	<!-- Reconnected -->
	
	
	<?php $array=array('Yes',
'No'
	);?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Have you faced any challenges - WELCOME </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 <th class="tblRBD">%</th>

    
  </tr>
    </thead>
	 <tbody>
<?php foreach($array as $arrayval)
{?>
  <tr>
   <td class="tblRD">
<?php echo $arrayval;?>
   </td>
   
  <td class="tblRD" align="center">
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (welcome_challange ='$arrayval')
		 AND disposation<>'' AND cmpaign='WELCOME' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $reconnected = mysql_num_rows($called_reports);?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($reconnected/$succcalls)*100,2);?>
	</td>
	</tr>
	<?php } ?>
	
	</tbody>
	</table>
	
	
	<!--WELCOME Promo-->
	
	
	<!--WELCOME Promo-->
	
	<!--Package Promo-->
	
	
	
	<!--End Package Promo-->
	</div>
	

	<!--  <div style="float:right; width:50%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="right">
	 <caption>Reachable Contact Status</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		 /*$full= "SELECT disposation FROM survey WHERE  $adwhere (disposation='Call Back' OR disposation='Customer Hanged Up' OR disposation='Complete Survey' OR  disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey') GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1  WHERE $adwhere (disposation='Call Back' OR disposation='Customer Hanged Up' OR disposation='Complete Survey' OR  disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey') ORDER BY S1.id DESC";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1  WHERE $adwhere disposation='".$value."'  ORDER BY S1.id DESC";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div> -->
	<!-- Start all Diposations -->
	<!-- <div style="float:left; width:60%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="left">
	 <caption>All Diposations</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		/* $full= "SELECT disposation FROM survey WHERE  $adwhere  disposation<>'' GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1  WHERE $adwhere  disposation<>'' ORDER BY S1.id DESC";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1  WHERE $adwhere disposation='".$value."' ORDER BY S1.id DESC";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div>-->
	<!-- Start Unreachables -->
	<!--<div style="float:right; width:30%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="right">
	 <caption>Unreachable Contact Status</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		/* $full= "SELECT disposation FROM survey WHERE  $adwhere (disposation='Wrong Number' OR disposation='Voice Mail' OR disposation='Switched Off' OR disposation='No Answer' OR disposation='Invalid' OR disposation='Language Barrier' OR disposation='Hang up' OR disposation='Call abandoned' OR disposation='Busy' OR disposation='Number out of Service') GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1 WHERE $adwhere (disposation='Wrong Number' OR disposation='Switched Off' OR disposation='Voice mail' OR disposation='Wrong Number' OR disposation='Call Abandoned' OR disposation='Hanged up' OR disposation='Language Barrier' OR disposation='No answer' OR disposation='Number out of service' OR disposation='Phone busy') ORDER BY S1.id DESC";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1 WHERE $adwhere disposation='".$value."' ORDER BY S1.id DESC";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div>-->
  <!-- end .content --></div>
<?php } ?>
 <script>
   $('.content').pleaseWait('stop');
</script>
</body>
</html>
<?php
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$finish = $time;
$total_time = round(($finish - $start), 4);
echo '<!--Page generated in '.$total_time.' seconds.-->'; ?>

