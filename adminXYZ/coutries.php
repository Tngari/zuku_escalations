<?php require_once('../Connections/air2013.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form1")) {
  $insertSQL = sprintf("INSERT INTO countries (country, cid) VALUES (%s, %s)",
                       GetSQLValueString($_POST['country'], "text"),
                       GetSQLValueString($_POST['cid'], "int"));

  mysql_select_db($database_air2013, $air2013);
  $Result1 = mysql_query($insertSQL, $air2013) or die(mysql_error());

  $insertGoTo = "coutries.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
  $updateSQL = sprintf("UPDATE countries SET display=%s WHERE id=%s",
                       GetSQLValueString($_POST['display'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_air2013, $air2013);
  $Result1 = mysql_query($updateSQL, $air2013) or die(mysql_error());

  $updateGoTo = "coutries.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form3")) {
  $updateSQL = sprintf("UPDATE countries SET display=%s WHERE id=%s",
                       GetSQLValueString($_POST['display'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_air2013, $air2013);
  $Result1 = mysql_query($updateSQL, $air2013) or die(mysql_error());

  $updateGoTo = "coutries.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
    $updateGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

mysql_select_db($database_air2013, $air2013);
$query_users = "SELECT * FROM countries";
$users = mysql_query($query_users, $air2013) or die(mysql_error());
$row_users = mysql_fetch_assoc($users);
$totalRows_users = mysql_num_rows($users);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Airtel - Millward Campaign</title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top">
            <form>
           	  <input type=button onClick="location.href='coutries.php'" value='COUNTRIES MANAGEMENT'> &nbsp;
              <input type=button onClick="location.href='users.php'" value='USER MANAGEMENT'> &nbsp; 
              <input type=button onClick="location.href='leads.php'" value='LEADS MANAGEMENT'> &nbsp; 
              <input type=button onClick="location.href='admin.php'" value='MAIN REPORTS'> &nbsp;
              <input type=button onClick="location.href='disprep.php'" value='DISPOSITION REPORTS'> &nbsp;
              <input type=button onClick="location.href='../home.php'" value='HOME'>
           	</form>
            </td>
      	</tr>
  	</table>
</div>

<div class="container">
  <div class="content">
    <table align="center" width="100%">
    	<tr>
        	<td align="center" class="tbl">
              <form action="<?php echo $editFormAction; ?>" method="post" name="form1" id="form1">
                <table align="center" cellpadding="5">
                  <tr valign="baseline">
                    <td nowrap="nowrap" align="right">Country:</td>
                    <td><input type="text" name="country" value="" size="25" /></td>
                    <td nowrap="nowrap" align="right">Country Code:</td>
                    <td><input type="text" name="cid" value="" size="2" /></td>
                    <td align="right"><input type="submit" value="Add Country" /></td>
                  </tr>
                </table>
                <input type="hidden" name="password" value="airtel" />
                <input type="hidden" name="MM_insert" value="form1" />
              </form>
            </td>
        </tr>
        
        <tr><td>&nbsp;</td></tr>
        
        <tr><td>
  			<table align="center">
            	<tr>
                	<td class="tblRBU"><strong>Country</strong></td>
                    <td class="tblRBU"><strong>Code</strong></td>
                    <td class="tblRBU"><strong>Displayed</strong></td>
                    <td class="tblRBU" align="center"><strong>Delete</strong></td>
              	</tr>
              	<?php do { ?>
                <tr>
                    <td class="tblRU"><?php echo $row_users['country']; ?></td>
                    <td class="tblRU"><?php echo $row_users['cid']; ?></td>
                    <td class="tblRU" align="center"><?php echo $row_users['display']; ?></td>
                  <td class="tblRU" align="center"><a href="del_country.php?id=<?php echo $row_users['id']; ?>" onclick="return confirm('Are you sure you want to DELETE <?php echo $row_users['country']; ?>?');"><img src="../images/cross.png" width="16" height="16" alt="Delete" /></a></td>
                    <td align="left">
                    <?php
						$yid = $row_users['id'];
						mysql_select_db($database_air2013, $air2013);
						$query_yes = "SELECT * FROM countries WHERE display = 'N' AND id = '".$yid."'";
						$yes = mysql_query($query_yes, $air2013) or die(mysql_error());
						$row_yes = mysql_fetch_assoc($yes);
						$totalRows_yes = mysql_num_rows($yes);
					?>
                    <?php if ($totalRows_yes > 0) { // Show if recordset not empty ?>
                      	<form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
                        	<input type="submit" value="Display" />
                        	<input type="hidden" name="display" value="Y" />
                        	<input type="hidden" name="MM_update" value="form2" />
                        	<input type="hidden" name="id" value="<?php echo $row_yes['id']; ?>" />
                      	</form>
              		<?php } // Show if recordset not empty ?>
					
					<?php
						$nid = $row_users['id'];
						mysql_select_db($database_air2013, $air2013);
						$query_no = "SELECT * FROM countries WHERE display = 'Y' AND id = '".$nid."'";
						$no = mysql_query($query_no, $air2013) or die(mysql_error());
						$row_no = mysql_fetch_assoc($no);
						$totalRows_no = mysql_num_rows($no);
					?>
                    <?php if ($totalRows_no > 0) { // Show if recordset not empty ?>
                    	<form action="<?php echo $editFormAction; ?>" method="post" name="form3" id="form3">
                      		<input type="submit" value="Hide" />
                      		<input type="hidden" name="display" value="N" />
                      		<input type="hidden" name="MM_update" value="form3" />
                      		<input type="hidden" name="id" value="<?php echo $row_no['id']; ?>" />
                    	</form>
                  	<?php } // Show if recordset not empty ?>
                    </td>
            	</tr>
            	<?php } while ($row_users = mysql_fetch_assoc($users)); ?>
      		</table>
        </td></tr>
  	</table>
  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($users);
mysql_free_result($yes);
mysql_free_result($no);
?>
