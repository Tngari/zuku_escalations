<?php require_once('../Connections/air2013.php'); error_reporting(0); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

if(isset($_GET['mdays']) && !empty($_GET['mdays']))
	{
		$dy=$_GET['mdays'];
		if($dy==30)
			$mqr='AND leads.No_Days_Disconnected>0 AND leads.No_Days_Disconnected<='.$dy.'';
		
			if($dy==60)
				$mqr='AND leads.No_Days_Disconnected>30 AND leads.No_Days_Disconnected<='.$dy.'';
			
				if($dy==90)
					$mqr='AND leads.No_Days_Disconnected>60 AND leads.No_Days_Disconnected<='.$dy.'';
	}
	else 
	{
		$mqr='';
	}

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;
$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
$phone=$_GET['phone'];
$campaign=$_GET['campn'];
mysql_select_db($database_air2013, $air2013);

if($todt!='')
{
$query_airtel_reports = "SELECT survey.id as `mid`,survey.lid,survey.*,leads.*  FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE update_time between '". $fromdate . "' AND '". $todate . "' AND disposation<>'' AND dbphone LIKE '%$phone%' AND cmpaign='DSTV' AND (q1_7='Promise to Pay' OR prmtopay='Yes') $mqr";
}
else 
	
{
	$query_airtel_reports = "SELECT survey.id as `mid`,survey.lid,survey.*,leads.*  FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE disposation<>'' AND dbphone LIKE '%$phone%' AND cmpaign='DSTV' AND (q1_7='Promise to Pay' OR prmtopay='Yes') $mqr";
	
}


$air2013_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());  //
$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
$totalRows_airtel_reports = mysql_num_rows($air2013_reports);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DSTV/GoTV - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2" nowrap="nowrap">
            <form>
           	 
              <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='promise-pay.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' type='textbox' value="<?php echo $fromdt; ?>" required class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' type='textbox' required class="dateselection"  value="<?php echo $todt; ?>" id="todt" size='20'/></td>
                      <td align="center">Phone No:</td>
                    <td align="center"><input name='phone' type='textbox' class="dateselection" id="" size='20'/></td>
                    <input type="hidden" name="campn" name="DSTV" required>
                      <td align="center">Disconnect:</td>
                    <td align="center">
					<select name="mdays">
					<option value="<?php echo $_GET['mdays'];?>"><?php echo $_GET['mdays'];?></option>
					<option value="">All</option>
					<option value="30">30 days</option>
					<option value="60">60 days</option>
					<option value="90">90 days</option>
					</select>
					
					</td>
             
                    <td align="center"><input type="submit" name="submit" value="GET MULTICHOICE REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
            <form action="getCSV.php" method ="post" > 
				<input type="hidden" name="csv_text" id="csv_text">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>

  <div class="content">
	<table id="csvdownload">
	<tr>
	<td class="tblRB">#</td>
	  <td class="tblRB">Qnr.ID</td>
	  <td class="tblRB">Phone No</td>
	    <td class="tblRB">Campaign</td>
		  <td class="tblRB">No_Days_Disconnected</td>
		    <td class="tblRB">City</td>
			  <td class="tblRB">Customer_Nr</td>
			    <td class="tblRB">Smartcard</td>
				  <td class="tblRB">Customer_Name</td>
				    <td class="tblRB">Account_Type</td>
					  <td class="tblRB">Mobile_Nr</td>
					    <td class="tblRB">Account_Status</td>
						  <td class="tblRB">Email</td>
						    <td class="tblRB">Outstanding_Balance</td>
							  <td class="tblRB">Home_Phone</td>
							    <td class="tblRB">Start_Date</td>
								  <td class="tblRB">Work_Phone</td>
								    <td class="tblRB">Disconnect_Date</td>
									  <td class="tblRB">Suburb</td>
									    <td class="tblRB">Dec_Model</td>
										  <td class="tblRB">Previous Package</td>
	    <td class="tblRB">Customer Name</td>
		
	   <td class="tblRB">intro</td>
	   <td class="tblRB">Main Disposation</td>
	    <td class="tblRB">Incorrect details</td>
		 <td class="tblRB">Promise to Pay</td>
     <td class="tblRB">Pay date</td>
     <td class="tblRB">Pay Time</td>
     <td class="tblRB">Reconnection Date</td>
     <td class="tblRB">Reconnection Time</td>
     <td class="tblRB">Competitor</td>
	  <td class="tblRB">Competitor Others</td>
	    <td class="tblRB">Stolen</td>
	  <td class="tblRB">Stolen Comments</td>
	   <td class="tblRB">GoTV Promotion</td>
	   <td class="tblRB">Financials</td>
	   <td class="tblRB">Financial Others</td>
	  <td class="tblRB">Time Reason</td>
	     <td class="tblRB">Time Reason Others</td>
	       <td class="tblRB">Technical Reasons</td>
	         <td class="tblRB">Technical Reasons Others</td>
  
	  <td class="tblRB">Watching Reasons</td>
	  <td class="tblRB">Watching Reasons Others</td>
	  <td class="tblRB">Billing Reasons</td>
	  <td class="tblRB">Billing Reasons Others</td>
     
     
     <td class="tblRB">Others</td>
    <td class="tblRB">Bad Customer Service Comments</td>
      <td class="tblRB">Others Commenst</td>
        <td class="tblRB">Escalate</td>
          <td class="tblRB">Reconnected</td>
       <td class="tblRB">Call Disposition</td>
        <td class="tblRB">Disposition Comments</td>
     <td class="tblRB">Call Date</td>
      <td class="tblRB">Call Time</td>
	  <td class="tblRB">Agent</td>
	 
	       
    </tr>

<?php 
$t=0;
do
 { 
 $t+=1;
?>
  <tr>
   <td class="tblR"><?php echo $t;?></td>
    <td class="tblR"><?php echo $id=$row_airtel_reports['mid']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['MSISDN']; ?></td>
        <td class="tblR"><?php echo $row_airtel_reports['cmpaign']; ?></td>
		
		  <td class="tblR"><?php echo $row_airtel_reports['No_Days_Disconnected']; ?></td>
		    <td class="tblR"><?php echo $row_airtel_reports['City']; ?></td>
			  <td class="tblR"><?php echo $row_airtel_reports['account']; ?></td>
			    <td class="tblR"><?php echo $row_airtel_reports['Smartcard']; ?></td>
				  <td class="tblR"><?php echo $row_airtel_reports['name']; ?></td>
				    <td class="tblR"><?php echo $row_airtel_reports['Account_Type']; ?></td>
					  <td class="tblR"><?php echo $row_airtel_reports['MSISDN']; ?></td>
					    <td class="tblR"><?php echo $row_airtel_reports['Account_Status']; ?></td>
						  <td class="tblR"><?php echo $row_airtel_reports['email']; ?></td>
						    <td class="tblR"><?php echo $row_airtel_reports['Outstanding_Balance']; ?></td>
							  <td class="tblR"><?php echo $row_airtel_reports['alt_phone']; ?></td>
							    <td class="tblR"><?php echo $row_airtel_reports['Start_Date']; ?></td>
								  <td class="tblR"><?php echo $row_airtel_reports['Work_Phone']; ?></td>
								    <td class="tblR"><?php echo $row_airtel_reports['Disconnect_Date']; ?></td>
									  <td class="tblR"><?php echo $row_airtel_reports['region']; ?></td>
									    <td class="tblR"><?php echo $row_airtel_reports['Dec_Model']; ?></td>
										  <td class="tblR"><?php echo $row_airtel_reports['package']; ?></td>
         <td class="tblR"><?php echo $row_airtel_reports['cname']; ?></td>
     <td class="tblR"><?php if($row_airtel_reports['intro']==1)echo 'Yes'; if($row_airtel_reports['intro']==2)echo 'No'; ?></td> 
    <td class="tblR"><?php   echo $row_airtel_reports['q1_7']; ?></td>
	<td class="tblR"><?php   echo $row_airtel_reports['incorrect_details']; ?></td>
	 <td class="tblR"><?php echo $row_airtel_reports['prmtopay']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['promisedate']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['promisetime']; ?></td>
    
     <td class="tblR"><?php echo $row_airtel_reports['reconnectdate']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['reconnecttime']; ?></td>
       <td class="tblR"><?php echo $row_airtel_reports['competitor']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['competitorother']; ?></td>
    
     <td class="tblR"><?php echo $row_airtel_reports['stolen']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['stolencomm']; ?></td>
	<td class="tblR"><?php echo $row_airtel_reports['gotvpromo']; ?></td>
	<td class="tblR"><?php echo $row_airtel_reports['q1_7b']; ?></td>
	<td class="tblR"><?php echo $row_airtel_reports['q1_7bcomm']; ?></td>
      <td class="tblR"><?php echo $row_airtel_reports['q1_7c']; ?></td>
       <td class="tblR"><?php echo $row_airtel_reports['q1_7ccomm']; ?></td>
        <td class="tblR"><?php echo $row_airtel_reports['q1_7d']; ?></td>
         <td class="tblR"><?php echo $row_airtel_reports['q1_7dcomm']; ?></td>
	<td class="tblR"><?php echo $row_airtel_reports['q1_7e']; ?></td>
	<td class="tblR"><?php echo $row_airtel_reports['q1_7ecomm']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['q1_7f']; ?></td>
      <td class="tblR"><?php echo $row_airtel_reports['q1_7fcomm']; ?></td>
      <td class="tblR"><?php echo $row_airtel_reports['q1_7g']; ?></td>
       <td class="tblR"><?php echo $row_airtel_reports['q1_7gacomm']; ?></td>
       <td class="tblR"><?php echo $row_airtel_reports['q1_7gbcomm']; ?></td>
      

    
	  
	   <td class="tblR"><?php echo $row_airtel_reports['escalated']; ?></td>
	   <td class="tblR"><?php echo $row_airtel_reports['reconenction']; ?></td>
	  <td class="tblR"><?php if($row_airtel_reports['disposation']=='Complete Survey') echo 'Complete Call'; else echo $row_airtel_reports['disposation'];   $sd=$row_airtel_reports['update_time']; $md=explode(" ",$sd); ?></td>
	    <td class="tblR"><?php echo $row_airtel_reports['discomments']; ?></td>
	    <td class="tblR"><?php echo $md[0]; ?></td>
		
		 
		     <td class="tblR">
		     <?php echo $md[1];?>
			</td>
	  <td class="tblR"><?php echo $row_airtel_reports['interviewer']; ?></td>
  	</tr>
  	<?php } while ($row_airtel_reports = mysql_fetch_assoc($air2013_reports)); ?>
	</table> 
  <!-- end .content --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($air2013_reports);
?>

