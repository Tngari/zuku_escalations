<?php require_once('../Connections/air2013.php'); error_reporting(0); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];

if($fromdt=='')
{
	$fromdt=date('Y-m-d');
}
else 
{
	$fromdt = $_GET["fromdate"];
}
$me_id=$_GET['id'];
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
mysql_select_db($database_air2013, $air2013);
$query_airtel_reports = "SELECT * FROM survey WHERE date(update_time)='".$fromdt. "' AND  disposation='Complete Survey' AND id='$me_id'";
$air2013_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());
$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
$totalRows_airtel_reports = mysql_num_rows($air2013_reports);

if(isset($_POST['save']))
{
	$q1=$_POST['q1'];
	$q2=$_POST['q2'];
	$q3=$_POST['q3'];
	$q4=$_POST['q4'];
	$q5=$_POST['q5'];
	$q6=$_POST['q6'];
	$q7=$_POST['q7'];
	$q8=$_POST['q8'];
    $sum=$_POST['q8'];
    $fromdate=$_POST['fromdate'];
    $s_id=$_POST['s_id'];
    $agent=$_POST['agent'];
    $mqc=$_POST['mqc'];
    $qc_comment=mysql_real_escape_string($_POST['qc_comment']);
    
    mysql_select_db($database_air2013, $air2013);
    
    $c='1';
    mysql_query("INSERT INTO qc(q1,q2,q3,q4,q5,q6,q7,q8,qc_avg,agent_name,qc_name,qc_date,q_id,s_date,qc_comment) VALUES('". $q1."','". $q2."','". $q3."','". $q4."','". $q5."','". $q6."','". $q7."','". $q8."','". $sum."','". $agent."','".$mqc."',now(),'". $s_id."','".$fromdate."','".$qc_comment."') ");
  
    mysql_query("UPDATE survey SET `qc` = '". $c ."'  WHERE id = '". $s_id ."'");
    
    header("location:quality-report.php?fromdate=".$fromdate."");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>QC - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>

<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2" nowrap="nowrap">
            
            </td>
      	</tr>
        <tr>
        	<td align="right">
           
			<table align="right">
    			<tr>
    			<td>AGENT NAME :</td>
    			<td><input type='textbox' readonly size='20' value="<?php echo ucwords($row_airtel_reports['interviewer']);?>" /></td>
                	<td>QA DATE :</td>
                	<td align="center"><input name='fromdate' type='textbox' class="dateselection" readonly size='20' value="<?php echo $fromdt;?>" /></td>
                    <td align="center">PHONE NO :</td>
                    <td align="center"><input type='textbox' readonly size='20' value="<?php echo ucwords($row_airtel_reports['dbphone']);?>" /></td>
                    <td align="center"></td>
                </tr>
			</table>
			
    		</td>
            
          
     	</tr>
  	</table>
</div>
<form name="form" method="POST">
<table><tr><td> </td></tr></table>
  <div class="content">
	<table id="csvdownload" style="margin-left:34%; width:40%">
	<tr>
	 <td class="tblRB">QC Param</td>
	  <td class="tblRB">QC Score</td>
	 
	   
    </tr>


  <tr>
   
    <td class="tblR">&nbsp;Welcome</td>
    <td class="tblR">&nbsp;<select required name="q1" style="width:100px" id="q1" onChange="updatesum()">
      <option value=""></option>
      <?php $s=100;
      
      for($s=100; $s>=0;$s--)
      {
      echo '<option value='.$s.'>'.$s.'</option>';
      }?>
    </select></td>

  	</tr>
  	
  	<tr>
   
    <td class="tblR">&nbsp;Data Protection </td>
    <td class="tblR">&nbsp;<select required name="q2" style="width:100px" id="q2" onChange="updatesum()">
      <option value=""></option>
      <?php $s=100;
      
      for($s=100; $s>=0;$s--)
      {
      echo '<option value='.$s.'>'.$s.'</option>';
      }?>
    </select></td>

  	</tr>
  	
  	<tr>
   
    <td class="tblR">&nbsp;Listening Skills </td>
    <td class="tblR">&nbsp;<select required name="q3" style="width:100px" id="q3" onChange="updatesum()">
      <option value=""></option>
      <?php $s=100;
      
      for($s=100; $s>=0;$s--)
      {
      echo '<option value='.$s.'>'.$s.'</option>';
      }?>
    </select></td>

  	</tr>
  	<tr>
   
    <td class="tblR">&nbsp;Soft Skills </td>
    <td class="tblR">&nbsp;<select required name="q4" style="width:100px" id="q4" onChange="updatesum()">
      <option value=""></option>
      <?php $s=100;
      
      for($s=100; $s>=0;$s--)
      {
      echo '<option value='.$s.'>'.$s.'</option>';
      }?>
    </select></td>

  	</tr>
  	<tr>
   
    <td class="tblR">&nbsp;Hold Procedure </td>
    <td class="tblR">&nbsp;<select required name="q5" style="width:100px" id="q5" onChange="updatesum()">
      <option value=""></option>
      <?php $s=100;
      
      for($s=100; $s>=0;$s--)
      {
      echo '<option value='.$s.'>'.$s.'</option>';
      }?>
    </select></td>

  	</tr>
  	<tr>
   
    <td class="tblR">&nbsp;Value Add </td>
    <td class="tblR">&nbsp;<select required name="q6" style="width:100px" id="q6" onChange="updatesum()">
      <option value=""></option>
      <?php $s=100;
      
      for($s=100; $s>=0;$s--)
      {
      echo '<option value='.$s.'>'.$s.'</option>';
      }?>
    </select></td>

  	</tr>
  	<tr>
   
    <td class="tblR">&nbsp;CRM detailing </td>
    <td class="tblR">&nbsp;<select required name="q7" style="width:100px" id="q7" onChange="updatesum()">
      <option value=""></option>
      <?php $s=100;
      
      for($s=100; $s>=0;$s--)
      {
      echo '<option value='.$s.'>'.$s.'</option>';
      }?>
    </select></td>

  	</tr>
  	
  	<tr>
   
    <td class="tblR">&nbsp;Call closing </td>
    <td class="tblR">&nbsp;<select required name="q8" style="width:100px" id="q8" onChange="updatesum()">
      <option value=""></option>
      <?php $s=100;
      
      for($s=100; $s>=0;$s--)
      {
      echo '<option value='.$s.'>'.$s.'</option>';
      }?>
    </select></td>

  	</tr>
  	<tr>
   
    <td class="tblRB">QC Comments</td>
    <td class="tblRB"><textarea name="qc_comment" required cols="30" rows="3"></textarea></td>

  	</tr>
  	<tr>
   
    <td class="tblRB">&nbsp;</td>
    <td class="tblRB">&nbsp;<input type="hidden" id="sum" required readonly name="sum"></input>
    
    <input type="hidden" value="<?php echo $_GET['fromdate']?>" name="fromdate" required></input>
    <input type="hidden" value="<?php echo $_GET['id']?>" name="s_id" required></input>
     <input type="hidden" value="<?php echo $row_airtel_reports['interviewer'];?>" name="agent" required></input>
      <input type="hidden" value="<?php echo $_SESSION['MM_Username'];?>" required name="mqc" ></input>
    </td>

  	</tr>
  	<tr>
   
    <td ></th> <td></td>
    <td ></td>

  	</tr>
  	<tr>
   
    <td> </td>
    <td></td>

  	</tr>
  	<tr>
   
    <td> </td>
    <td><input type="submit" value="Save Data" name="save"></input></td>

  	</tr>
	</table> 
	</form>
  <!-- end .content --></div>
  <script>

  function updatesum() {
	 var q1=parseInt($("#q1").val());
	 var q2=parseInt($("#q2").val());
	 var q3=parseInt($("#q3").val());
	 var q4=parseInt($("#q4").val());
	 var q5=parseInt($("#q5").val());
	 var q6=parseInt($("#q6").val());
	 var q7=parseInt($("#q7").val());
	 var q8=parseInt($("#q8").val());

	  var total=(q1+q2+q3+q4+q5+q6+q7+q8)/8;

         document.form.sum.value =total;

	 
  }
  
  </script>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($air2013_reports);
?>
