<?php require_once('../Connections/air2013.php'); ?>
<?php
error_reporting(0);
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

if (!isset($_SESSION)) {
    session_start();
}
$MM_authorizedUsers = "Admin,Supervisor,agent";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) {
    // For security, start by assuming the visitor is NOT authorized. 
    $isValid = False;

    // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
    // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
    if (!empty($UserName)) {
        // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
        // Parse the strings into arrays. 
        $arrUsers = Explode(",", $strUsers);
        $arrGroups = Explode(",", $strGroups);
        if (in_array($UserName, $arrUsers)) {
            $isValid = true;
        }
        // Or, you may restrict access to only certain users based on their username. 
        if (in_array($UserGroup, $arrGroups)) {
            $isValid = true;
        }
        if (($strUsers == "") && false) {
            $isValid = true;
        }
    }
    return $isValid;
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("", $MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {
    $MM_qsChar = "?";
    $MM_referrer = $_SERVER['PHP_SELF'];
    if (strpos($MM_restrictGoTo, "?"))
        $MM_qsChar = "&";
    if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0)
        $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
    $MM_restrictGoTo = $MM_restrictGoTo . $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
    header("Location: " . $MM_restrictGoTo);
    exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {

    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") {
        if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        }

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }

}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
    $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt . $fromsp ;
$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt . $tosp ;
$status  = $_GET["status"];
mysql_select_db($database_air2013, $air2013);
if($status=='Open' || $status==''){
$query_airtel_reports = "SELECT  * FROM survey  WHERE date(survey.agent_track) between '" . $fromdate . "' AND '" . $todate . "' AND survey.escalation = 'escalation' AND survey.status IS NULL ORDER BY survey.start_time DESC";
}else{
$query_airtel_reports = "SELECT  * FROM survey  WHERE date(survey.agent_track) between '" . $fromdate . "' AND '" . $todate . "' AND survey.escalation = 'escalation' AND survey.status='".$status."' ORDER BY survey.start_time DESC";	
}
$airtel_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());
$row_airtel_reports = mysql_fetch_assoc($airtel_reports);
$totalRows_airtel_reports = mysql_num_rows($airtel_reports);
?>
<?php 
	  if(isset($_POST['submit1'])){	   
	  if(preg_match("/^[  a-zA-Z]+/", $_POST['lid'])){ 
	  $name = filter_var($_POST['lid'], FILTER_SANITIZE_NUMBER_INT);
	  $name =  $_POST['lid']; 
 
	  //$db=mysql_connect  ("servername", "username",  "password") or die ('I cannot connect to the database  because: ' . mysql_error()); 
	 
	  //$mydb=mysql_select_db("yourDatabase"); 
	  mysql_select_db($database_air2013, $air2013);
	  //-query  the database table 
	   
	  $query_airtel_reports="SELECT * FROM survey WHERE lid LIKE '%" . $name .  "%'"; 
	  //-run  the query against the mysql query function 
	  $result=mysql_query($query_airtel_reports); 
	
	  while($row=mysql_fetch_array($result)){ 
	          $lid  =$row['lid']; 
 
	  
	  echo "<ul>\n"; 
	  echo "<li>" . "<a  href=\"technicaldetails.php?id=$lid\">"   .$lid . " </a></li>\n"; 
	  echo "</ul>"; 
	  } 
	  } 
	  else{ 
	  echo  "<p>Please enter a search query</p>"; 
	  } 
	  } 
	   
	?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ZUKU - <?php include "../cat.php"; ?></title>
        <link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
        <script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
        <link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
        <style type="text/css">
        @import url("../css/bootstrap/assets/css/docs.min.css");
        </style>
        <link href="../css/table_style.css" rel="stylesheet" type="text/css" />
        <link href="../css/bootstrap/assets/css/docs.min.css" rel="stylesheet" type="text/css" />
        <link href="../css/admin.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
        <script type="text/javascript" src="js/table2CSV.js" ></script>
        <script type="text/javascript" src="js/jquery.pleaseWait.js" ></script>
        <script type="text/javascript">
            $(function () {
                $('#fromdt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });

                $('#todt').datepicker({
                    dateFormat: 'yy-mm-dd',
                    firstDay: 1,
                    changeMonth: true,
                    changeYear: true,
                });
            });
        </script>
    </head>

    <body  class="css" >
        <div class="header">
            <table align="center">
                <tr>
                    <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="129" height="40" alt="Millward Airtel" /></td>
                    <td align="right" valign="top" colspan="2">
                        <form>

<?php include "menu.php"; ?>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <form id='frmsearchbar' action='' method='GET'>
                          <table align="right">
                            <tr>
                              <td>From:</td>
                              <td align="center"><input name='fromdate' required type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                              <td align="center">To:</td>
                              <td align="center"><input name='todate' required type='textbox' class="dateselection" id="todt" size='20'/></td>
                              <td align="center">Status:</td>
                              <td align="center"><select name="status" class="dropdown-item">
                                <option value="">select</option>
                                <option value="Open">Open</option>
                                <option value="In Progress">In Progress</option>
                                <option value="Escalated to Zuku" selected="selected">Escalated to Zuku</option>
                                <option value="Closed">Closed</option>
                              </select></td>
                              <td align="center"><input   type="submit" name="submit" value="GET ESCALATIONS REPORT" /></td>
                            </tr>
                          </table>
                        </form>
                    </td>

                    <td align="right">

                    </td>
                </tr>
            </table>
        </div>
<?php if ($_GET["fromdate"] != '' && $_GET["todate"] != '') { ?>
            <div class="bd-content">
                <h3>
  <script>
                    $('.content').pleaseWait();
                </script>
                  
  <!-- 

                Start Technical Summary 
                <h3 align="center">TECHNICAL SUMMARY</h3>



                

                <br/><br/>

                <!-- End Technical Summarry -->
                </h3>
                <h3 align="center" class="h2_refresh"> Escalations</h3>
                <form action="getCSVDisp.php" method ="post" style="margin-left:20%" > 
                    <input type="hidden" name="csv_text" id="csv_text2">
                        <input type="submit" alt="Submit Form" value="Download To Excel" onClick="getCSVData()" />
                </form>
                <script>
                    function getCSVData() {
                        var csv_value = $('#csvdownload2').table2CSV({delivery: 'value'});
                        $("#csv_text2").val(csv_value);
                    }
                </script>
                <h3>Search  Contacts Details</h3> 
    <p>You  may search either by account number</p> 
    	    <form  method="post" action="technicaldetails.php"  id="searchform"> 	   
               <input  type="text" name="name">  
               <input  type="submit" name="submit1" value="Search"> 
    </form> 
                <table  align="center" cellpadding="5" class="swatch-light" id="csvdownload2" style="width:50%">
                    <tr>
                        <td class="tblRBD"><span class="table-responsive">#</span></td>
                        <td class="table"><span class="table-responsive">Account No:</span></td>
                        <td class="table"><span class="table-responsive">Client Name:</span></td>
                        <td class="table"><span class="table-responsive">Phone No:</span></td>
                        <td class="table"><span class="table-responsive">Call Date:</span></td>
                        <td class="table"><span class="table-responsive">Escalations</span></td>
                        <td class="table"><span class="table-responsive">Escalations Other Comment</span></td>
                        <td class="table"><span class="table-responsive">Escalation Status:</span></td>
                        <td class="table-responsive"><span class="table-responsive">In Progress Comment:</span></td>
                        <td class="table"><span class="table-responsive">In Progress Time:</span></td>
                        <td class="table"><span class="table-responsive">Escalated to ZUKU Comment:</span></td>
                        <td class="table"><span class="table-responsive">Escalated to ZUKU Time:</span></td>
                        <td class="table"><span class="table-responsive">Close Comment:</span></td>
                        <td class="table-responsive"><span class="table-responsive">Close Time:</span></td>
                        <td class="table"><span class="table-responsive">Agent Name</span></td>
                    </tr>

    <?php $t = 0;
    do {
        $t += 1;
        ?>
                        <tr>
                            <td class="tblRD"><span class="table-responsive"><?php echo $t; ?></span></td>
                        <td class='tblRD'><span class="table-responsive"><a href="technicaldetails_1.php?id=<?php echo $row_airtel_reports['id'] ?>"><?php echo $row_airtel_reports['subs'] ?></a></b></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['name']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['phone']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['date']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['escalation']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['disptype']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['status']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['in_progress_comment']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['in_progress_time']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['escalated_to_mcu_comment']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['escalated_to_mcu_time']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['closed_comment']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['closed_time']; ?></span></td>
                            <td class="tblRD"><span class="table-responsive"><?php echo $row_airtel_reports['interviewer']; ?></span></td>
                        </tr>
    <?php } while ($row_airtel_reports = mysql_fetch_assoc($airtel_reports)); ?>
                </table> 
                </div>
            <script>
                $('.content').pleaseWait('stop');
            </script>
<?php } ?>



                                                                        <p style="margin-left:35%;class="fixed-bottom"> &copy <?php echo date('Y'); ?> Developed and Designed by Techno Brain BPO/ITES</p>

                                                                    
    </body>
</html>
        <?php
        mysql_free_result($userDets);
        mysql_free_result($airtel_reports);
        ?>
