<?php require_once('../Connections/air2013.php'); 
	error_reporting(0);?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;



$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;


if(isset($_GET["fromdate"]))
{
	
	$adwhere="update_time between '". $fromdate . "' AND '". $todate . "' AND";
}
else
{
	
	$adwhere="";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OLX SCRIPT</title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="jquery2/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="jquery2/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery2/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="jquery2/js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>

<script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="jquery2/js/jquery.highchartTable.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart')
  .bind('highchartTable.beforeRender', function (event, highChartConfig) {
        console.log(highChartConfig);

        highChartConfig.chart.events = {
            load: function () {
                var data = this.series[0].data,
                    dLen = data.length,
                    i = 0;

                while (dLen > i) {
                    var point = data[i];
                    if (point.sliced) {
                        $report.html('<h2>' + point.name + '</h2><p>' + point.percentage.toFixed(1) + '%</p>');
                        i = dLen;
                    }
                    i++;
                }

            }
        }

        highChartConfig.plotOptions = {
            pie: {
                cursor: 'pointer',
                size: '60%', // size of pieChart
                stickyTracking: false, // if false, events per slice/obj
                innerSize: '35%', // innerSize dount ring
                //allowPointSelect: true,
                dataLabels: {
                    distance: -50
                },
                center: ['35%', '50%'],
                showInLegend: true,
                slicedOffset: 15,
                shadow: 0,
                point: {
                    events: {
                        mouseOver: function (event) {
                            $report.html('<h2>' + this.name + '</h2><p>' + this.percentage.toFixed(1) + '%</p>');
                            this.select(true);
                        },
                        mouseOut: function (event) {
                            this.select(false);
                        },
                        legendItemClick: function (event) {
                            this.select(null);
                            $report.html('<h2>' + this.name + '</h2><p>' + this.percentage.toFixed(1) + '%</p>');

                            return false;
                        }
                    }
                }
            }
        },
        highChartConfig.tooltip = {
            enabled: true
        },
        highChartConfig.legend = {
            cursor: 'pointer',
            floating: true,
           
            borderColor: null,
            verticalAlign: 'top',
            x: 0,
            y: 20,
            width:400,
            itemWidth: 180
        };
    })
  .highchartTable();

});


</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	
            <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='improvements.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' type='textbox' class="dateselection" id="todt" size='20'/></td>
                    <td align="center"><input type="submit" name="submit" value="GET DASHBOARD REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
           
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>
<br>
<form>
<?php include "menu2.php";?>
</form>
  <div class="content">
 

	<div style="float:left; width:70%; margin-left:10px;margin-right:10px;padding:10px">
	 <h2>Improvement Feedback</h2>	
	 
	
	<table>
	<thead>
	<tr>
    <th class="tblRBD">#
</th>
    <th class="tblRBD">Feedback
</th>
  
    
  </tr>
  
	</thead>
	<tbody>
	<?php

	mysql_select_db($database_air2013, $air2013);
  $query_james= "SELECT q5b FROM survey WHERE $adwhere q5b<>'' AND disposation<>''  GROUP BY q5b";
 $called_james = mysql_query($query_james, $air2013) or die(mysql_error());

 $more=0;
while($row=mysql_fetch_assoc($called_james))
	
{
	$more+=1;	
echo "<tr><td class='tblR' >".$more."</td><td class='tblR' >".ucwords($row['q5b'])."</td></tr>";

}
?>
 
</tbody>
	 
	</table> 
	</div>

	
	
	
  <!-- end .content --></div>
</body>
</html>




