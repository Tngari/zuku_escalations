<?php require_once('../Connections/air2013.php'); 
ini_set(max_execution_time,0);?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OFFERS & FIXTURES - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left"><img src="../images/logo_right_small.png" width="250" height="60" alt="Mutlichoice" /></td>
          	<td align="right" valign="top">
            <form>
           	 
              <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
  	</table>
</div>

<div class="container">
  <div class="content">
   <fieldset>
   
   <legend>Offers and Fixtures</legend>
   <form method="post" enctype="multipart/form-data"  action="<?php echo $_SERVER['PHP_SELF']?>" >
   <table style="margin-left:10%; width:50%">
   <tr><td class="tblRB">Name of Offer/Fixture</td>  <td></td>  <td ><input type="text" style="width:100%" name="offer" required></input></td></tr>
    <tr><td class="tblRB">Campaign</td>  <td></td>  <td >
	<select name="campaign" required>
	<option value="All">All</option>
	<option value="GoTV">GoTV</option>
	<option value="DSTV">DSTV</option>
	<option value="Others">Others</option>
	</select>
	
	
	</td></tr>
   <tr><td class="tblRB">Description</td>  <td></td>  <td><textarea style="width:100%" rows="10" name="description" required></textarea></td></tr>
    <tr><td class="tblRB">Any Upload</td>  <td></td>  <td >
	<input type="hidden" name="MAX_FILE_SIZE" value="8000000" /> 
	<input type="file" name="filefield" id="filefield"></input></td></tr>
	 <tr><td ></td>  <td></td>  <td > <input type="submit" name="save" value='SAVE DETAILS'>  &nbsp;&nbsp; <input type="reset" value='RESET DETAILS' onclick="return confirm('Are you sure of this action of resetting the form details?')"></td></tr>
   </table>
   
   <?php
   
   if(isset($_POST['save']))
	   
	   {
		   $offer=mysql_real_escape_string($_POST['offer']);
		   $description=mysql_real_escape_string($_POST['description']);
		   $date_leo=date('Y-m-d H:i:s');
		   
		   $logged=$_SESSION['MM_Username'];
		   $campaign=mysql_real_escape_string($_POST['campaign']);
		   
		   if($_FILES['filefield']['error']>0)
			   
			   {
			
$upload="INSERT INTO offers(offer_type,description,upload_by,upload_date,campaign) VALUES('$offer','$description','$logged','$date_leo','$campaign')";
$res=mysql_query($upload) or die(mysql_error());	

if($res==true)	
{
	echo "<p style='margin-left:20%'>Upload successfull<p>";
}	
else
{
	echo "<p style='margin-left:20%'>No Upload</p>";
}
				   
			   }

			   
else if(isset($_FILES['filefield']))

{
	
$file=$_FILES['filefield'];

$upload_directory='uploads/';

$ext_str = "gif,jpg,jpeg,mp3,tiff,bmp,doc,docx,ppt,pptx,txt,pdf,xlsx";


$allowed_extensions=explode(',',$ext_str);

$max_file_size = 10485760;//10 mb remember 1024bytes =1kbytes /* check allowed extensions here */

$ext = substr($file['name'], strrpos($file['name'], '.') + 1); //get file extension from last sub string from last . character

if (!in_array($ext, $allowed_extensions) ) {
	
echo "Only ".$ext_str." files allowed to upload"; // exit the script by warning

} /* check file size of the file if it exceeds the specified size warn user */

if($file['size']>=$max_file_size){

echo "Upload only the file less than ".$max_file_size."mb  allowed to upload"; // exit the script by warning

}

//if(!move_uploaded_file($file['tmp_name'],$upload_directory.$file['name'])){

$path=md5(microtime()).'.'.$ext;

if(move_uploaded_file($file['tmp_name'],$upload_directory.$path)){

$upload="INSERT INTO offers(offer_type,description,upload_by,upload_date,upload_file,campaign) VALUES('$offer','$description','$logged','$date_leo','$path','$campaign')";
$res=mysql_query($upload) or die(mysql_error());	


if($res==true)	
{
	echo "<p style='margin-left:20%'>Upload successfull<p>";
}	
else
{
	echo "<p style='margin-left:20%'>No Upload</p>";
}
				   
			 

}

else{

echo "The file cant moved to target directory."; //file can't moved with unknown reasons likr cleaning of server temperory files cleaning

}

}

/*

Hurrey we uploaded a file to server successfully.

*/
}

?>
   </form>
   </fieldset>
   
  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
?>
