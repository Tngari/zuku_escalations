<?php
//header("location:leads.php");
?>
<?php require_once('Connections/air2013.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}

if (isset($_POST['username'])) {
  $loginUsername=$_POST['username'];
  $password=$_POST['password'];
  $MM_fldUserAuthorization = "level";
  $MM_redirectLoginSuccess = "home.php";
  $MM_redirectLoginFailed = "failed.php";
  $MM_redirecttoReferrer = true;
  mysql_select_db($database_air2013, $air2013);
  	
  $LoginRS__query=sprintf("SELECT username, password, level FROM users WHERE username=%s AND password=%s",
  GetSQLValueString($loginUsername, "text"), GetSQLValueString($password, "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $air2013) or die(mysql_error());
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
    
    $loginStrGroup  = mysql_result($LoginRS,0,'level');
    
	if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
    //declare two session variables and assign them
    $_SESSION['MM_Username'] = $loginUsername;
    $_SESSION['MM_UserGroup'] = $loginStrGroup;	      

    if (isset($_SESSION['PrevUrl']) && true) {
      $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
    }
    header("Location: " . $MM_redirectLoginSuccess );
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php include "cat.php";?></title>
<link href="css/oneColFixCtr.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<link href="css/login.css" rel="stylesheet" type="text/css" />
<script src="SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<script type="text/javascript" src="jquery/js/disable.js"></script>

</head>

<body>

<div class="container">
  <div class="content">
  	<p align="center"><img src="images/logo_right.png"></img></p>
    <p align="center">
    <form name="frmLogin" action="<?php echo $loginFormAction; ?>" enctype="multipart/form-data" method="POST">
    <fieldset>
    <legend>Log In -<?php include "cat.php";?></legend>
    <table width="500" cellpadding="5" align="center">
        	<tr>
            	<td align="right" width="50%"><label>Username:</label></td>
                <td align="left" width="50%"><input type="text" name="username" value="" id="username" size="22" /></td>
            	<td align="right" width="50%"><label>Password:</label></td>
                <td align="left" width="50%"><input type="password" name="password" value="" id="password" size="22" /></td>
          	</tr>
   		</table>     
    </fieldset>
    <fieldset >
   		<p align="center"><input type="submit" value="Login" /> <input type="reset" value="Reset" /></p>
    </fieldset>
    </form>
    
    <p align="center"><img src="images/logo.png" width="200" height="50" alt="Millward Airtel" /></p>
    <p align="center">&copy; Copyright <?php echo date('Y');?></p>
  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>