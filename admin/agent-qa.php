<?php require_once('../Connections/air2013.php'); error_reporting(0); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];


  $fromdt = $_GET["fromdate"];

$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
mysql_select_db($database_air2013, $air2013);
$query_airtel_reports = "SELECT * FROM qc GROUP BY s_date ORDER BY s_date DESC"; // WHERE s_date='".$fromdt. "'
$air2013_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());
$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
$totalRows_airtel_reports = mysql_num_rows($air2013_reports);

//QC Params
$dt=$_GET['dt'];
$agn=$_GET['agn'];
$query_qa = "SELECT avg(q1) AS q1 FROM qc WHERE s_date='".$dt."' AND agent_name='".$agn."'"; // WHERE s_date='".$fromdt. "'
$qc_avg = mysql_query($query_qa, $air2013) or die(mysql_error());
$row_olx = mysql_fetch_array($qc_avg);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>QC - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<script type="text/javascript">
  $(function(){
    $('#fromdt').datepicker({
      dateFormat: 'yy-mm-dd',
            firstDay: 1,
      changeMonth: true,
            changeYear: true,
    });
    
    $('#todt').datepicker({
      dateFormat: 'yy-mm-dd',
            firstDay: 1,
      changeMonth: true,
            changeYear: true,
    });
  });
</script>
</head>

<body>
<div class="header">
  <table align="center" width="100%">
      <tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
            <td align="right" valign="top" colspan="2" nowrap="nowrap">
            <form>
             
          <?php include "menu.php";?>
            </form>
            </td>
        </tr>
        <tr>
          <td align="right">
            <form id='frmsearchbar' action='quality-report.php' method='GET'>
      <table align="right">
          <tr>
                  <td>QC Date:</td>
                  <td align="center"><input name='fromdate' type='textbox' class="dateselection" id="fromdt" size='20' value="<?php echo $fromdt;?>" /></td>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td align="center"><input type="submit" name="submit" value="GET QC REPORTS" /></td>
                </tr>
      </table>
      </form>
        </td>
            
            <td align="right">
            <form action="getCSV.php" method ="post" > 
        <input type="hidden" name="csv_text" id="csv_text">
        <input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
      </form>
      <script>
        function getCSVData(){
        var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
        $("#csv_text").val(csv_value);
        }
      </script>
            </td>
      </tr>
    </table>
</div>
<table><tr><td> <form>
             
            <input type=button onClick="location.href='quality-report.php'" value='QC DATA'> &nbsp;   
              <input type=button onClick="location.href='agent-qa.php'" value='AGENT QA SCORE'> &nbsp;
               <input type=button onClick="location.href='qc-score.php'" value='QC STATISTICS'> &nbsp;
               <input type=button onClick="location.href='qc-rawdata.php'" value='QC RAW DATA'> &nbsp;
       
            </form></td></tr></table>
  <div class="content">
  <table id="csvdownload" style="margin-left:2%; width:30%; float:left">
  <tr>
   <td class="tblRB">Date/Agent</td>
   
   <?php 
   $qc= "SELECT agent_name FROM qc GROUP BY agent_name ";
   $qc = mysql_query($qc, $air2013) or die(mysql_error());
  
   $qc_a=array();
   while($row=mysql_fetch_assoc($qc))
   {
    $qc_a[]=$row['agent_name'];
    
   
   }
   
   foreach($qc_a as $fala)
   {
    echo '<td class="tblRB">'.ucwords($fala).'</td>';
   }
   
   ?>
    
     
    </tr>

<?php 
$t=0;
do
 { 
  $t+=1;
  
//Count agents

  
?>
  <tr>
   <td class="tblR"><?php echo $row_airtel_reports['s_date'];?></td>
   <?php 
   
   
   foreach($qc_a as $fala)
   {
  $avg= "SELECT avg(q1) as q1,avg(q2) as q2,avg(q3) as q3,avg(q4) as q4,avg(q5) as q5,avg(q6) as q6,avg(q7) as q7,avg(q8) as q8 FROM qc WHERE agent_name='$fala' AND s_date='".$row_airtel_reports['s_date']."'";
    $avg_r = mysql_query($avg, $air2013) or die(mysql_error());
     
    $row=mysql_fetch_assoc($avg_r);
   
    $t1+=$row['q1'];
    $tot=round(($row['q1']+$row['q2']+$row['q3']+$row['q4']+$row['q5']+$row['q6']+$row['q7']+$row['q8'])/8,2).'%';
    echo '<td class="tblR" align="center"><a href="agent-qa.php?dt='.$row_airtel_reports['s_date'].'&agn='.$fala.'&t1='.$row['q1'].'&t2='.$row['q2'].'&t3='.$row['q3'].'&t4='.$row['q4'].'&t5='.$row['q5'].'&t6='.$row['q6'].'&t7='.$row['q7'].'&t8='.$row['q8'].'">'.$tot.'</a></td>';
  
   }
   
   ?>
    
   
    </tr>
    <?php  } while ($row_airtel_reports = mysql_fetch_assoc($air2013_reports)); ?>
  </table> 
  
  <!--QA Param Reports--->
  <?php if(!empty($_GET['agn']))
  {
    ?>

  <table id="csvdownload" style="margin-right:5%; width:30%; float:right; ">
  <tr><td class="tblRB">Agent</td><td class="tblR"><?php echo ucwords($_GET['agn']);?></td><td class="tblRB">Date</td><td class="tblR"><?php echo $_GET['dt'];?></td></tr>
  <tr>
  <!--Welcome    
 Data Protection   
 Listening Skills    
 Soft Skills   
 Hold Procedure    
 Value Add   
 CRM detailing   
 Call closing -->
   <td class="tblRB">QC_Param</td>
   
    <td class="tblRB">Score</td>
     
    </tr>
  <tr>
   <td class="tblR"> Welcome</td>
   <td class="tblR"><?php echo $s1=round($_GET['t1'],2).'%';?></td>
   
    </tr>
  <tr>
   <td class="tblR"> Data Protection </td>
   <td class="tblR"><?php echo $s2=round($_GET['t2'],2).'%';?></td>
   
    </tr>
   <tr>
   <td class="tblR"> Listening Skills </td>
   <td class="tblR"><?php echo $s3=round($_GET['t3'],2).'%';?></td>
   
    </tr>
   <tr>
   <td class="tblR"> Soft Skills</td>
   <td class="tblR"><?php echo $s4=round($_GET['t4'],2).'%';?></td>
   
    </tr>
   <tr>
   <td class="tblR"> Hold Procedure </td>
   <td class="tblR"><?php echo $s5=round($_GET['t5'],2).'%';?></td>
   
    </tr>
    <tr>
   <td class="tblR"> Value Add</td>
   <td class="tblR"><?php echo $s6=round($_GET['t6'],2).'%';?></td>
   
    </tr>
   <tr>
   <td class="tblR"> CRM detailing  </td>
   <td class="tblR"><?php echo $s7=round($_GET['t7'],2).'%';?></td>
   
    </tr>
   <tr>
   <td class="tblR"> Call closing</td>
   <td class="tblR"><?php echo $s8=round($_GET['t8'],2).'%';?></td>
   
   
    </tr>
  
    <tr>
   <td class="tblR"> Average</td>
   <td class="tblR"><?php echo round(($s1+$s2+$s3+$s4+$s5+$s6+$s7+$s8)/8,2);?></td>
  </tr>
  </table> 
  <?php 
  }?>
  <!--End QA param Report-->
  <!-- end .content --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($air2013_reports);
?>
