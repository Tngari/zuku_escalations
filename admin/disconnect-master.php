<?php require_once('../Connections/air2013.php');
ini_set("max_execution_time",0);


include "config.php"; ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multichoice - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	
              <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id="frmsearchbar" action="disconnect-master.php" method="GET">
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' value="<?php echo $_GET['fromdate'];?>" required type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' value="<?php echo $_GET['todate'];?>" required type='textbox' class="dateselection" id="todt" size='20'/></td>
                    
                    <td align="center">Campaign:</td>
                    <td align="center"><select name="campaign">
                    <option value="<?php echo $_GET['campaign'];?>"><?php echo $_GET['campaign'];?></option>
                    <option value="GoTV">GoTV</option>
                    <option value="DSTV">DSTV</option>
                    
                    </select></td>
                    <td align="center"><input type="submit" name="submit" value="GET DATA" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
            <form action="attendance.php" method ="post" > 
				<input type="hidden" name="csv_text" id="csv_text">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>
<?php

$fromdate=$_GET['fromdate'];
$todate=$_GET['todate'];
if($fromdate=='') //stop the page from loading at first
{exit;}
$campaign=$_GET['campaign'];
if(isset($fromdate))
{
  $qry="AND date(date_loaded) BETWEEN '$fromdate' AND '$todate' AND campaign LIKE '%$campaign%'";

  $qb="AND date(update_time) BETWEEN '$fromdate' AND '$todate' AND leads.campaign LIKE '%$campaign%' AND survey.disposation<>''";
  
 // $qryl="WHERE date(date_loaded) BETWEEN '$fromdate' AND '$todate' AND campaign LIKE '%$campaign%' AND No_Days_Disconnected!='No_Days_Disconnected' AND No_Days_Disconnected<>''";
  
  //$qbl="AND date(date_loaded) BETWEEN '$fromdate' AND '$todate' AND campaign LIKE '%$campaign%' GROUP BY account";
}
else

{
   $qry="";
   $qb="AND leads.campaign LIKE '%$campaign%' AND survey.disposation<>''";
   
   //$qryl="WHERE No_Days_Disconnected!='No_Days_Disconnected' AND No_Days_Disconnected<>''";
  // $qbl="AND campaign LIKE '%$campaign%' GROUP BY account";
}


//$attend=$db->prepare("SELECT  DISTINCT (No_Days_Disconnected) as `disconnect` FROM leads $qry1  ORDER BY No_Days_Disconnected ASC");

$attend=$db->prepare("SELECT No_Days_Disconnected, count(*) AS total 
					FROM (SELECT
						  CASE WHEN No_Days_Disconnected BETWEEN 0 AND 30 THEN '30 Days'
						  WHEN No_Days_Disconnected BETWEEN 31 and 60 THEN '60 Days'
						  WHEN No_Days_Disconnected BETWEEN 61 and 90 THEN '90 Days'
						  END AS No_Days_Disconnected
						  FROM leads) leads
					GROUP BY No_Days_Disconnected");
$attend->execute();
$attend->store_result();
$attend->bind_result($disconnect,$total);

while($attend->fetch())
{
	$array[]=$disconnect;
}
?>
  <div class="content">
	<table id="csvdownload" cellpadding="5"  align="center" style="margin-left:20%; width:70%">
	<tr>
	
    <td class="tblRBD">Disposition</td>
     
     <?php foreach($array AS $value)
     { ?>
    <td class="tblRBD"><?php echo $value; ?></td>
    <?php
  }
    ?>
     <td class="tblRBD">Total</td>
     
  </tr>
<?php
/*$stmt=$db->prepare("SELECT  DISTINCT (interviewer) as `agent` FROM survey $qry ORDER BY interviewer DESC");

$stmt->execute();
$stmt->store_result();
$stmt->bind_result($agent);
$tmn=$stmt->num_rows;
$disposation='';
$t=0;
while($stmt->fetch())
{
  //total leads
$stmt2=$db->prepare("SELECT id FROM survey WHERE interviewer=? $qb AND disposation<>''");
$stmt2->bind_param('s',$agent);
$stmt2->execute();
$stmt2->store_result();
$tmn2=$stmt2->num_rows;

$t+=1;
*/
?>
 <tr>

    <td class="tblR">Data Received</td>
     
     <?php 
     $s=0;

     foreach($array AS $value)
   { 
   	
   if($value=='30 Days')
   		$disco="leads.No_Days_Disconnected<=30 AND leads.No_Days_Disconnected<>''";
  
   		if($value=='60 Days')
   	$disco="leads.No_Days_Disconnected >30 AND leads.No_Days_Disconnected <=60 AND leads.No_Days_Disconnected<>''";
   		
   			if($value=='90 Days')
   				$disco="leads.No_Days_Disconnected >60 AND leads.No_Days_Disconnected<>''";
   			
   	  	
$stmt3=$db->prepare("SELECT id FROM leads WHERE $disco  $qry");
$stmt3->bind_param('s',$value);
$stmt3->execute();
$stmt3->store_result();
$tmn3=$stmt3->num_rows;

$s+=$tmn3;
      ?>
    <td class="tblR" align="center"><?php echo number_format($tmn3); ?></td>
    <?php
  }
    ?>
     
     <td class="tblR" align="center"><?php echo number_format($s); ?></td>
  </tr>
  
  <!-- Data Dialled -->
  <tr>

    <td class="tblR">Data Dialled</td>
     
     <?php 
$s=0;
     foreach($array AS $value)
   { 	
   	if($value=='30 Days')
   		$disco="leads.No_Days_Disconnected<=30 AND leads.No_Days_Disconnected<>''";
  
   		if($value=='60 Days')
   	$disco="leads.No_Days_Disconnected >30 AND leads.No_Days_Disconnected <=60 AND leads.No_Days_Disconnected<>''";
   		
   			if($value=='90 Days')
   				$disco="leads.No_Days_Disconnected >60 AND leads.No_Days_Disconnected<>''";
   			
$stmt3=$db->prepare("SELECT survey.id FROM survey LEFT JOIN leads ON leads.id=survey.lid WHERE $disco $qb");
//$stmt3->bind_param('s',$value);
$stmt3->execute();
$stmt3->store_result();
$tmn3=$stmt3->num_rows;

$s+=$tmn3;
      ?>
    <td class="tblR" align="center"><?php echo number_format($tmn3); ?></td>
    <?php
  }
    ?>
    <td class="tblR" align="center"><?php echo number_format($s); ?></td>  
    
  </tr>
  <!-- End Data Dialled -->
  
  <!-- Contacted -->
  <tr>

    <td class="tblR">Reachable</td>
     
     <?php $s=0; foreach($array AS $value)
   { 
   	
   	if($value=='30 Days')
   		$disco="leads.No_Days_Disconnected<=30 AND leads.No_Days_Disconnected<>''";
   	
   		if($value=='60 Days')
   			$disco="leads.No_Days_Disconnected >30 AND leads.No_Days_Disconnected <=60 AND leads.No_Days_Disconnected<>''";
   			 
   			if($value=='90 Days')
   				$disco="leads.No_Days_Disconnected >60 AND leads.No_Days_Disconnected<>''";
   			
$stmt3=$db->prepare("SELECT survey.id FROM survey INNER JOIN leads ON leads.id=survey.lid 
WHERE $disco $qb AND 
(
   disposation='Already Paid'  
OR disposation='Contact details are not for the right customer' 
OR disposation='Language Barrier' 
OR disposation='Decision-maker was not available' 
OR disposation='Call Back' 
OR disposation='Complete Survey' 
OR disposation='Already Contacted' 
OR disposation='Not Interested' 
OR disposation='Partial Survey' 
OR disposation='Customer Hanged Up'

)");
$stmt3->bind_param('s',$value);
$stmt3->execute();
$stmt3->store_result();
$tmn3=$stmt3->num_rows;

$s+=$tmn3;
      ?>
    <td class="tblR" align="center"><?php echo number_format($tmn3); ?></td>
    <?php
  }
    ?>
     
     <td class="tblR" align="center"><?php echo number_format($s); ?></td>
  </tr>
  <!-- End Data Dialled -->
  <!-- End Contacted -->
  
  <!-- Non Contacted -->
  
  <tr>

    <td class="tblR">Unreachable</td>
     
     <?php $s=0;foreach($array AS $value)
   { 

   	if($value=='30 Days')
   		$disco="leads.No_Days_Disconnected<=30 AND leads.No_Days_Disconnected<>''";
   	
   		if($value=='60 Days')
   			$disco="leads.No_Days_Disconnected >30 AND leads.No_Days_Disconnected <=60 AND leads.No_Days_Disconnected<>''";
   			 
   			if($value=='90 Days')
   				$disco="leads.No_Days_Disconnected >60 AND leads.No_Days_Disconnected<>''";
   			
$stmt3=$db->prepare("SELECT survey.id FROM survey INNER JOIN leads ON leads.id=survey.lid 
WHERE $disco $qb AND (

   disposation='Silent Call' 
OR disposation='Call Drop' 
OR disposation='Switched Off' 
OR disposation='Voice mail' 
OR disposation='Call Abandoned' 
OR disposation='No answer' 
OR disposation='Number out of service' 
OR disposation='Phone busy'

) ");
$stmt3->bind_param('s',$value);
$stmt3->execute();
$stmt3->store_result();
$tmn3=$stmt3->num_rows;

$s+=$tmn3;
      ?>
    <td class="tblR" align="center"><?php echo number_format($tmn3); ?></td>
    <?php
  }
    ?>
     
     <td class="tblR" align="center"><?php echo number_format($s); ?></td>
  </tr>
  <!--End Non Contacted  -->
 <?php
//}
 ?>
	

  
  
  <!-- Summarized -->
  <?php
$dispo=array('Complete Survey','Partial Survey','Not Interested','Call Back','Already Contacted','Customer Hanged Up','Phone busy','Voice mail','Hanged up','Already Paid','No answer','Wrong Number','Switched Off','Language Barrier','Number out of Service','Call abandoned','Invalid');
?>
 <?php
 //reachable
 
if($value=='30 Days')
   		$disco="leads.No_Days_Disconnected<=30 AND leads.No_Days_Disconnected<>''";
  
   		if($value=='60 Days')
   	$disco="leads.No_Days_Disconnected >30 AND leads.No_Days_Disconnected <=60 AND leads.No_Days_Disconnected<>''";
   		
   			if($value=='90 Days')
   				$disco="leads.No_Days_Disconnected >60 AND leads.No_Days_Disconnected<>''";
 		//echo "SELECT survey.id FROM survey INNER JOIN leads ON leads.id=survey.lid WHERE  (disposation='Language Barrier' OR disposation='Call Back' OR disposation='Complete Survey' OR disposation='Customer Hanged Up' OR  disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey') $qb"; echo '<br>';
 $stmt4=$db->prepare("SELECT survey.id FROM survey INNER JOIN leads ON leads.id=survey.lid WHERE  
 (
 
   disposation='Already Paid'  
OR disposation='Contact details are not for the right customer' 
OR disposation='Language Barrier' 
OR disposation='Decision-maker was not available' 
OR disposation='Call Back' 
OR disposation='Complete Survey' 
OR disposation='Already Contacted' 
OR disposation='Not Interested' 
OR disposation='Partial Survey' 
OR disposation='Customer Hanged Up'
 
 ) $qb AND leads.No_Days_Disconnected<>''");
//$stmt4->bind_param('ss',$agent,$value);
$stmt4->execute();
$stmt4->store_result();
$tmn4=$stmt4->num_rows;


 ?>
 	<tr> <td class="tblR" align="center"><b>REACHABLE</b></td> <td class="tblRB" align="center"><b><?php echo number_format($tmn4);?></b></td><td></td> <td></td><td></td><td ></td> <td ></td><td><h3></td><td ></td><td ></td><td ></td><td ></td><td ></td><td ></td></tr>

	<tr>
	
    <td class="tblRBD">Disposition/Date</td>
   
     
     <?php foreach($array AS $value)
     { ?>
    <td class="tblRBD"><?php if($value=='') echo 'Empty'; else echo $value; ?></td>
    <?php
  }
    ?>
     <td class="tblRBD">Total</td>
  </tr>
<?php
$fromdate=$_GET['fromdate'];
$todate=$_GET['todate'];

if(isset($fromdate))
{
  $qry="WHERE date(update_time) BETWEEN '$fromdate' AND '$todate' AND disposation<>'' 
  AND (
  
   disposation='Already Paid'  
OR disposation='Contact details are not for the right customer' 
OR disposation='Language Barrier' 
OR disposation='Decision-maker was not available' 
OR disposation='Call Back' 
OR disposation='Complete Survey' 
OR disposation='Already Contacted' 
OR disposation='Not Interested' 
OR disposation='Partial Survey' 
OR disposation='Customer Hanged Up'
  
  
  ) AND cmpaign LIKE '%$campaign%'";

  $qb="AND date(update_time) BETWEEN '$fromdate' AND '$todate' AND cmpaign LIKE '%$campaign%'";
}
else

{
   $qry="WHERE disposation<>'' AND (
   
   disposation='Already Paid'  
OR disposation='Contact details are not for the right customer' 
OR disposation='Language Barrier' 
OR disposation='Decision-maker was not available' 
OR disposation='Call Back' 
OR disposation='Complete Survey' 
OR disposation='Already Contacted' 
OR disposation='Not Interested' 
OR disposation='Partial Survey' 
OR disposation='Customer Hanged Up'
   
   ) AND cmpaign LIKE '%$campaign%'";
   $qb="AND cmpaign LIKE '%$campaign%'";
}
$stmt=$db->prepare("SELECT  DISTINCT(disposation) as `agent` FROM survey $qry ORDER BY date(disposation) DESC");

$stmt->execute();
$stmt->store_result();
$stmt->bind_result($agent);
$tmn=$stmt->num_rows;
$st=0;
while($stmt->fetch())
{
  //total
$stmt2=$db->prepare("SELECT id FROM survey WHERE date(update_time)=? $qb AND disposation<>''");
$stmt2->bind_param('s',$agent);
$stmt2->execute();
$stmt2->store_result();
$tmn2=$stmt2->num_rows;

$st+=1;

if($agent=='Complete Survey')
	$s='Complete Call';
else
	$s=$agent;
?>

 <tr>

    <td class="tblR"><?php echo $s;?></td>
 
  
     <?php 
	 $m=0;
	 foreach($array AS $value)
     { 
     	
     	if($value=='30 Days')
     		$disco="leads.No_Days_Disconnected<=30 AND leads.No_Days_Disconnected<>''";
     	
     		if($value=='60 Days')
     			$disco="leads.No_Days_Disconnected >30 AND leads.No_Days_Disconnected <=60 AND leads.No_Days_Disconnected<>''";
     			 
     			if($value=='90 Days')
     				$disco="leads.No_Days_Disconnected >60 AND leads.No_Days_Disconnected<>''";
     				
//echo "SELECT survey.id FROM survey INNER JOIN leads ON leads.id=survey.lid WHERE $disco AND disposation='$agent' $qb"; echo '<br>';					
$stmt3=$db->prepare("SELECT survey.id FROM survey INNER JOIN leads ON leads.id=survey.lid WHERE $disco AND disposation=? $qb");
$stmt3->bind_param('s',$agent);
$stmt3->execute();
$stmt3->store_result();
$tmn3=$stmt3->num_rows;
$m+=$tmn3;


      ?>
    <td class="tblR" align="center"><?php echo $tmn3; ?></td>
    <?php

  }
    

    ?>
    <td class="tblR" align="center"><?php echo $m;?></td> 
    
  </tr>
 <?php
}
 ?>
 
 <!--Calculate Total-->

 <!--End Total calculate-->
 <?php
 //reachable
 


$stmt5=$db->prepare("SELECT survey.id FROM survey INNER JOIN leads ON leads.id=survey.lid  

WHERE (

   disposation='Silent Call' 
OR disposation='Call Drop' 
OR disposation='Switched Off' 
OR disposation='Voice mail' 
OR disposation='Call Abandoned' 
OR disposation='No answer' 
OR disposation='Number out of service' 
OR disposation='Phone busy'


) $qb AND leads.No_Days_Disconnected<>''");
$stmt5->bind_param('ss',$agent,$value);
$stmt5->execute();
$stmt5->store_result();
$tmn5=$stmt5->num_rows;

 ?>
 <!--End Total calculate-->

 	<tr> <td class="tblR" align="center"><b>UNREACHABLE</b></td> <td class="tblRB" align="center"><b><?php echo number_format($tmn5);?></b></td><td></td> <td></td><td></td><td ></td> <td ></td><td><h3></td><td ></td><td ></td><td ></td><td ></td><td ></td><td ></td></tr>


	<tr>
	
    <td class="tblRBD">Disposition/Date</td>
   
     
     <?php foreach($array AS $value)
     { ?>
    <td class="tblRBD"><?php if($value=='') echo 'Empty'; else echo $value; ?></td>
    <?php
  }
    ?>
     <td class="tblRBD">Total</td>
  </tr>
<?php
$fromdate=$_GET['fromdate'];
$todate=$_GET['todate'];

if(isset($fromdate))
{
  $qry="WHERE date(update_time) BETWEEN '$fromdate' AND '$todate' AND disposation<>'' AND 
  (
  
   disposation='Silent Call' 
OR disposation='Call Drop' 
OR disposation='Switched Off' 
OR disposation='Voice mail' 
OR disposation='Call Abandoned' 
OR disposation='No answer' 
OR disposation='Number out of service' 
OR disposation='Phone busy'
  
  
  ) AND cmpaign LIKE '%$campaign%'";

  $qb="AND date(update_time) BETWEEN '$fromdate' AND '$todate' AND cmpaign LIKE '%$campaign%'";
}
else

{
   $qry="WHERE disposation<>'' AND (
   
   disposation='Silent Call' 
OR disposation='Call Drop' 
OR disposation='Switched Off' 
OR disposation='Voice mail' 
OR disposation='Call Abandoned' 
OR disposation='No answer' 
OR disposation='Number out of service' 
OR disposation='Phone busy'
   
   ) AND cmpaign LIKE '%$campaign%'";
   $qb="AND cmpaign LIKE '%$campaign%'";
}
$stmt=$db->prepare("SELECT  DISTINCT(disposation) as `agent` FROM survey $qry ORDER BY date(disposation) DESC");

$stmt->execute();
$stmt->store_result();
$stmt->bind_result($agent);
$tmn=$stmt->num_rows;
$st=0;
while($stmt->fetch())
{
  //total
$stmt2=$db->prepare("SELECT id FROM survey WHERE date(update_time)=? $qb AND disposation<>''");
$stmt2->bind_param('s',$agent);
$stmt2->execute();
$stmt2->store_result();
$tmn2=$stmt2->num_rows;

$st+=1;


?>

 <tr>
 
    <td class="tblR"><?php echo $agent;?></td>
 
  
     <?php 
	 $m=0;
	 foreach($array AS $value)
     { 
     	if($value=='30 Days')
     		$disco="leads.No_Days_Disconnected<=30 AND leads.No_Days_Disconnected<>''";
     		if($value=='60 Days')
     			$disco="leads.No_Days_Disconnected >30 AND leads.No_Days_Disconnected <=60 AND leads.No_Days_Disconnected<>''"; 
     			if($value=='90 Days')
     				$disco="leads.No_Days_Disconnected >60 AND leads.No_Days_Disconnected<>''";
     			
$stmt3=$db->prepare("SELECT survey.id FROM survey INNER JOIN leads ON leads.id=survey.lid WHERE $disco AND disposation=? $qb");
$stmt3->bind_param('s',$agent);
$stmt3->execute();
$stmt3->store_result();
$tmn3=$stmt3->num_rows;
$m+=$tmn3;


      ?>
    <td class="tblR" align="center"><?php echo $tmn3; ?></td>
    <?php

  }
    

    ?>
    <td class="tblR" align="center"><?php echo $m;?></td> 
    
  </tr>
 <?php
}
 ?>
 
 <!--Calculate Total-->
 
	</table> 
  <!-- end .content --></div>
  <!-- Non Contacted Summary -->
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($airtel_reports);
?>
