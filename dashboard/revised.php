<?php require_once('../Connections/air2013.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;
$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
mysql_select_db($database_air2013, $air2013);
$query_scbretail = "SELECT * FROM airtel2nd_revised WHERE `Start Time` between '". $fromdate . "' and '". $todate . "'";
$scbretail = mysql_query($query_scbretail, $air2013) or die(mysql_error());
$row_scbretail = mysql_fetch_assoc($scbretail);
$totalRows_scbretail = mysql_num_rows($scbretail);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Airtel - Millward Campaign</title>
<link href="../css/oneColFixCtr.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="jquery/js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="table2CSV.js" > </script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../jquery/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../jquery/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div id="top">
<table cellpadding="5">
<tr>
<td><img src="../images/logo.png" width="174" height="50" alt="TechnoBrainBPO" /></td>
<td>
<form id='frmsearchbar' action='revised.php' method='GET'>
<table cellpadding="10">
	<tr>
		<td>Select Start Date:</td>
   		<td><input name='fromdate' type='textbox' id="fromdt" size='19' /></td>
		<td>Select End Date:</td>
		<td><input name='todate' type='textbox' id="todt" size='19'/></td>
 		<td><input type="submit" name="submit" value="GET AIRTEL SURVEY REPORTS" /></td>
 	</tr>
</table>
</form>
</td>
<td>
<form action="getCSV.php" method ="post" > 
<input type="hidden" name="csv_text" id="csv_text">
<input type="submit" alt="Submit Form" value="Download CSV" onclick="getCSVData()" />
</form>

<script>
function getCSVData(){
 var csv_value=$('#csvdown').table2CSV({delivery:'value'});
 $("#csv_text").val(csv_value);
}
</script>
</td>
<td><a href="index.php"><h3>UPLOAD CSV</h3></a></td>
</tr>
</table>

<table id="csvdown">
<tr>
    <td class="tblRB">X2a</td>
    <td class="tblRB">X2b</td>
    <td class="tblRB">X2c</td>
    <td class="tblRB">QNo</td>
    <td class="tblRB">DB Phone</td>
    <td class="tblRB">Respondent Name</td>
    <td class="tblRB">Interviewer ID</td>
    <td class="tblRB">Customer Type</td>
    <td class="tblRB">X3a</td>
    <td class="tblRB">X3b</td>
    <td class="tblRB">X4</td>
    <td class="tblRB">X8a</td>
    <td class="tblRB">X8b</td>
    <td class="tblRB">X8c</td>
    <td class="tblRB">X8d</td>
    <td class="tblRB">X9</td>
    <td class="tblRB">X10a</td>
    <td class="tblRB">X10b</td>
    <td class="tblRB">Q1a</td>
    <td class="tblRB">Q1b</td>
    <td class="tblRB">Q1c</td>
    <td class="tblRB">Q2_1</td>
    <td class="tblRB">Q2_2</td>
    <td class="tblRB">Q2_3</td>
    <td class="tblRB">Q2_4</td>
    <td class="tblRB">Q2_5</td>
    <td class="tblRB">Q2_6</td>
    <td class="tblRB">Q3a</td>
    <td class="tblRB">Q3b</td>
    <td class="tblRB">Q4</td>
    <td class="tblRB">Q5_1</td>
    <td class="tblRB">Q5_2</td>
    <td class="tblRB">Q5_3</td>
    <td class="tblRB">Q5_4</td>
    <td class="tblRB">Q9a</td>
    <td class="tblRB">Q9b</td>
    <td class="tblRB">Q9d_1</td>
    <td class="tblRB">Q9d_2</td>
    <td class="tblRB">Q9d_3</td>
    <td class="tblRB">Q9d_4</td>
    <td class="tblRB">Q9d_5</td>
    <td class="tblRB">Q9d_6</td>
    <td class="tblRB">Q10a</td>
    <td class="tblRB">Q10b</td>
    <td class="tblRB">Q10d_1</td>
    <td class="tblRB">Q10d_2</td>
    <td class="tblRB">Q10d_3</td>
    <td class="tblRB">Q10d_4</td>
    <td class="tblRB">Q11a</td>
    <td class="tblRB">Q11b_1</td>
    <td class="tblRB">Q11b_2</td>
    <td class="tblRB">Q11b_3</td>
    <td class="tblRB">Q11c_1</td>
    <td class="tblRB">Q11c_2</td>
    <td class="tblRB">Q11c_3</td>
    <td class="tblRB">Q14_1</td>
    <td class="tblRB">Q14_2</td>
    <td class="tblRB">Q14_3</td>
    <td class="tblRB">Q14_4</td>
    <td class="tblRB">Q14_5</td>
    <td class="tblRB">Q15</td>
    <td class="tblRB">Q16_1</td>
    <td class="tblRB">Q16_2</td>
    <td class="tblRB">Q16_3</td>
    <td class="tblRB">Q16_4</td>
    <td class="tblRB">Q16_5</td>
    <td class="tblRB">Q16_6</td>
    <td class="tblRB">Q17a_1</td>
    <td class="tblRB">Q17a_2</td>
    <td class="tblRB">Q17a_3</td>
    <td class="tblRB">Q17a_5</td>
    <td class="tblRB">Q17a_6</td>
    <td class="tblRB">Q17b_1</td>
    <td class="tblRB">Q17b_2</td>
    <td class="tblRB">Q17b_3</td>
    <td class="tblRB">Q17b_5</td>
    <td class="tblRB">Q17b_6</td>
    <td class="tblRB">Q17e</td>
    <td class="tblRB">Q18a</td>
    <td class="tblRB">Q19_1</td>
    <td class="tblRB">Q19_2</td>
    <td class="tblRB">Q19_3</td>
    <td class="tblRB">Q19_4</td>
    <td class="tblRB">Q19_5</td>
    <td class="tblRB">Q19_6</td>
    <td class="tblRB">Q20_1</td>
    <td class="tblRB">Q20_2</td>
    <td class="tblRB">Q20_3</td>
    <td class="tblRB">Q20_4</td>
    <td class="tblRB">Q21_1</td>
    <td class="tblRB">Q21_2</td>
    <td class="tblRB">Q21_3</td>
    <td class="tblRB">Q21_4</td>
    <td class="tblRB">Q21_5</td>
    <td class="tblRB">Q22</td>
    <td class="tblRB">Q23_1</td>
    <td class="tblRB">Q23_2</td>
    <td class="tblRB">Q23_3</td>
    <td class="tblRB">Q24a</td>
    <td class="tblRB">Q24b_1</td>
    <td class="tblRB">Q24b_2</td>
    <td class="tblRB">Q24b_3</td>
    <td class="tblRB">Q24b_4</td>
    <td class="tblRB">D2b</td>
    <td class="tblRB">D5b</td>
    <td class="tblRB">D6a</td>
    <td class="tblRB">D6a Currency</td>
    <td class="tblRB">D6b</td>
    <td class="tblRB">D6b Currency</td>
    <td class="tblRB">D7</td>
    <td class="tblRB">D13</td>
    <td class="tblRB">Start Time</td>
    <td class="tblRB">Stop Time</td>
    <td class="tblRB">Disposition</td>
    <td class="tblRB">Disposition Comments</td>
    <td class="tblRB">Audio File 1</td>
    <td class="tblRB">Audio File 2</td>
    <td class="tblRB">Audio File 3</td>
    <td class="tblRB">Audio File 4</td>
    <td class="tblRB">Audio File 5</td>
    <td class="tblRB">Talk Time</td>
  </tr>

<?php do { ?>
  <tr>
    <td class="tblR"><?php echo $row_scbretail['X2a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X2b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X2c']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['QNo']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['DB Phone']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Respondent Name']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Interviewer ID']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Customer Type']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X3a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X3b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X8a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X8b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X8c']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X8d']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X9']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X10a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['X10b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q1a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q1b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q1c']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q2_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q2_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q2_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q2_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q2_5']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q2_6']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q3a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q3b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q5_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q5_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q5_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q5_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q9a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q9b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q9d_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q9d_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q9d_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q9d_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q9d_5']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q9d_6']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q10a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q10b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q10d_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q10d_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q10d_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q10d_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q11a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q11b_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q11b_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q11b_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q11c_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q11c_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q11c_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q14_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q14_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q14_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q14_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q14_5']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q15']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q16_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q16_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q16_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q16_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q16_5']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q16_6']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17a_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17a_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17a_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17a_5']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17a_6']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17b_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17b_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17b_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17b_5']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17b_6']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q17e']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q18a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q19_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q19_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q19_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q19_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q19_5']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q19_6']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q20_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q20_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q20_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q20_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q21_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q21_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q21_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q21_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q21_5']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q22']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q23_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q23_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q23_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q24a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q24b_1']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q24b_2']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q24b_3']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Q24b_4']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['D2b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['D5b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['D6a']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['D6a Currency']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['D6b']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['D6b Currency']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['D7']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['D13']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Start Time']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Stop Time']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Disposition']; ?></td>
    <td class="tblR"><?php echo $row_scbretail['Disposition Comments']; ?></td>
    <td class="tblR"><a href="audiofiles/<?php echo $row_scbretail['Audio File 1']; ?>.mp3"><?php echo $row_scbretail['Audio File 1']; ?></a></td>
    <td class="tblR"><a href="audiofiles/<?php echo $row_scbretail['Audio File 2']; ?>.mp3"><?php echo $row_scbretail['Audio File 2']; ?></a></td>
    <td class="tblR"><a href="audiofiles/<?php echo $row_scbretail['Audio File 3']; ?>.mp3"><?php echo $row_scbretail['Audio File 3']; ?></a></td>
    <td class="tblR"><a href="audiofiles/<?php echo $row_scbretail['Audio File 4']; ?>.mp3"><?php echo $row_scbretail['Audio File 4']; ?></a></td>
    <td class="tblR"><a href="audiofiles/<?php echo $row_scbretail['Audio File 5']; ?>.mp3"><?php echo $row_scbretail['Audio File 5']; ?></a></td>
    <td class="tblR"><?php echo $row_scbretail['Talk Time']; ?></td>
  </tr>
  <?php } while ($row_scbretail = mysql_fetch_assoc($scbretail)); ?>

</table>    
    
</body>
</html>
<?php
mysql_free_result($scbretail);
?>
