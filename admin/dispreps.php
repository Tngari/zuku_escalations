<?php require_once('../Connections/air2013.php'); ?>
<?php

ini_set('max_execution_time',0);
ini_set('memory_limit','-1'); 

if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;
$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
mysql_select_db($database_air2013, $air2013);
$query_airtel_reports = "SELECT  survey.disposation,survey.start_time,survey.update_time,survey.interviewer,survey.discomments,survey.dbphone,survey.callbacktime,leads.account,leads.Smartcard FROM survey INNER JOIN leads ON leads.id=survey.lid WHERE update_time between '". $fromdate . "' AND '". $todate . "' AND disposation<>'' ORDER BY disposation";
$airtel_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());
$row_airtel_reports = mysql_fetch_assoc($airtel_reports);
$totalRows_airtel_reports = mysql_num_rows($airtel_reports);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multichoice - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<script type="text/javascript" src="js/jquery.pleaseWait.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	
              <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='dispreps.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' type='textbox' class="dateselection" id="todt" size='20'/></td>
                    <td align="center"><input  onclick="$('.content').pleaseWait();"  type="submit" name="submit" value="GET DISPOSITION REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
          
            </td>
     	</tr>
  	</table>
</div>

<div class="content">
<script>
   $('.content').pleaseWait();
</script>
    
<!-- Start Dispo Summary -->
  <h3 align="center">DISPOSITION SUMMARY</h3>
   
   <table width="77%" id="csvdownload"  align="center" border="0" cellspacing="0" cellpadding="5" style="width:50%">
    
  <tr class="tablebx_topbg">
  <td class="tblRB">DISPOSITION</td>
   <td class="tblRB">COUNT</td>

	  
  </tr>

			<?php 
$full_dispo= "SELECT disposation FROM survey WHERE  disposation<>'' GROUP BY disposation";
$full= mysql_query($full_dispo,$air2013) or die(mysql_error());
	
						$t=0;
						while($row=mysql_fetch_assoc($full))
						{
							$disposation=$row['disposation'];
							
							
							echo '<tr>
    <td class="tblR">'; ?>
	<?php
	if($disposation=='Complete Survey')
		echo 'Complete Call';
	else
		 echo $disposation;
	?>
	
	<?php
	echo '</td>';
			   $pls= "SELECT * FROM survey WHERE date(update_time) between '". $fromdate . "' AND '". $todate . "' AND disposation='".$disposation."'";
        $pls= mysql_query($pls, $air2013) or die(mysql_error());

	$count=mysql_num_rows($pls);
	
		echo '<td class="tblR">'.$count.'</td>';

					
	

					?>
  
    </tr>
	<?php 

	} ?>
	
  <tr class="tablebx_topbg">
    <td class="tblRB" style="font-weight:bold">Total</td>
    <td class="tblRB" style="font-weight:bold"><?php 
$total_reports = "SELECT * FROM survey WHERE date(update_time) between '". $fromdate . "' AND '". $todate . "'  AND disposation<>'' ORDER BY update_time ASC";
$total_reports = mysql_query($total_reports, $air2013) or die(mysql_error());
$row_total= mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);
	echo $total;
	
	?></td>
  
  	</tr>
	
	  <tr>
    <td class="tblRB" style="font-weight:bold">Reachable</td>
    <td class="tblRB" style="font-weight:bold"><?php 
$query_called = "SELECT  id FROM survey  AS S1 
WHERE date(update_time) between '". $fromdate . "' AND '". $todate . "' AND  
	(disposation='Language Barrier' 
	OR disposation='Call Back' 
	OR disposation='Complete Survey' 
	OR disposation='Customer Hanged Up' 
	OR disposation='Already Contacted'
	OR disposation='Already Paid'
	OR disposation='Not Interested' 
	OR disposation='Partial Survey' 
	OR disposation='Language Barrier' 
	OR disposation='Customer Hanged Up')
	
	";
 $called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
$row_called = mysql_fetch_assoc($called_reports);
echo $specific = mysql_num_rows($called_reports);
	
	?></td>
  
  	</tr>
	
	  <tr>
    <td class="tblRB" style="font-weight:bold">Un Reachable</td>
    <td class="tblRB" style="font-weight:bold"><?php 
$query_unreachable = "SELECT id FROM survey AS S1 WHERE date(update_time) between '". $fromdate . "' AND '". $todate . "' AND 
		 (disposation='Phone busy'  
		 OR disposation='Voice mail'  
		 OR disposation='No answer' 
		 OR disposation='Number out of Service'  
		 OR disposation='Switched Off'  
		 OR disposation='Call Back' )

";
 $unreachable_reports = mysql_query($query_unreachable, $air2013) or die(mysql_error());
$row_unreach = mysql_fetch_assoc($unreachable_reports);
echo $unreach = mysql_num_rows($unreachable_reports);
	
	?></td>
  
  	</tr>
	 
</table>

<br/><br/>

<!-- End Dispo Summarry -->
 <h3 align="center">DISPOSITION BREAKUP</h3>
   <form action="getCSVDisp.php" method ="post" style="margin-left:20%" > 
				<input type="hidden" name="csv_text" id="csv_text2">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload2').table2CSV({delivery:'value'});
 				$("#csv_text2").val(csv_value);
				}
			</script>
	<table id="csvdownload2" cellpadding="5"  align="center" style="width:50%">
	<tr>
	<td class="tblRBD">#</td>
    <td class="tblRBD">Phone Number</td>
	<td class="tblRBD">Customer No</td>
	<td class="tblRBD">Smartcard No</td>
    <td class="tblRBD">Agent</td>
    <td class="tblRBD">Disposation</td>
    <td class="tblRBD">Comments</td>
    <td class="tblRBD">Date</td>
     <td class="tblRBD">Time</td>
	
	<td class="tblRBD">Callback</td>
  </tr>

<?php $t=0; do { 
$t+=1;?>
  <tr>
  <td class="tblRD"><?php echo $t;?></td>
    <td class="tblRD"><?php echo $row_airtel_reports['dbphone']; ?></td>
	 <td class="tblRD"><?php echo $row_airtel_reports['account']; ?></td>
	  <td class="tblRD"><?php echo $row_airtel_reports['Smartcard']; ?></td>
    <td class="tblRD"><?php echo $row_airtel_reports['interviewer']; ?></td>
    <td class="tblRD"><?php $s=$row_airtel_reports['disposation']; 
	if($s=='Complete Survey')
		echo 'Complete Call';
	else
		echo $row_airtel_reports['disposation'];	
	?></td>
    <td class="tblRD"><?php echo $row_airtel_reports['discomments']; ?></td>
    <td class="tblRD"><?php $dt=explode(' ',$row_airtel_reports['update_time']); echo $dt[0]; ?></td>
 <td class="tblRD"><?php echo $dt[1]; ?></td>
	
	          <td class="tblRD"><?php echo $row_airtel_reports['callbacktime']; ?></td>

  	</tr>
  	<?php } while ($row_airtel_reports = mysql_fetch_assoc($airtel_reports)); ?>
	</table> 
  <!-- end .content --></div>
   <script>
   $('.content').pleaseWait('stop');
</script>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($airtel_reports);
?>
