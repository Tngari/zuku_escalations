<?php require_once('../Connections/air2013.php'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;



$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;


if(isset($_GET["fromdate"]))
{
	
	$adwhere="AND update_time between '". $fromdate . "' AND '". $todate . "' ";
}
else
{
	
	$adwhere="";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multichoice SCRIPT</title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="jquery2/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="jquery2/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery2/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="jquery2/js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	
            <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id="frmsearchbar" action="disconnect-report.php" method="GET">
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' required value="<?php echo $fromdt; ?>" type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' required value="<?php echo $todt;?>" type='textbox' class="dateselection" id="todt" size='20'/></td>
                    
                   
                    
                    <td align="center"><input type="submit" name="submit" value="GET REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
           
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>
<br>
<form>
<?php //include "menu2.php";?>
</form>
   <div class="content">
 

	<div style="float:left; width:40%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	<?php
	
		$dy=30;
			$mqr30='AND leads.No_Days_Disconnected>0 AND leads.No_Days_Disconnected<='.$dy.'';
		
		$dy=60;
				$mqr60='AND leads.No_Days_Disconnected>30 AND leads.No_Days_Disconnected<='.$dy.'';
			
			$dy=90;
					$mqr90='AND leads.No_Days_Disconnected>60 AND leads.No_Days_Disconnected<='.$dy.'';
	?>
	 <caption>Complete Calls Disconnect</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disconnect/Campaign</th>
    <th class="tblRBD">GoTV</th>
 
<th class="tblRBD">DSTV</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD" align="center">
   30 days
   </td>
   
    <td class="tblRD" align="center">
   <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='GoTV'  $adwhere 
		 AND disposation='Complete Survey' $mqr30 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$go30 = mysql_num_rows($called_reports);
		echo $a1=number_format($go30);?>
   </td>
    <td class="tblRD" align="center">
  <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='DSTV'  $adwhere 
		 AND disposation='Complete Survey' $mqr30 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$dstv30 = mysql_num_rows($called_reports);
		echo number_format($dstv30);?>
   </td>
   
   </tr>
   
   <tr>
   <td class="tblRD" align="center">
   60 days
   </td>
   
    <td class="tblRD" align="center">
   <?php 
 $query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='GoTV'  $adwhere 
		 AND disposation='Complete Survey' $mqr60 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$go60 = mysql_num_rows($called_reports);
		echo $a2=number_format($go60);?>
   </td>
    <td class="tblRD" align="center">
    <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='DSTV'  $adwhere 
		 AND disposation='Complete Survey' $mqr60 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$dstv60 = mysql_num_rows($called_reports);
		echo $b2=number_format($dstv60);?>
   </td>
   
   </tr>
   <tr>
   <td class="tblRD" align="center">
   90 days
   </td>
   
    <td class="tblRD" align="center">
   <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='GoTV'  $adwhere 
		 AND disposation='Complete Survey' $mqr90 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$go90 = mysql_num_rows($called_reports);
		echo number_format($go90);?>
   </td>
    <td class="tblRD" align="center">
   <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='DSTV'  $adwhere 
		 AND disposation='Complete Survey' $mqr90 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$dstv90 = mysql_num_rows($called_reports);
		echo $b3=number_format($dstv90);?>
   </td>
   
   </tr>
   
   <tr>
   <td class="tblRD" align="center">
   Welcome
   </td>
   
    <td class="tblRD" align="center">
   <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='Welcome'  $adwhere 
		 AND disposation='Complete Survey' AND leads.category='GoTV' ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$gowelcome = mysql_num_rows($called_reports);
		echo number_format($gowelcome);?>
   </td>
    <td class="tblRD" align="center">
   <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='Welcome'  $adwhere 
		 AND disposation='Complete Survey'  AND leads.category='DSTV' ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$dstvwelcome = mysql_num_rows($called_reports);
		echo $b3=number_format($dstvwelcome);?>
   </td>
   
   </tr>
   <tr><td class="tblRD" align="center">Total </td><td class="tblRD" align="center"><?php echo number_format($go30+$go60+$go90+$gowelcome);?></td><td class="tblRD" align="center"><?php echo number_format($dstv30+$dstv60+$dstv90+$dstvwelcome);?></td></tr>
   </table>
   
   
  <td class="tblRD" align="center">
	
	
	<?php 
	
	/*if(isset($_GET['mdays']) && !empty($_GET['mdays']))
	{
		$dy=$_GET['mdays'];
		if($dy==30)
			$mqr='AND leads.No_Days_Disconnected>0 AND leads.No_Days_Disconnected<='.$dy.'';
		
			if($dy==60)
				$mqr='AND leads.No_Days_Disconnected>30 AND leads.No_Days_Disconnected<='.$dy.'';
			
				if($dy==90)
					$mqr='AND leads.No_Days_Disconnected>60 AND leads.No_Days_Disconnected<='.$dy.'';
	}
	else 
	{
		$mqr='';
	}
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (disposation='Language Barrier' OR disposation='Call Back' OR disposation='Complete Survey' OR disposation='Customer Hanged Up' OR disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey')
		 AND disposation<>'' AND cmpaign='DSTV' $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $allcalls = mysql_num_rows($called_reports);*/
		?>
	


</div>	
	
		<div style="float:right; width:40%; margin-left:10px;margin-right:10px;padding:10px">	
	
	<?php $array=array('Content/Programmimg',
'Seasonal subscriber','Others');?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Promise to Pay </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disconnect</th>
    <th class="tblRBD">GoTV</th>
 

     <th class="tblRBD">DSTV</th>
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD" align="center">
   30 days
   </td>
   
    <td class="tblRD" align="center">
   <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='GoTV'  $adwhere 
		 AND disposation='Complete Survey' AND (q1_7='Promise to Pay' OR prmtopay='Yes') $mqr30 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$go30 = mysql_num_rows($called_reports);
		?>
				<a href="promise-pay-gotv.php?fromdate=<?php echo $_GET['fromdate'];?>&todate=<?php echo $_GET['todate'];?>&mdays=30&campn=GoTV"><?php echo $a1=number_format($go30);?></a>

   </td>
    <td class="tblRD" align="center">
  <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='DSTV'  $adwhere 
		 AND disposation='Complete Survey'AND (q1_7='Promise to Pay' OR prmtopay='Yes') $mqr30 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$dstv30 = mysql_num_rows($called_reports);
		?>
						<a href="promise-pay.php?fromdate=<?php echo $_GET['fromdate'];?>&todate=<?php echo $_GET['todate'];?>&mdays=30&campn=DSTV"><?php echo number_format($dstv30);?></a>

   </td>
   
   </tr>
   
   <tr>
   <td class="tblRD" align="center">
   60 days
   </td>
   
    <td class="tblRD" align="center">
   <?php 
 $query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='GoTV'  $adwhere 
		 AND disposation='Complete Survey' AND (q1_7='Promise to Pay' OR prmtopay='Yes') $mqr60 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$go60 = mysql_num_rows($called_reports);
		//echo $a2=number_format($go60);?>
				<a href="promise-pay-gotv.php?fromdate=<?php echo $_GET['fromdate'];?>&todate=<?php echo $_GET['todate'];?>&mdays=60&campn=GoTV"><?php echo $a1=number_format($go60);?></a>

   </td>
    <td class="tblRD" align="center">
    <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='DSTV'  $adwhere 
		 AND disposation='Complete Survey' AND (q1_7='Promise to Pay' OR prmtopay='Yes') $mqr60 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$dstv60 = mysql_num_rows($called_reports);
		//echo $b2=number_format($dstv60);?>
		<a href="promise-pay.php?fromdate=<?php echo $_GET['fromdate'];?>&todate=<?php echo $_GET['todate'];?>&mdays=60&campn=DSTV"><?php echo number_format($dstv60);?></a>

   </td>
   
   </tr>
   <tr>
   <td class="tblRD" align="center">
   90 days
   </td>
   
    <td class="tblRD" align="center">
   <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='GoTV'  $adwhere 
		 AND disposation='Complete Survey' AND (q1_7='Promise to Pay' OR prmtopay='Yes') $mqr90 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$go90 = mysql_num_rows($called_reports);
		//echo number_format($go90);?>
				<a href="promise-pay-gotv.php?fromdate=<?php echo $_GET['fromdate'];?>&todate=<?php echo $_GET['todate'];?>&mdays=90&campn=GoTV"><?php echo $a1=number_format($go90);?></a>

   </td>
    <td class="tblRD" align="center">
   <?php 
	$query_called = "SELECT S1.id FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE cmpaign='DSTV'  $adwhere 
		 AND disposation='Complete Survey' AND (q1_7='Promise to Pay' OR prmtopay='Yes') $mqr90 ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$dstv90 = mysql_num_rows($called_reports);
		//echo $b3=number_format($dstv90);?>
		<a href="promise-pay.php?fromdate=<?php echo $_GET['fromdate'];?>&todate=<?php echo $_GET['todate'];?>&mdays=90&campn=DSTV"><?php echo number_format($dstv90);?></a>

   </td>
   
   </tr>
   <tr><td class="tblRD" align="center">Total </td><td class="tblRD" align="center"><?php echo number_format($go30+$go60+$go90);?></td><td class="tblRD" align="center"><?php echo number_format($dstv30+$dstv60+$dstv90);?></td></tr>
   </table>

	</div>

	
  <!-- end .content --></div>
</body>
</html>

