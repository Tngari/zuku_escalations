<?php require_once('../Connections/air2013.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Airtel - Millward Campaign</title>
<link href="dashboard.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="top">
<table width="100%">
<tr>
<td align="left"><img src="../images/logo.png" width="200" height="50" alt="TechnoBrainBPO" /></td>
<td align="center">
UPLOAD FILES
</td>
<td align="right" width="200"><a href="reports.php">DOWNLOAD REPORTS</a></td>
</tr>
</table>
</div>

<div id="content">
<!-- Start of Tabular Data -->

<table align="center">
<tr><td align="center">
<?php
 
include "connection.php"; //Connect to Database

//Upload File
if (isset($_POST['submit'])) {
	if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
		echo "<h1>" . "File ". $_FILES['filename']['name'] ." uploaded successfully." . "</h1>";
	}
 
	//Import uploaded file to Database
	$handle = fopen($_FILES['filename']['tmp_name'], "r");
 
	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$import="INSERT into airtel_revised(X2a,X2b,X2c,QNo,`DB Phone`,`Respondent Name`,`Interviewer ID`,`Customer Type`,X3a,X3b,X4,X8a,X8b,X8c,X8d,X9,X10a,X10b,Q1a,Q1b,Q1c,Q2_1,Q2_2,Q2_3,Q2_4,Q2_5,Q2_6,Q3a,Q3b,Q4,Q5_1,Q5_2,Q5_3,Q5_4,Q9a,Q9b,Q9d_1,Q9d_2,Q9d_3,Q9d_4,Q9d_5,Q9d_6,Q10a,Q10b,Q10d_1,Q10d_2,Q10d_3,Q10d_4,Q11a,Q11b_1,Q11b_2,Q11b_3,Q11c_1,Q11c_2,Q11c_3,Q14_1,Q14_2,Q14_3,Q14_4,Q14_5,Q15,Q16_1,Q16_2,Q16_3,Q16_4,Q16_5,Q16_6,Q17a_1,Q17a_2,Q17a_3,Q17a_5,Q17a_6,Q17b_1,Q17b_2,Q17b_3,Q17b_5,Q17b_6,Q17e,Q18a,Q19_1,Q19_2,Q19_3,Q19_4,Q19_5,Q19_6,Q20_1,Q20_2,Q20_3,Q20_4,Q21_1,Q21_2,Q21_3,Q21_4,Q21_5,Q22,Q23_1,Q23_2,Q23_3,Q24a,Q24b_1,Q24b_2,Q24b_3,Q24b_4,D2b,D5b,D6a,`D6a Currency`,D6b,`D6b Currency`,D7,D13,`Start Time`,`Stop Time`,Disposition,`Disposition Comments`,`Audio File 1`,`Audio File 2`,`Audio File 3`,`Audio File 4`,`Audio File 5`,`Talk Time`) values('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]','$data[5]','$data[6]','$data[7]','$data[8]','$data[9]','$data[10]','$data[11]','$data[12]','$data[13]','$data[14]','$data[15]','$data[16]','$data[17]','$data[18]','$data[19]','$data[20]','$data[21]','$data[22]','$data[23]','$data[24]','$data[25]','$data[26]','$data[27]','$data[28]','$data[29]','$data[30]','$data[31]','$data[32]','$data[33]','$data[34]','$data[35]','$data[36]','$data[37]','$data[38]','$data[39]','$data[40]','$data[41]','$data[42]','$data[43]','$data[44]','$data[45]','$data[46]','$data[47]','$data[48]','$data[49]','$data[50]','$data[51]','$data[52]','$data[53]','$data[54]','$data[55]','$data[56]','$data[57]','$data[58]','$data[59]','$data[60]','$data[61]','$data[62]','$data[63]','$data[64]','$data[65]','$data[66]','$data[67]','$data[68]','$data[69]','$data[70]','$data[71]','$data[72]','$data[73]','$data[74]','$data[75]','$data[76]','$data[77]','$data[78]','$data[79]','$data[80]','$data[81]','$data[82]','$data[83]','$data[84]','$data[85]','$data[86]','$data[87]','$data[88]','$data[89]','$data[90]','$data[91]','$data[92]','$data[93]','$data[94]','$data[95]','$data[96]','$data[97]','$data[98]','$data[99]','$data[100]','$data[101]','$data[102]','$data[103]','$data[104]','$data[105]','$data[106]','$data[107]','$data[108]','$data[109]','$data[110]','$data[111]','$data[112]','$data[113]','$data[114]','$data[115]','$data[116]','$data[117]','$data[118]','$data[119]','$data[120]')";
 
		mysql_query($import) or die(mysql_error());
	}
 
	fclose($handle);
 
	print "Import done";
 
	//view upload form
}else {
 
	print "Upload new csv by browsing to file and clicking on Upload<br />\n";
 
	print "<form enctype='multipart/form-data' action='index.php' method='post'>";
 
	print "File name to import:<br />\n";
 
	print "<input size='50' type='file' name='filename'><br />\n";
 
	print "<input type='submit' name='submit' value='Upload'></form>";
 
}
 
?>
</td></tr></table>
<!-- End of Tabular Data -->

<p align="center"><a href="index.php">Upload Another File?</a></p>
</div>
</body>
</html>