<?php require_once('../Connections/air2013.php');

include "config.php"; ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multichoice - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	
              <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='agent-report.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' value="<?php echo $_GET['fromdate'];?>" type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' value="<?php echo $_GET['todate'];?>" type='textbox' class="dateselection" id="todt" size='20'/></td>
                    <td align="center"><input type="submit" name="submit" value="GET REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
            <form action="getCSVDisp.php" method ="post" > 
				<input type="hidden" name="csv_text" id="csv_text">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>
<?php
$dispo=array('Complete Survey','Already Paid','Partial Survey','Not Interested','Call Back','Already Contacted','Phone busy','Voice mail','Hanged up','No answer','Switched Off','Language Barrier','Number out of Service','Call abandoned','Wrong Number','Invalid');
?>
  <div class="content">
	<table id="csvdownload" cellpadding="5"  align="center">
	<tr>
    <td class="tblRBD">Agent Name</td>
     <td class="tblRBD">Total</td>
     <?php foreach($dispo AS $value)
     { ?>
    <td class="tblRBD"><?php if($value=='') echo 'Empty'; else echo $value; ?></td>
    <?php
  }
    ?>
     
  </tr>
<?php
$fromdate=$_GET['fromdate'];
$todate=$_GET['todate'];

if(isset($fromdate))
{
  $qry="WHERE date(update_time) BETWEEN '$fromdate' AND '$todate'";

  $qb="AND date(update_time) BETWEEN '$fromdate' AND '$todate'";
}
else

{
   $qry="";
   $qb="";
}
$stmt=$db->prepare("SELECT  DISTINCT (interviewer) as `agent` FROM survey $qry ORDER BY interviewer DESC");

$stmt->execute();
$stmt->store_result();
$stmt->bind_result($agent);
$tmn=$stmt->num_rows;
$disposation='';
while($stmt->fetch())
{
  //total leads
$stmt2=$db->prepare("SELECT id FROM survey WHERE interviewer=? $qb AND disposation<>''");
$stmt2->bind_param('s',$agent);
$stmt2->execute();
$stmt2->store_result();
$tmn2=$stmt2->num_rows;


?>
 <tr>
    <td class="tblR"><?php echo ucwords($agent);?></td>
    <td class="tblR" align="center"><?php echo $tmn2; ?></td>

     <?php foreach($dispo AS $value)
     { 
$stmt3=$db->prepare("SELECT id FROM survey WHERE interviewer=? AND disposation=? $qb");
$stmt3->bind_param('ss',$agent,$value);
$stmt3->execute();
$stmt3->store_result();
$tmn3=$stmt3->num_rows;
      ?>
    <td class="tblR" align="center"><?php echo $tmn3; ?></td>
    <?php
  }
    ?>
     
    
  </tr>
 <?php
}
 ?>
	</table> 
  <!-- end .content --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($airtel_reports);
?>
