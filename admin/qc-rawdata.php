<?php require_once('../Connections/air2013.php'); error_reporting(0); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$todt= $_GET["fromdate"];
if($fromdt=='')
{
	$fromdt=date('Y-m-d');
}
else 
{
	$fromdt = $_GET["fromdate"];
}
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
mysql_select_db($database_air2013, $air2013);
$query_airtel_reports = "SELECT * FROM qc WHERE s_date='".$fromdt."'";
$air2013_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());
$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
$totalRows_airtel_reports = mysql_num_rows($air2013_reports);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>QC - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2" nowrap="nowrap">
            <form>
           	 
             <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='qc-rawdata.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>Survey Date:</td>
                	<td align="center"><input name='fromdate' type='textbox' class="dateselection" id="fromdt" size='20' value="<?php echo $fromdt;?>" /></td>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td align="center"><input type="submit" name="submit" value="GET QC REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
            <form action="getCSV.php" method ="post" > 
				<input type="hidden" name="csv_text" id="csv_text">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>
<table><tr><td> <form>
           	 
            <input type=button onClick="location.href='quality-report.php'" value='QC DATA'> &nbsp; 	
              <input type=button onClick="location.href='agent-qa.php'" value='AGENT QA SCORE'> &nbsp;
               <input type=button onClick="location.href='qc-score.php'" value='QC STATISTICS'> &nbsp;
			    <input type=button onClick="location.href='qc-rawdata.php'" value='QC RAW DATA'> &nbsp;
           	</form></td></tr></table>
  <div class="content">
	<table id="csvdownload" style="margin-left:1%">
	<tr>
	 <td class="tblRB">#</td>
	  <td class="tblRB">Qnr.ID</td>
	  <td class="tblRB">Survey Date</td>
	  <td class="tblRB">Agent</td>
	   <td class="tblRB">Welcome1</td>
	    <td class="tblRB">Welcome2</td>
	     <td class="tblRB">Welcome3</td>
	      <td class="tblRB">Welcome4</td>
	       <td class="tblRB">Welcome5</td>
	        <td class="tblRB">Welcome6</td>
	         <td class="tblRB">Welcome7</td>
	          <td class="tblRB">Welcome8</td>
	           <td class="tblRB">Welcome9</td>
	            <td class="tblRB">Welcome10</td>
	             <td class="tblRB">Welcome11</td>
	              <td class="tblRB">Welcome12</td>
	               <td class="tblRB">Welcome13</td>
	                <td class="tblRB">Welcome14</td>
	                 <td class="tblRB">Welcome15</td>
	                  <td class="tblRB">Welcome16</td>
	                   <td class="tblRB">Welcome17</td>
	                     <td class="tblRB">30 days and more 1</td>
	                      <td class="tblRB">30 days and more 2</td>
	                       <td class="tblRB">30 days and more 3</td>
	                        <td class="tblRB">30 days and more 4</td>
	                         <td class="tblRB">30 days and more 5</td>
	                          <td class="tblRB">30 days and more 6</td>
	                           <td class="tblRB">30 days and more 7</td>
	                            <td class="tblRB">30 days and more 8</td>
	                             <td class="tblRB">30 days and more 9</td>
	                              <td class="tblRB">30 days and more 10</td>
	                               <td class="tblRB">30 days and more 11</td>
	                                <td class="tblRB">30 days and more 12</td>
	                                 <td class="tblRB">30 days and more 13</td>
	                                  <td class="tblRB">30 days and more 14</td>
	                                   <td class="tblRB">30 days and more 15</td>
	                                    <td class="tblRB">30 days and more 16</td>
	                                     <td class="tblRB">30 days and more 17</td>
	                                      <td class="tblRB">30 days and more 18</td>
	                                        <td class="tblRB">Inactive with credit 1</td>
	                                        <td class="tblRB">Inactive with credit 2</td>
	                                        <td class="tblRB">Inactive with credit 3</td>
	                                        <td class="tblRB">Inactive with credit 4</td>
	                                        <td class="tblRB">Inactive with credit 5</td>
	                                        <td class="tblRB">Inactive with credit 6</td>
	                                        <td class="tblRB">Inactive with credit 7</td>
	                                        <td class="tblRB">Inactive with credit 8</td>
	                                        <td class="tblRB">Inactive with credit 9</td>
	                                        <td class="tblRB">Inactive with credit 10</td>
	                                        <td class="tblRB">Inactive with credit 11</td>
	                                        <td class="tblRB">Inactive with credit 12</td>
	                                        <td class="tblRB">Inactive with credit 13</td>
	                                        <td class="tblRB">Inactive with credit 14</td>
	                                        <td class="tblRB">Inactive with credit 15</td>
	                                        <td class="tblRB">Inactive with credit 16</td>
	                                        <td class="tblRB">Inactive with credit 17</td>
	                                        <td class="tblRB">Inactive with credit 18</td>
	                                        <td class="tblRB">Inactive with credit 19</td>
	                                        <td class="tblRB">Inactive with credit 20</td>
	  <td class="tblRB">Welcome</td>
	  <td class="tblRB">Data Protection</td>
	  <td class="tblRB">Listening Skills</td>
	  
	   <td class="tblRB">Soft Skills</td>
	    <td class="tblRB">Hold Procedure </td>
	      <td class="tblRB">Value Add </td>
	      
	      <td class="tblRB">CRM detailing </td>
	     <td class="tblRB">Call closing</td>
	      <td class="tblRB">Comments</td>
		 <td class="tblRB">Average</td>
    <td class="tblRB">QC Date</td>
	   <td class="tblRB">QC</td>
	   
    </tr>

<?php 
$t=0;
do
 { 
 	$t+=1;
 	
//Count agents

?>
  <tr>
   <td class="tblR"><?php echo $t; ?></td>
    <td class="tblR"><?php echo $id=$row_airtel_reports['q_id']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['s_date']; ?></td>
     <td class="tblR"><?php echo $row_airtel_reports['agent_name']; ?></td>
      <td class="tblR"><?php echo $row_airtel_reports['welcome1']; ?></td>
        <td class="tblR"><?php echo $row_airtel_reports['welcome2']; ?></td>
          <td class="tblR"><?php echo $row_airtel_reports['welcome3']; ?></td>
            <td class="tblR"><?php echo $row_airtel_reports['welcome4']; ?></td>
              <td class="tblR"><?php echo $row_airtel_reports['welcome5']; ?></td>
                <td class="tblR"><?php echo $row_airtel_reports['welcome6']; ?></td>
                  <td class="tblR"><?php echo $row_airtel_reports['welcome7']; ?></td>
                    <td class="tblR"><?php echo $row_airtel_reports['welcome8']; ?></td>
                      <td class="tblR"><?php echo $row_airtel_reports['welcome9']; ?></td>
                        <td class="tblR"><?php echo $row_airtel_reports['welcome10']; ?></td>
                          <td class="tblR"><?php echo $row_airtel_reports['welcome11']; ?></td>
                            <td class="tblR"><?php echo $row_airtel_reports['welcome12']; ?></td>
                              <td class="tblR"><?php echo $row_airtel_reports['welcome13']; ?></td>
                                <td class="tblR"><?php echo $row_airtel_reports['welcome14']; ?></td>
                                  <td class="tblR"><?php echo $row_airtel_reports['welcome15']; ?></td>
                                    <td class="tblR"><?php echo $row_airtel_reports['welcome16']; ?></td>
                                      <td class="tblR"><?php echo $row_airtel_reports['welcome17']; ?></td>
                                         <td class="tblR"><?php echo $row_airtel_reports['30day1']; ?></td>
                          <td class="tblR"><?php echo $row_airtel_reports['30day2']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day3']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day4']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day5']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day6']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day7']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day8']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day9']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day10']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day11']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day12']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day13']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day14']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day15']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day16']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day17']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['30day18']; ?></td>
                                                                                    
                                                                                         <td class="tblR"><?php echo $row_airtel_reports['inactive1']; ?></td>
                          <td class="tblR"><?php echo $row_airtel_reports['inactive2']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive3']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive4']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive5']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive6']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive7']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive8']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive9']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive10']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive11']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive12']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive13']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive14']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive15']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive16']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive17']; ?></td>
                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive18']; ?></td>
	
	                                                               <td class="tblR"><?php echo $row_airtel_reports['inactive19']; ?></td>
	                                                                                                                              <td class="tblR"><?php echo $row_airtel_reports['inactive20']; ?></td>
	
	<td class="tblR"><?php echo $q1=$row_airtel_reports['q1'];  ?></td>
  <td class="tblR"><?php  echo $q2=$row_airtel_reports['q2']; ?></td>
    
     <td class="tblR"><?php echo $q3=$row_airtel_reports['q3']; ?></td>
     <td class="tblR"><?php echo $q4=$row_airtel_reports['q4']; ?></td>
     <td class="tblR"><?php echo $q5=$row_airtel_reports['q5']; ?></td>
     <td class="tblR"><?php echo $q6=$row_airtel_reports['q6']; ?></td>
   
     <td class="tblR"><?php echo $q7=$row_airtel_reports['q7']; ?></td>
     <td class="tblR"><?php echo $q8=$row_airtel_reports['q8']; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['qc_comment']; ?></td>
      <td class="tblR"><?php echo round (($q1+$q2+$q3+$q4+$q5+$q6+$q7+$q8)/8,2).'%'; ?></td>
       <td class="tblR"><?php echo $row_airtel_reports['qc_date']; ?></td>
       <td class="tblR"><?php echo ucwords($row_airtel_reports['qc_name']); ?></td>
      
  	</tr>
  	<?php  } while ($row_airtel_reports = mysql_fetch_assoc($air2013_reports)); ?>
	</table> 
  <!-- end .content --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($air2013_reports);
?>
