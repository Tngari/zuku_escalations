<p>1 Did the csr use correct call greeting or apply correct local call greeting as defined </p>
<p style="margin-left:30%"><input type="radio" name="t30day1" value="1" required>Yes</input>
<input type="radio" name="t30day1" value="0" required>No</input>
<input type="radio" name="t30day1" value="na" required>NA</input>
</p>
    
    <p>2 Did the csr avoid to use leading and loaded questions  </p>
<p style="margin-left:30%"><input type="radio" name="t30day2" value="1" required>Yes</input>
<input type="radio" name="t30day2" value="0" required>No</input>
<input type="radio" name="t30day2" value="na" required>NA</input>
</p>	

<p>3 Did csr capture accurate churn reason   </p>
<p style="margin-left:30%"><input type="radio" name="t30day3" value="1" required>Yes</input>
<input type="radio" name="t30day3" value="0" required>No</input>
<input type="radio" name="t30day3" value="na" required>NA</input>
</p>

<p>4 Did the csr probe churn reason (if applicable)   </p>
<p style="margin-left:30%"><input type="radio" name="t30day4" value="1" required>Yes</input>
<input type="radio" name="t30day4" value="0" required>No</input>
<input type="radio" name="t30day4" value="na" required>NA</input>
</p>

<p>5 Did the csr capture accurate churn details  </p>
<p style="margin-left:30%"><input type="radio" name="t30day5" value="1" required>Yes</input>
<input type="radio" name="t30day5" value="0" required>No</input>
<input type="radio" name="t30day5" value="na" required>NA</input>
</p>

<p>6 Was the csr knowledgeable by providing customers with the right information whenever a question was directed to them   </p>
<p style="margin-left:30%"><input type="radio" name="t30day6" value="1" required>Yes</input>
<input type="radio" name="t30day6" value="0" required>No</input>
<input type="radio" name="t30day6" value="na" required>NA</input>
</p>

<p>7 Did the csr demonstrate selling skills to winback the customer   </p>
<p style="margin-left:30%"><input type="radio" name="t30day7" value="1" required>Yes</input>
<input type="radio" name="t30day7" value="0" required>No</input>
<input type="radio" name="t30day7" value="na" required>NA</input>
</p>

<p>8 Did the csr mention the available promotion to winback the customer    </p>
<p style="margin-left:30%"><input type="radio" name="t30day8" value="1" required>Yes</input>
<input type="radio" name="t30day8" value="0" required>No</input>
<input type="radio" name="t30day8" value="na" required>NA</input>
</p>

<p>9 Did the customer promise reconnect     </p>
<p style="margin-left:30%"><input type="radio" name="t30day9" value="1" required>Yes</input>
<input type="radio" name="t30day9" value="0" required>No</input>
<input type="radio" name="t30day9" value="na" required>NA</input>
</p>

<p>10 Did the csr professional throught the call     </p>
<p style="margin-left:30%"><input type="radio" name="t30day10" value="1" required>Yes</input>
<input type="radio" name="t30day10" value="0" required>No</input>
<input type="radio" name="t30day10" value="na" required>NA</input>
</p>

<p>11 Did the csr use the script as a set guideline </p>
<p style="margin-left:30%"><input type="radio" name="t30day11" value="1" required>Yes</input>
<input type="radio" name="t30day11" value="0" required>No</input>
<input type="radio" name="t30day11" value="na" required>NA</input>
</p>

<p>12 Did the csr check the alternative contact number  </p>
<p style="margin-left:30%"><input type="radio" name="t30day12" value="1" required>Yes</input>
<input type="radio" name="t30day12" value="0" required>No</input>
<input type="radio" name="t30day12" value="na" required>NA</input>
</p>

<p>13 Did the csr check email  </p>
<p style="margin-left:30%"><input type="radio" name="t30day13" value="1" required>Yes</input>
<input type="radio" name="t30day13" value="0" required>No</input>
<input type="radio" name="t30day13" value="na" required>NA</input>
</p>

<p>14 Did the csr use customer name immediately and appropriately  </p>
<p style="margin-left:30%"><input type="radio" name="t30day14" value="1" required>Yes</input>
<input type="radio" name="t30day14" value="0" required>No</input>
<input type="radio" name="t30day14" value="na" required>NA</input>
</p>

<p>15 Did the csr end the call approprietly and indicate that the call has ended by saying goodbye </p>
<p style="margin-left:30%"><input type="radio" name="t30day15" value="1" required>Yes</input>
<input type="radio" name="t30day15" value="0" required>No</input>
<input type="radio" name="t30day15" value="na" required>NA</input>
</p>

<p>16 Did the csr offer further assistance </p>
<p style="margin-left:30%"><input type="radio" name="t30day16" value="1" required>Yes</input>
<input type="radio" name="t30day16" value="0" required>No</input>
<input type="radio" name="t30day16" value="na" required>NA</input>
</p>

<p>17 Was immediate feedback provided to the csr </p>
<p style="margin-left:30%"><input type="radio" name="t30day17" value="1" required>Yes</input>
<input type="radio" name="t30day17" value="0" required>No</input>
<input type="radio" name="t30day17" value="na" required>NA</input>
</p>

<p>18 Was coaching provided to the csr </p>
<p style="margin-left:30%"><input type="radio" name="t30day18" value="1" required>Yes</input>
<input type="radio" name="t30day18" value="0" required>No</input>
<input type="radio" name="t30day18" value="na" required>NA</input>
</p>