<?php require_once('../Connections/air2013.php'); error_reporting(0); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;
$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
mysql_select_db($database_air2013, $air2013);
$query_airtel_reports = "SELECT survey.dbphone,survey.disposation,survey.update_time FROM survey WHERE disposation<>'' GROUP BY dbphone";//INNER JOIN attempts ON airtel2nd.dbphone=attempts.phone WHERE qsno IS NOT NULL AND start_time between '". $fromdate . "' AND '". $todate . "' AND disptype <> 'No Answer' AND disptype <> 'Busy' AND disptype <> 'Invalid' AND disptype <> 'Voicemail'
$air2013_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());
$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
$totalRows_airtel_reports = mysql_num_rows($air2013_reports);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OLX SCRIPT</title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="jquery2/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="jquery2/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery2/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="jquery2/js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2" nowrap="nowrap">
            <form>
           	  <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='attempts.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' type='textbox' class="dateselection" id="todt" size='20'/></td>
                    <td align="center"><input type="submit" name="submit" value="GET REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
            <form action="getCSV.php" method ="post" > 
				<input type="hidden" name="csv_text" id="csv_text">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>

  <div class="content">
	<table id="csvdownload" align="center">
	<tr>
	  <td class="tblRB">#</td>
    <td class="tblRB">PhoneNumber</td>
    <td class="tblRB">Attempt1</td>
    <td class="tblRB">Attempt2</td>
    <td class="tblRB">Attempt3</td>
    <td class="tblRB">Attempt4</td>
    <td class="tblRB">Attempt5</td>
    <td class="tblRB">Attempt6</td>
   
    </tr>
	<?php
	$t=0;
	do 
	{
		$t+=1;
		//attempt1
mysql_select_db($database_air2013, $air2013);
$query_attempt1 = "SELECT phone,dispo,date(`date_attemp`) AS `date_done` FROM attemps WHERE phone='".$row_airtel_reports['dbphone']."'";//INNER JOIN attempts ON airtel2nd.dbphone=attempts.phone WHERE qsno IS NOT NULL AND start_time between '". $fromdate . "' AND '". $todate . "' AND disptype <> 'No Answer' AND disptype <> 'Busy' AND disptype <> 'Invalid' AND disptype <> 'Voicemail'
$air2013_attempt1= mysql_query($query_attempt1, $air2013) or die(mysql_error());
$row_attempt1= mysql_fetch_assoc($air2013_attempt1);

//attempt2
		$query_attempt2= "SELECT phone,dispo,date(`date_attemp`) AS `date_done` FROM attemps WHERE phone='".$row_airtel_reports['dbphone']."'";//INNER JOIN attempts ON airtel2nd.dbphone=attempts.phone WHERE qsno IS NOT NULL AND start_time between '". $fromdate . "' AND '". $todate . "' AND disptype <> 'No Answer' AND disptype <> 'Busy' AND disptype <> 'Invalid' AND disptype <> 'Voicemail'
$air2013_attempt2= mysql_query($query_attempt2, $air2013) or die(mysql_error());
$row_attempt2= mysql_fetch_assoc($air2013_attempt2);

//attempt3
$query_attempt3= "SELECT phone,dispo,date(`date_attemp`) AS `date_done` FROM attemps WHERE phone='".$row_airtel_reports['dbphone']."'";//INNER JOIN attempts ON airtel2nd.dbphone=attempts.phone WHERE qsno IS NOT NULL AND start_time between '". $fromdate . "' AND '". $todate . "' AND disptype <> 'No Answer' AND disptype <> 'Busy' AND disptype <> 'Invalid' AND disptype <> 'Voicemail'
$air2013_attempt3= mysql_query($query_attempt3, $air2013) or die(mysql_error());
$row_attempt3= mysql_fetch_assoc($air2013_attempt3);

//attempt4
$query_attempt4= "SELECT phone,dispo,date(`date_attemp`) AS `date_done` FROM attemps WHERE phone='".$row_airtel_reports['dbphone']."' ";//INNER JOIN attempts ON airtel2nd.dbphone=attempts.phone WHERE qsno IS NOT NULL AND start_time between '". $fromdate . "' AND '". $todate . "' AND disptype <> 'No Answer' AND disptype <> 'Busy' AND disptype <> 'Invalid' AND disptype <> 'Voicemail'
$air2013_attempt4= mysql_query($query_attempt4, $air2013) or die(mysql_error());
$row_attempt4= mysql_fetch_assoc($air2013_attempt4);

//attempt5
$query_attempt5= "SELECT phone,dispo,date(`date_attemp`) AS `date_done` FROM attemps WHERE phone='".$row_airtel_reports['dbphone']."'";//INNER JOIN attempts ON airtel2nd.dbphone=attempts.phone WHERE qsno IS NOT NULL AND start_time between '". $fromdate . "' AND '". $todate . "' AND disptype <> 'No Answer' AND disptype <> 'Busy' AND disptype <> 'Invalid' AND disptype <> 'Voicemail'
$air2013_attempt5= mysql_query($query_attempt5, $air2013) or die(mysql_error());
$row_attempt5= mysql_fetch_assoc($air2013_attempt5);

//attempt6
$query_attempt6= "SELECT phone,dispo,date(`date_attemp`) AS `date_done` FROM attemps WHERE phone='".$row_airtel_reports['dbphone']."'";//INNER JOIN attempts ON airtel2nd.dbphone=attempts.phone WHERE qsno IS NOT NULL AND start_time between '". $fromdate . "' AND '". $todate . "' AND disptype <> 'No Answer' AND disptype <> 'Busy' AND disptype <> 'Invalid' AND disptype <> 'Voicemail'
$air2013_attempt6= mysql_query($query_attempt6, $air2013) or die(mysql_error());
$row_attempt6= mysql_fetch_assoc($air2013_attempt6);

	?>
  <tr>
    <td class="tblR"><?php echo $t; ?></td>
    <td class="tblR"><?php echo $row_airtel_reports['dbphone']; ?></td>
    <td class="tblR"><?php
if($row_airtel_reports['cald']==0)
{
	echo $row_airtel_reports['update_time']; echo '<br>'; echo $row_airtel_reports['disposation']; 
}
else
{

	echo $row_attempt1['date_done']; echo '<br>'; echo $row_attempt1['dispo'];
}	?></td>
    <td class="tblR"><?php //echo $row_attempt2['date_done']; echo '<br>'; echo $row_attempt2['dispo']; ?></td>
    <td class="tblR"><?php //echo $row_attempt3['date_done']; echo '<br>'; echo $row_attempt3['dispo']; ?></td>
    <td class="tblR"><?php //echo $row_attempt4['date_done']; echo '<br>'; echo $row_attempt4['dispo']; ?></td>
    <td class="tblR"><?php //echo $row_attempt5['date_done']; echo '<br>'; echo $row_attempt5['dispo']; ?></td>
    <td class="tblR"><?php //echo $row_attempt6['date_done']; echo '<br>'; echo $row_attempt6['dispo']; ?></td>
   
  	</tr>
  	<?php 
} while ($row_airtel_reports = mysql_fetch_assoc($air2013_reports)); ?>
	</table> 
  <!-- end .content --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($air2013_reports);
?>

