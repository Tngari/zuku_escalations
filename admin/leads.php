<?php require_once('../Connections/air2013.php'); 
ini_set(max_execution_time,0);?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multichoice - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top">
            <form>
           	 
              <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
  	</table>
</div>

<div class="container">
  <div class="content">
    <table align="center" width="100%">
    	<tr>
        	<td align="center"><h1>UPLOAD MSISDN LEADS<br />
            <a href="leads.csv" target="_blank">Download CSV File Sample Here &raquo;</a></h1></td>
        </tr>
        <tr><td align="center">
<?php
//Upload File

date_default_timezone_set("Africa/Nairobi");
ini_set(max_execution_time,0);
if (isset($_POST['submit'])) {
	if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
		echo "<h1>" . "File ". $_FILES['filename']['name'] ." uploaded successfully." . "</h1>";
	}
 
	//Import uploaded file to Database
	$handle = fopen($_FILES['filename']['tmp_name'], "r");
 
 $date=date('Y-m-d H:i:s');
	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        
		
        $data1=mysql_real_escape_string(trim($data[0]));
        $data2=mysql_real_escape_string(trim($data[1]));
        $data3=mysql_real_escape_string(trim($data[2]));
        $data4=mysql_real_escape_string(trim($data[3]));
        $data5=mysql_real_escape_string(trim($data[4]));
       $data6=mysql_real_escape_string(trim($data[5]));
	   $data7=mysql_real_escape_string(trim($data[6]));
	   $data8=mysql_real_escape_string(trim($data[7]));
	   $data9=mysql_real_escape_string(trim($data[8]));
	   $data10=mysql_real_escape_string(trim($data[9]));
	   $data11=mysql_real_escape_string(trim($data[10]));
	   $data12=mysql_real_escape_string(trim($data[11]));
	   $data13=mysql_real_escape_string(trim($data[12]));
	   $data14=mysql_real_escape_string(trim($data[13]));
	   $data15=mysql_real_escape_string(trim($data[14]));
	   $data16=mysql_real_escape_string(trim($data[15]));
	   $data17=mysql_real_escape_string(trim($data[16]));
	   
	    $data18=mysql_real_escape_string(trim($data[17]));
		 $data19=mysql_real_escape_string(trim($data[18]));
$data20=mysql_real_escape_string(trim($data[19]));

//
	   if($data4=="")
	   {
		   
	   }
	   else{
	   	
	
$import="INSERT into leads(`No_Days_Disconnected`,`account`,`name`,`MSISDN`,`Email`,`alt_phone`,`Work_Phone`,`region`,`City`,`Smartcard`,`Account_Type`,`Account_Status`,`Outstanding_Balance`,`Start_Date`,`Disconnect_Date`,`Dec_Model`,`campaign`,`Decoder`,`package`,`category`,`date_loaded`) 
    values('$data1','$data2','$data3','$data4','$data5','$data6','$data7','$data8','$data9','$data10','$data11','$data12','$data13','$data14','$data15','$data16','$data17','$data18','$data19','$data20','$date')";
 
		mysql_query($import) or die(mysql_error());
	   }
	}
 
	fclose($handle);
 
	print "Import done";
 
	//view upload form
}else {
 
	print "Upload new csv by browsing to file and clicking on Upload<br />\n";
 
	print "<form enctype='multipart/form-data' action='leads.php' method='post'>";
 
	print "File name to import:<br />\n";
 
	print "<input size='50' type='file' name='filename'><br />\n";
 
	print "<input type='submit' name='submit' value='Upload'></form>";
 
}
?>
</td>
</tr>
  	</table>
    
    <p align="center"><a href="leads.php">Upload Another File?</a></p>
  <!-- end .content --></div>
  <!-- end .container --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
?>
