<?php require_once('../Connections/air2013.php');
ini_set('max_execution_time',0);
ini_set('memory_limit','-1'); ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;



$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;


if(isset($_GET["fromdate"]))
{
	
	$adwhere="update_time between '". $fromdate . "' AND '". $todate . "' AND";
}
else
{
	
	$adwhere="";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Multichoice SCRIPT</title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="jquery2/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="jquery2/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="jquery2/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="jquery2/js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>

<!--<script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script>
<script src="jquery2/js/jquery.highchartTable.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('table.highchart')
  .bind('highchartTable.beforeRender', function (event, highChartConfig) {
        console.log(highChartConfig);

        highChartConfig.chart.events = {
            load: function () {
                var data = this.series[0].data,
                    dLen = data.length,
                    i = 0;

                while (dLen > i) {
                    var point = data[i];
                    if (point.sliced) {
                        $report.html('<h2>' + point.name + '</h2><p>' + point.percentage.toFixed(1) + '%</p>');
                        i = dLen;
                    }
                    i++;
                }

            }
        }

        highChartConfig.plotOptions = {
            pie: {
                cursor: 'pointer',
                size: '60%', // size of pieChart
                stickyTracking: false, // if false, events per slice/obj
                innerSize: '35%', // innerSize dount ring
                //allowPointSelect: true,
                dataLabels: {
                    distance: -50
                },
                center: ['35%', '50%'],
                showInLegend: true,
                slicedOffset: 15,
                shadow: 0,
                point: {
                    events: {
                        mouseOver: function (event) {
                            $report.html('<h2>' + this.name + '</h2><p>' + this.percentage.toFixed(1) + '%</p>');
                            this.select(true);
                        },
                        mouseOut: function (event) {
                            this.select(false);
                        },
                        legendItemClick: function (event) {
                            this.select(null);
                            $report.html('<h2>' + this.name + '</h2><p>' + this.percentage.toFixed(1) + '%</p>');

                            return false;
                        }
                    }
                }
            }
        },
        highChartConfig.tooltip = {
            enabled: true
        },
        highChartConfig.legend = {
            cursor: 'pointer',
            floating: true,
           
            borderColor: null,
            verticalAlign: 'top',
            x: 0,
            y: 20,
            width:400,
            itemWidth: 180
        };
    })
  .highchartTable();

});


</script>-->
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	
            <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='dashboard.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' required value="<?php echo $fromdt; ?>" type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' required value="<?php echo $todt;?>" type='textbox' class="dateselection" id="todt" size='20'/></td>
                    
                    <td align="center">Days:</td>
                    <td align="center">
                    
                    <select name="mdays">
                     <option value="<?php echo $_GET['mdays'];?>"><?php echo $_GET['mdays'];?></option>
                    <option value="30">30 days</option>
                    <option value="60">60 Days</option>
                    <option value="90">90 Days</option>
                   
                    </select>
                    
                    </td>
                    <td align="center">Inactive_with_Credit</td>
                    <td align="center">
                    
                   <select name="inactive"  style="width:100px">
                     <option value="<?php echo $_GET['inactive'];?>"><?php echo $_GET['inactive'];?></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                   
					 <option value="">All</option>
                   
                    </select>
                    
                    </td>
                    <td align="center"><input type="submit" name="submit" value="GET DSTV DASHBOARD REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
           
            </td>
     	</tr>
  	</table>
</div>
<br>
<form>
<?php include "menu2.php";?>
</form>
<?php if(isset($_GET["submit"])){?>
   <div class="content">
 

	<div style="float:left; width:40%; margin-left:10px;margin-right:10px;padding:10px">
	<!-- <form action="getCSVDisp.php" method ="post" > 
				<input type="hidden" name="csv_text" id="csv_text">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>-->
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
<table id="csvdownload">
<tr><td>	
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Campaign Name  - DSTV </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
   Data Dialled
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	
	if(isset($_GET['mdays']) && !empty($_GET['mdays']))
	{
		$dy=$_GET['mdays'];
		if($dy==30)
			$mqr='AND leads.No_Days_Disconnected>0 AND leads.No_Days_Disconnected<='.$dy.'';
		
			if($dy==60)
				$mqr='AND leads.No_Days_Disconnected>30 AND leads.No_Days_Disconnected<='.$dy.'';
			
				if($dy==90)
					$mqr='AND leads.No_Days_Disconnected>60 AND leads.No_Days_Disconnected<='.$dy.'';
	}
	else 
	{
		$mqr='';
	}
	
	if(isset($_GET['inactive']) && !empty($_GET['inactive']))
	{
		if($_GET['inactive']=='Yes')
		{
			//check cleints with negative balance
			$inactive="AND (leads.Outstanding_Balance>0 OR leads.Outstanding_Balance<0)";
		}
		else if($_GET['inactive']=='No')
		{
			//check clinets with 0 or less
			$inactive="AND (leads.Outstanding_Balance='0' OR leads.Outstanding_Balance='')";
		}
			else{
				$inactive="";
			}
	}
	else
	{
		$inactive="";
	}
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (disposation='Language Barrier' OR disposation='Call Back' OR disposation='Complete Survey' OR disposation='Customer Hanged Up' OR disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $allcalls = mysql_num_rows($called_reports);
		?>
	</td>
	
	 <td class="tblRD" align="center">
	
	</td>
  	</tr>
  	<tr><td class="tblRD">No of successful calls</td><td class="tblRD" align="center">
  	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (disposation='Complete Survey')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $succcalls = mysql_num_rows($called_reports);
		?>
  	</td>
  	
  	 <td class="tblRD" align="center">
	<?php echo round(($succcalls/$allcalls)*100,2);?>
	</td>
  	
  	</tr>

<tr><td class="tblRD">Customer is unreachable</td><td class="tblRD" align="center">

<?php 
	$query_called = "
		 
		 
		 SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (disposation='Phone busy'  OR disposation='Voice mail'  OR disposation='No answer' OR disposation='Number out of Service'  OR disposation='Switched Off'  OR disposation='Call Back' )
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ORDER BY S1.id DESC";
		 
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $cpaid = mysql_num_rows($called_reports);
		?>

</td>
 <td class="tblRD" align="center">
	<?php echo round(($cpaid/$succcalls)*100,2)?>
	</td></tr>

<tr><td class="tblRD">Contact details are not for the right customer </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_7='Reconnected')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $reconnected = mysql_num_rows($called_reports);
		?>
</td>
 <td class="tblRD" align="center">
	<?php echo round(($reconnected/$succcalls)*100,2);?>
	</td></tr>

<tr><td class="tblRD">Customer has already paid and is active </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_7='Promise to Pay' OR prmtopay='Yes')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr  ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		$prtopay = mysql_num_rows($called_reports);
		?>
		<a href="promise-pay.php?fromdate=<?php echo $_GET['fromdate'];?>&todate=<?php echo $_GET['todate'];?>&mdays=<?php echo $_GET['mdays'];?>&campn=DSTV"><?php echo $prtopay;?></a>
</td>
 <td class="tblRD" align="center">
	<?php echo round(($prtopay/$succcalls)*100,2);?>
	</td></tr>
<tr>
  <td class="tblRD">Decision-maker was not available </td>
  <td class="tblRD" align="center">&nbsp;</td>
  <td class="tblRD" align="center">&nbsp;</td>
</tr>
<tr>
  <td class="tblRD">Call was incomplete </td>
  <td class="tblRD" align="center">&nbsp;</td>
  <td class="tblRD" align="center">&nbsp;</td>
</tr>
	 </tbody>
	</table> 
	
	<!--  Q2. If you have not renewed your subscription with DSTV what are you currently watching? -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>
	 If you have not renewed your subscription with DSTV what are you currently watching?
	 </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
  <th class="tblRBD">%</th>

    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">Bought a new smartcard/decoder of the same brand </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1b LIKE '%Bought a new smartcard/decoder of the same brand%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $expensive = mysql_num_rows($called_reports);
		?>
	</td>
	
	<td class="tblRD" align="center">
	<?php echo round(($expensive/$succcalls)*100,2);?>
  </td>
  	</tr>
  	<tr><td class="tblRD">Not watching TV </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1b LIKE '%Not watching TV%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $hasmoney = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($hasmoney/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">Watching only free-to-air channels</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1b LIKE '%Watching only free-to-air channels%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $schoolfee= mysql_num_rows($called_reports);
		?>

</td>
<td class="tblRD" align="center">
<?php echo round(($schoolfee/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">Switched to another pay TV service</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1b LIKE '%Switched to another pay TV service%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $hasnojob= mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($hasnojob/$succcalls)*100,2);?>
  </td></tr>



	 </tbody>
	</table> 
	
	<!-- Q3. Which other pay TV service are they using?  -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Which other pay TV service are they using? </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
GOTV
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%GOTV%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr><td class="tblRD">Startimes</td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Startimes%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">StarSat</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%StarSat%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr><td class="tblRD">Zuku over fiber</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Zuku over fiber%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">Zuku over satellite</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Zuku over satellite%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Azam TV</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Azam TV%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Zap over satellite</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Zap over satellite%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Zap over TV Cabo</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Zap over TV Cabo%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">TV Cabo (not Zap or DStv)</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%TV Cabo (not Zap or DStv)%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">CanalSat</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%CanalSat%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Bein Sports</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Bein Sports%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Pirated services</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Pirated services%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Others</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1c LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table> 
    
    
    
    
    
  
  	<!-- Q4.a) Main (Primary) reason for having disconnected: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Main (Primary) reason for having disconnected:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">Payment reasons </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Payment reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">Financial reasons /cost is too high </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Financial reasons /cost is too high%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">External influences </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%External influences%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">Billing reasons </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Billing reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">Technical reasons </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Technical reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Prefers a competitor service instead </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Prefers a competitor service instead%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Customer service </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Customer service%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Electricity issues /Fuel scarcity </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Electricity issues /Fuel scarcity%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Value /Content relevance </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Value /Content relevance%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Other </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1d LIKE '%Other%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table> 
    
    

  	<!-- Q4.b) Secondary reason for having disconnected: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Secondary reason for having disconnected:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Payment reasons 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Payment reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Financial reasons /cost is too high
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Financial reasons /cost is too high%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    External influences </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%External influences%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Billing reasons </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Billing reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Technical reasons</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Technical reasons%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Prefers a competitor service instead </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Prefers a competitor service instead%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Customer service </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Customer service%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Electricity issues /Fuel scarcity </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Electricity issues /Fuel scarcity%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Value /Content relevance </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Value /Content relevance%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Other reasons </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1e LIKE '%Other%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table> 
    
    
    
  	<!-- Q5. Payment reasons: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Payment reasons:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     No time to make payment
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1f LIKE '%No time to make payment%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Inconvenient to make payment
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1f LIKE '%Inconvenient to make payment%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Did not know the disconnect date</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1f LIKE '%Did not know the disconnect date%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Forgot to pay</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1f LIKE '%Forgot to pay%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Prefers subscription to finish before paying </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1f LIKE '%Prefers subscription to finish before paying%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1f LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table> 

    
  	<!-- Q6. Financial reasons/Cost: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Financial reasons/Cost: </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Too expensive – luxury I cannot afford 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1g LIKE '%Too expensive – luxury I cannot afford%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Have other financial responsibility I need to prioritize 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1g LIKE '%Have other financial responsibility I need to prioritize%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Lost his job / lower income</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1g LIKE '%Lost his job / lower income%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    I have not been paid yet </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1g LIKE '%I have not been paid yet%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Too expensive / service is not worth the cost </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1g LIKE '%Too expensive / service is not worth the cost%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Installation cost was too high after I moved and not worth it </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1g LIKE '%Installation cost was too high after I moved and not worth it%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Was paying school fees or rent</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1g LIKE '%Was paying school fees or rent%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>   
  

  	<!-- Q7. Did they consider downgrading before making a decision to disconnect the service (Do not ask Access)? -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Did they consider downgrading before making a decision to disconnect the service (Do not ask Access)?</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Did not consider – was not aware of other packs
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1h LIKE '%Did not consider – was not aware of other packs%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Considered – but key channels not available on lower packs
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1h LIKE '%Considered – but key channels not available on lower packs%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Considered – but was not aware how</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1h LIKE '%Considered – but was not aware how%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Considered – but haven&rsquo;t yet downgraded</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1h LIKE '%Considered – but havent yet downgraded%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
	 </tbody>
	</table>   
  

  	<!-- Q8. External influences -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>External influences</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Had to travel for a while  
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1i LIKE '%Had to travel for a while%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    No time to watch / watching too little
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1i LIKE '%No time to watch / watching too little%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Experience high level of power outages </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1i LIKE '%Experience high level of power outages%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Moved homes and don&rsquo;t need anymore</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1i LIKE '%Moved homes and dont need anymore%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    No longer wants to watch TV</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1i LIKE '%No longer wants to watch TV%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Lifestyle change (Kids not at home, death, divorce, got married, job requirement etc.…)</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1i LIKE '%Lifestyle change%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1i LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
    
  
  
  	<!-- Q9. Billing reasons: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Billing reasons:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Incorrect disconnection 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1j LIKE '%Incorrect disconnection%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Paid, but not reconnected and resulted in having to view for less than 30 days 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1j LIKE '%Paid, but not reconnected and resulted in having to view for less than 30 days%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Paid, but not reflected in the account- </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1j LIKE '%Paid, but not reflected in the account%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Paid, but was an incorrect amount - </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1j LIKE '%Paid, but was an incorrect amount%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1j LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
    
  
  	<!-- Q10. Technical reasons: -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Technical reasons:</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Poor signal due to weather conditions 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Poor signal due to weather conditions%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Poor signal in my area 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Poor signal in my area%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Decoder/accessories/etc. not working</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Decoder/accessories/etc. not working%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Could not clear error codes/channels missing</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Could not clear error codes/channels missing%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
    
  
  
   

	</td></tr></table>
	</div>
	
	
	<div style="float:right; width:40%; margin-left:10px;margin-right:10px;padding:10px">
        
        
        
    <!--WORK ITEM TEST THIS!!! -->
  
  	<!-- Q11. Reason for preferring a competitor service instead  -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption> Reason for preferring a competitor service instead</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Poor signal due to weather conditions 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Poor signal due to weather conditions%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Poor signal in my area 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Poor signal in my area%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Decoder/accessories/etc. not working</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Decoder/accessories/etc. not working%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Could not clear error codes/channels missing</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Could not clear error codes/channels missing%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1k LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
        
        
        	

  	<!-- Q12. Customer service  -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption> Customer service</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">Issues with customer support when we had a query</td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1l LIKE '%Issues with customer support when we had a query%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Couldn&rsquo;t get hold of customer service when I had a billing/technical query
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1l LIKE '%Couldnt get hold of customer service when I had a billing/technical query%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Difficult to get support from DSTV/GOTV customer service </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1l LIKE '%Difficult to get support from DSTV/GOTV customer service%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Not enough flexibility in terms of payment terms/fractional payments </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1l LIKE '%Not enough flexibility in terms of payment terms/fractional payments%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    I paid and was not reconnected immediately waited for 2/3 days was never reimbursed 
    <div>
      <div> </div>
    </div></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1l LIKE '%I paid and was not reconnected immediately waited for 2/3 days was never reimbursed%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    No easy way to pay for monthly subscription </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1l LIKE '%No easy way to pay for monthly subscription%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">Others </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1l LIKE '%Others%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     
  
  
  	<!-- Q13. Reasons for preferring Value/Content Value  -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Reasons for preferring Value/Content Value</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Not watching enough to justify the cost 
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Not watching enough to justify the cost%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Subscribed for a while to watch a specific sporting event 
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Subscribed for a while to watch a specific sporting event%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Subscribed for a while to watch a specific programme or channel</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Subscribed for a while to watch a specific programme or channel%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Can get the same programme from another provider for less money </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Can get the same programme from another provider for less money%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Too many repeated programmes</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Too many repeated programmes%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Too many old movies, series etc…</td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Too many old movies, series etc%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">
    Favourite TV programme no longer available </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Favourite TV programme no longer available%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD"><ol>
    Not enough interesting local sports 
  </ol></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Not enough interesting local sports%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD"><ol>
    Not enough interesting international sports 
  </ol></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Not enough interesting international sports%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD"><ol>
    Not enough local content
  </ol></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%Not enough local content%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD"><ol>
    No rewards for being loyal 
  </ol></td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%No rewards for being loyal%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
<tr>
  <td class="tblRD">No rebate is offered when specific channels or programmes are no longer available  </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1m LIKE '%No rebate is offered when specific channels or programmes are no longer available%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>       
  
  
   	<!-- Q14 a. DBC - Alternative contact number checked? -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>DBC - Alternative contact number checked?</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Confirmed e-mail
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1n LIKE '%Confirmed e-mail%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Modified/added e-mail
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1n LIKE '%Modified/added e-mail%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Does not have e-mail</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1n LIKE '%Does not have e-mail%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Did not check </td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1n LIKE '%Did not check%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
	 </tbody>
	</table>       
  
  
   	<!-- Q14 b. DBC - E-mail checked? -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>DBC - E-mail checked?</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Confirmed mobile
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1o LIKE '%Confirmed mobile%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Modified/added mobile
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1o LIKE '%Modified/added mobile%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Did not check</td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1o LIKE '%Did not check%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>
	 </tbody>
	</table>       
   

  
   	<!-- Q15. Expected outcome of the call: the customer was… -->
	<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Expected outcome of the call: the customer was</caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>

  <tr>
   <td class="tblRD">
     Not happy with the call and is <u>unlikely to re-connect</u>
   </td>
   
  <td class="tblRD" align="center">
	
	
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1p LIKE '%Not happy with the call and is <u>unlikely to re-connect%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $travelled = mysql_num_rows($called_reports);
		?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($travelled/$succcalls)*100,2)?>
  </td>
  	</tr>
  	<tr>
  	  <td class="tblRD">
  	    Happy with the call but it&rsquo;s <u>unclear if they will re-connect</u>
	    </td><td class="tblRD" align="center">
  	<?php 
		$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1p LIKE '%Happy with the call but its unclear if they will re-connect%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $notimewatch = mysql_num_rows($called_reports);
		?>
  	</td>
  	<td class="tblRD" align="center">
  	<?php echo round(($notimewatch/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Happy with the call and is expected to <u>re-connect immediately</u></td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1p LIKE '%Happy with the call and is expected to re-connect immediately%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $sick = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">
<?php echo round(($sick/$succcalls)*100,2);?>
  </td></tr>

<tr>
  <td class="tblRD">
    Happy with the call and is expected to <u>re-connect eventually</u></td><td class="tblRD" align="center">

<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1p LIKE '%Happy with the call and is expected to re-connect eventually%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?>
</td>
<td class="tblRD" align="center">

<?php echo round(($holiday/$succcalls)*100,2);?>
  </td></tr>
<tr>
  <td class="tblRD">
    Don&rsquo;t know/not sure </td>
  <td class="tblRD" align="center"><?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (q1_1p LIKE '%Dont know/not sure%')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $holiday = mysql_num_rows($called_reports);
		?></td>
  <td class="tblRD" align="center"><?php echo round(($holiday/$succcalls)*100,2);?></td>
  </tr>
	 </tbody>
	</table>     

	<!-- Escalated -->
	
	<?php $array=array('Yes',
'No'
	);?>

<table  width="500px" data-graph-container-before="1" data-graph-type="column"  align="right">
	 <caption>Escalated - DSTV </caption>
	  <thead>
	<tr>
    <th class="tblRBD">Disposition</th>
    <th class="tblRBD">Count</th>
 
<th class="tblRBD">%</th>
    
  </tr>
    </thead>
	 <tbody>
<?php foreach($array as $arrayval)
{?>
  <tr>
   <td class="tblRD">
<?php echo $arrayval;?>
   </td>
   
  <td class="tblRD" align="center">
	<?php 
	$query_called = "SELECT * FROM survey AS S1 INNER JOIN leads ON leads.id=S1.lid WHERE $adwhere (escalated ='$arrayval')
		 AND disposation<>'' AND cmpaign='DSTV' $inactive $mqr ORDER BY S1.id DESC";
		$called_reports = mysql_query($query_called, $air2013) or die(mysql_error());
		$row_called = mysql_fetch_assoc($called_reports);
		echo $escalated = mysql_num_rows($called_reports);?>
	</td>
	<td class="tblRD" align="center">
	<?php echo round(($escalated/$succcalls)*100,2);?>
	</td>
	</tr>
	<?php } ?>
	
	</tbody>
	</table>

	<!-- Reconnected -->
	
	
	<?php $array=array('Yes',
'No'
	);?>

	<!--DSTV Promo-->
	
	
	<!--DSTV Promo-->
	
	<!--Package Promo-->
	
	
	
	<!--End Package Promo-->
	</div>
	

	<!--  <div style="float:right; width:50%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="right">
	 <caption>Reachable Contact Status</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		 /*$full= "SELECT disposation FROM survey WHERE  $adwhere (disposation='Call Back' OR disposation='Customer Hanged Up' OR disposation='Complete Survey' OR  disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey') GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1  WHERE $adwhere (disposation='Call Back' OR disposation='Customer Hanged Up' OR disposation='Complete Survey' OR  disposation='Already Contacted' OR disposation='Not Interested' OR disposation='Partial Survey') ORDER BY S1.id DESC";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1  WHERE $adwhere disposation='".$value."'  ORDER BY S1.id DESC";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div> -->
	<!-- Start all Diposations -->
	<!-- <div style="float:left; width:60%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="left">
	 <caption>All Diposations</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		/* $full= "SELECT disposation FROM survey WHERE  $adwhere  disposation<>'' GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1  WHERE $adwhere  disposation<>'' ORDER BY S1.id DESC";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1  WHERE $adwhere disposation='".$value."' ORDER BY S1.id DESC";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div>-->
	<!-- Start Unreachables -->
	<!--<div style="float:right; width:30%; margin-left:10px;margin-right:10px;padding:10px">	
	<table  width="10px" class="highchart"  data-graph-container-before="1" data-graph-type="column"  style="display:none" align="right">
	 <caption>Unreachable Contact Status</caption>
	  <thead>
	<tr>
     <th class="tblRBD">Disposition</th>
   <th class="tblRBD">Disposition Rate(%)</th>

    
  </tr>
    </thead>
	 <tbody>

	<?php 
		/* $full= "SELECT disposation FROM survey WHERE  $adwhere (disposation='Wrong Number' OR disposation='Voice Mail' OR disposation='Switched Off' OR disposation='No Answer' OR disposation='Invalid' OR disposation='Language Barrier' OR disposation='Hang up' OR disposation='Call abandoned' OR disposation='Busy' OR disposation='Number out of Service') GROUP BY disposation";
$full= mysql_query($full, $air2013) or die(mysql_error());
	   while($rows=mysql_fetch_assoc($full))
	   {
		     $value=$rows['disposation'];
			 		echo '<tr>
    <td class="tblR">'.$value.'</td>';	

$query_total = "SELECT * FROM survey AS S1 WHERE $adwhere (disposation='Wrong Number' OR disposation='Switched Off' OR disposation='Voice mail' OR disposation='Wrong Number' OR disposation='Call Abandoned' OR disposation='Hanged up' OR disposation='Language Barrier' OR disposation='No answer' OR disposation='Number out of service' OR disposation='Phone busy') ORDER BY S1.id DESC";
 $total_reports = mysql_query($query_total, $air2013) or die(mysql_error());
$row_total = mysql_fetch_assoc($total_reports);
$total=mysql_num_rows($total_reports);




$query_disp = "SELECT * FROM survey AS S1 WHERE $adwhere disposation='".$value."' ORDER BY S1.id DESC";
 $disp_reports = mysql_query($query_disp, $air2013) or die(mysql_error());
$row_disp = mysql_fetch_assoc($disp_reports);
$successful=mysql_num_rows($disp_reports);

$perc=round((($successful/$total)*100),2);
echo '<td class="tblR">'.$perc.'%</td>';*/
					?>
  
    </tr>
	<?php 

	//} ?>
	 </tbody>
	</table> 
	</div>-->
  <!-- end .content --></div>
<?php }?>
</body>
</html>

