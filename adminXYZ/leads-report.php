<?php require_once('../Connections/air2013.php');

include "config.php"; ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DT DOBIE - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2">
            <form>
           	 <?php include "menu.php";?>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="right">
            <form id='frmsearchbar' action='leads-report.php' method='GET'>
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' type='textbox' class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' type='textbox' class="dateselection" id="todt" size='20'/></td>
                    <td align="center"><input type="submit" name="submit" value="GET DISPOSITION REPORTS" /></td>
                </tr>
			</table>
			</form>
    		</td>
            
            <td align="right">
            <form action="getCSVDisp.php" method ="post" > 
				<input type="hidden" name="csv_text" id="csv_text">
				<input type="submit" alt="Submit Form" value="Download To Excel" onclick="getCSVData()" />
			</form>
			<script>
				function getCSVData(){
 				var csv_value=$('#csvdownload').table2CSV({delivery:'value'});
 				$("#csv_text").val(csv_value);
				}
			</script>
            </td>
     	</tr>
  	</table>
</div>

  <div class="content">
	<table id="csvdownload" cellpadding="5"  align="center">
  <tr>
    <td class="tblRBD">Date</td>
    <td class="tblRBD">Total</td>
    <td class="tblRBD">Pending</td>
    <td class="tblRBD">Reachable</td>
    <td class="tblRBD">Unreachable</td>
  </tr>
<?php
$fromdate=$_GET['fromdate'];
$todate=$_GET['todate'];

if(isset($fromdate))
{
  $qry="WHERE date(date_loaded) BETWEEN '$fromdate' AND '$todate'";
}
else

{
   $qry="";
}
$stmt=$db->prepare("SELECT  DISTINCT date(date_loaded) FROM leads $qry ORDER BY date(date_loaded) DESC");

$stmt->execute();
$stmt->store_result();
$stmt->bind_result($date_loaded);
$tmn=$stmt->num_rows;
while($stmt->fetch())
{
  //total leads
  $date_l=$date_loaded;
  $msdn='Mobile';
$stmt2=$db->prepare("SELECT * FROM leads WHERE date(date_loaded)=? AND  MSISDN<>?");
$stmt2->bind_param('ss',$date_l,$msdn);
$stmt2->execute();
$stmt2->store_result();
$tmn2=$stmt2->num_rows;

//Pending Leads
$dilled='N';
$stmt3=$db->prepare("SELECT * FROM leads WHERE date(date_loaded)=? AND DIALLED=? AND  MSISDN<>?");
$stmt3->bind_param('sss',$date_l,$dilled,$msdn);
$stmt3->execute();
$stmt3->store_result();
$tmn3=$stmt3->num_rows;

//Reachables
$dilled='Y';

$stmt4=$db->prepare("SELECT DISTINCT(id) FROM leads WHERE date(date_loaded)=? AND leads.DIALLED=? AND STATUS IN('Already Contacted','Complete Survey','Not Interested','Partial Survey','Call Back')");
$stmt4->bind_param('ss',$date_l,$dilled);
$stmt4->execute();
$stmt4->store_result();
$stmt4->bind_result($id);
$tmn4=$stmt4->num_rows;

//Unreachables
$stmt5=$db->prepare("SELECT DISTINCT(id) FROM leads  WHERE date(leads.date_loaded)=? AND leads.DIALLED=? AND STATUS IN('Invalid','Switched Off','Language Barrier','Call abandoned','Busy','Number out of Service','No answer','Hanged up','Voice mail','Phone busy','IS NULL')");
$stmt5->bind_param('ss',$date_l,$dilled);
$stmt5->execute();
$stmt5->store_result();
$stmt5->bind_result($id);
$tmn5=$stmt5->num_rows;
?>
 <tr>
    <td class="tblR"><?php echo $date_loaded;?></td>
    <td class="tblR" align="center"><a href="_leads.php?d=<?php echo $date_loaded;?>"><?php echo $tmn2;?></a></td>
    <td class="tblR" align="center"><?php echo $tmn3;?></td>
    <td class="tblR" align="center"><?php echo $tmn4;?></td>
    <td class="tblR" align="center"><a href="_unrechable.php?d=<?php echo $date_loaded;?>"><?php echo $tmn5;?></a></td>
  </tr>
 <?php
}
 ?>
  </table> 
  <!-- end .content --></div>
<!-- Go to www.addthis.com/dashboard to customize your tools -->

</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($airtel_reports);
?>
