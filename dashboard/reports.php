<?php require_once('../Connections/air2013.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Airtel - Millward Campaign</title>
<link href="../css/oneColFixCtr.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="../jquery/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../jquery/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>

<form id='frmsearchbar' action='revised.php' method='GET'>
<table width="100%">
<tr><td align="center">
	<table cellpadding="20" cellspacing="10" align="center">
	<tr><td colspan="2" height="50">&nbsp;</td></tr>
    <tr><td colspan="2"><h1>AIRTEL REVISED REPORTS</h1></td></tr>
    <tr>
		<td align="right">From:</td>
   		<td><input name='fromdate' type='textbox' class="dateselection" id="fromdt" size='25' /></td>
 	</tr>
    <tr>
		<td align="right">To:</td>
		<td><input name='todate' type='textbox' class="dateselection" id="todt" size='25'/></td>
  	</tr>
	<tr><td>&nbsp;</td><td><input type="submit" name="submit" value="GET AIRTEL SURVEY REPORTS" /></td>
	</table>
</td></tr></table>
</form>
</body>
</html>
