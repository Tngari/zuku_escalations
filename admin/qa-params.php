<?php require_once('../Connections/air2013.php'); error_reporting(0);

?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];

if($fromdt=='')
{
	$fromdt=date('Y-m-d');
}
else 
{
	$fromdt = $_GET["fromdate"];
}
$me_id=$_GET['id'];
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
mysql_select_db($database_air2013, $air2013);
$query_airtel_reports = "SELECT survey.*,leads.No_Days_Disconnected,leads.Outstanding_Balance,leads.campaign FROM survey INNER JOIN leads ON leads.id=survey.lid WHERE date(update_time)='".$fromdt. "' AND  survey.disposation='Complete Survey' AND survey.id='$me_id'";
$air2013_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());
$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
$totalRows_airtel_reports = mysql_num_rows($air2013_reports);

if(isset($_POST['save']))
{
	$q1=$_POST['q1'];
	$q2=$_POST['q2'];
	$q3=$_POST['q3'];
	$q4=$_POST['q4'];
	$q5=$_POST['q5'];
	$q6=$_POST['q6'];
	$q7=$_POST['q7'];
	$q8=$_POST['q8'];
    $sum=$_POST['q8'];
    $fromdate=$_POST['fromdate'];
    $s_id=$_POST['s_id'];
    $agent=$_POST['agent'];
    $mqc=$_POST['mqc'];
    
    //3odays
    
  $welcome1=$_POST['welcome1'];
  $welcome2=$_POST['welcome2'];
  $welcome3=$_POST['welcome3'];
  $welcome4=$_POST['welcome4'];
  $welcome5=$_POST['welcome5'];
  $welcome6=$_POST['welcome6'];
  $welcome7=$_POST['welcome7'];
  $welcome8=$_POST['welcome8'];
  $welcome9=$_POST['welcome9'];
  $welcome10=$_POST['welcome10'];
  $welcome11=$_POST['welcome11'];
  $welcome12=$_POST['welcome12'];
  $welcome13=$_POST['welcome13'];
  $welcome14=$_POST['welcome14'];
  $welcome15=$_POST['welcome15'];
  $welcome16=$_POST['welcome16'];
  $welcome17=$_POST['welcome17'];
  
  //inactive

  $inactive1=$_POST['inactive1'];
  $inactive2=$_POST['inactive2'];
  $inactive3=$_POST['inactive3'];
  $inactive4=$_POST['inactive4'];
  $inactive5=$_POST['inactive5'];
  $inactive6=$_POST['inactive6'];
  $inactive7=$_POST['inactive7'];
  $inactive8=$_POST['inactive8'];
  $inactive9=$_POST['inactive9'];
  $inactive10=$_POST['inactive10'];
  $inactive11=$_POST['inactive11'];
  $inactive12=$_POST['inactive12'];
  $inactive13=$_POST['inactive13'];
  $inactive14=$_POST['inactive14'];
  $inactive15=$_POST['inactive15'];
  $inactive16=$_POST['inactive16'];
  $inactive17=$_POST['inactive17'];
  $inactive18=$_POST['inactive18'];
  $inactive19=$_POST['inactive19'];
  $inactive20=$_POST['inactive20'];
   
  $t30day1=$_POST['t30day1'];
  $t30day2=$_POST['t30day2'];
  $t30day3=$_POST['t30day3'];
  $t30day4=$_POST['t30day4'];
  $t30day5=$_POST['t30day5'];
  $t30day6=$_POST['t30day6'];
  $t30day7=$_POST['t30day7'];
  $t30day8=$_POST['t30day8'];
  $t30day9=$_POST['t30day9'];
  $t30day10=$_POST['t30day10'];
  $t30day11=$_POST['t30day11'];
  $t30day12=$_POST['t30day12'];
  $t30day13=$_POST['t30day13'];
  $t30day14=$_POST['t30day14'];
  $t30day15=$_POST['t30day15'];
  $t30day16=$_POST['t30day16'];
  $t30day17=$_POST['t30day17'];
  $t30day18=$_POST['t30day18'];
  
    $qc_comment=mysql_real_escape_string($_POST['qc_comment']);
    
    mysql_select_db($database_air2013, $air2013);
    
    $c='1';
    mysql_query("INSERT INTO qc(q1,
    		q2,
    		q3,
    		q4,
    		q5,
    		q6,
    		q7,
    		q8,
    		welcome1,
    		welcome2,
    		welcome3,
    		welcome4,
    		welcome5,
    		welcome6,
    		welcome7,
    		welcome8,
    		welcome9,
    		welcome10,
    		welcome11,
    		welcome12,
    		welcome13,
    		welcome14,
    		welcome15,
    		welcome16,
    		welcome17,
    		inactive1,
    		inactive2,
    		inactive3,
    		inactive4,
    		inactive5,
    		inactive6,
    		inactive7,
    		inactive8,
    		inactive9,
    		inactive10,
    		inactive11,
    		inactive12,
    		inactive13,
    		inactive14,
    		inactive15,
    		inactive16,
    		inactive17,
inactive18,
inactive19,
inactive20,
    		30day1,
    		30day2,
    		30day3,
    		30day4,
    		30day5,
    		30day6,
    		30day7,
    		30day8,
    		30day9,
    		30day10,
    		30day11,
    		30day12,
    		30day13,
    		30day14,
    		30day15,
    		30day16,
    		30day17,
30day18,
    		qc_avg,
    		agent_name,
    		qc_name,
    		qc_date,
    		q_id,
    		s_date,
    		qc_comment
    		) VALUES('". $q1."',
    		'". $q2."',
    		'". $q3."',
    		'". $q4."',
    		'". $q5."',
    		'". $q6."',
    		'". $q7."',
    		'". $q8."',
    		'".$welcome1."',
    		'".$welcome2."',
    		'".$welcome3."',
    		'".$welcome4."',
    		'".$welcome5."',
    		'".$welcome6."',
    		'".$welcome7."',
    		'".$welcome8."',
    		'".$welcome9."',
    		'".$welcome10."',
    		'".$welcome11."',
    		'".$welcome12."',
    		'".$welcome13."',
    		'".$welcome14."',
    		'".$welcome15."',
    		'".$welcome16."',
    		'".$welcome17."',
    		'".$inactive1."',
    		'".$inactive2."',
    		'".$inactive3."',
    		'".$inactive4."',
    		'".$inactive5."',
    		'".$inactive6."',
    		'".$inactive7."',
    		'".$inactive8."',
    		'".$inactive9."',
    		'".$inactive10."',
    		'".$inactive11."',
    		'".$inactive12."',
    		'".$inactive13."',
    		'".$inactive14."',
    		'".$inactive15."',
    		'".$inactive16."',
    		'".$inactive17."',
'".$inactive18."',
'".$inactive19."',
'".$inactive20."',
    		
'".$t30day1."',
    		'".$t30day2."',
    		'".$t30day3."',
    		'".$t30day4."',
    		'".$t30day5."',
    		'".$t30day6."',
    		'".$t30day7."',
    		'".$t30day8."',
    		'".$t30day9."',
    		'".$t30day10."',
    		'".$t30day11."',
    		'".$t30day12."',
    		'".$t30day13."',
    		'".$t30day14."',
    		'".$t30day15."',
    		'".$t30day16."',
    		'".$t30day17."',
'".$t30day18."',
    		
    		'". $sum."',
    		'". $agent."',
    		'".$mqc."',
    		now(),
    		'". $s_id."',
    		'".$fromdate."',
    		'".$qc_comment."'
    		
    		) ");
  
    mysql_query("UPDATE survey SET `qc` = '". $c ."'  WHERE id = '". $s_id ."'");
    
    header("location:quality-report.php?fromdate=".$fromdate."");
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>QC - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>

<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">


	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2" nowrap="nowrap">
            
            </td>
      	</tr>
        <tr>
        	<td align="right">
           
			<table align="right">
    			<tr>
    			<td>AGENT NAME :</td>
    			<td><input type='textbox' readonly size='20' value="<?php echo ucwords($row_airtel_reports['interviewer']);?>" /></td>
                	<td>QA DATE :</td>
                	<td align="center"><input name='fromdate' type='textbox' class="dateselection" readonly size='20' value="<?php echo $fromdt;?>" /></td>
                    <td align="center">PHONE NO :</td>
                    <td align="center"><input type='textbox' readonly size='20' value="<?php echo ucwords($row_airtel_reports['dbphone']);?>" /></td>
                    <td align="center"></td>
                </tr>
			</table>
			
    		</td>
            
          
     	</tr>
  	</table>
</div>
<form name="form" method="POST">
<table><tr><td> </td></tr></table>
  <div class="content">
  
  <!-- Churn 30 days and More -->
  <?php if($_GET['disc']>=30)
  {?>
<fieldset style="margin-left:20%; width:60%">
<legend>Disconnected 30 and More Days </legend>

<?php include "incqa/disc30days.php";?>
</fieldset>
  
 
  
  
  <!-- End Less than 30 days -->
  
  <br></br>
   <?php } ?>
   
    <?php if($_GET['cs']==1)
  {?>
  <fieldset  style="margin-left:20%; width:60%">
  <legend>Inactive With Credit</legend>
  <?php include "incqa/inactive.php";?>
  </fieldset>
  
  
  <!-- Welcome Calls -->
  
  <br></br>
  <?php } ?>
  
   <?php if($_GET['cmp']=="Welcome")
  {?>
  <fieldset  style="margin-left:20%; width:60%">
  <legend>Welcome calls</legend>
  <?php include "incqa/welcome.php";?>
  </fieldset>
  
   <br></br>
   <?php } ?>
  <fieldset  style="margin-left:20%; width:60%">
  <legend>Overall Parameters</legend>
  <?php include "incqa/overall.php";?>
  </fieldset>
  
  
  
  
	
	</form>
  <!-- end .content --></div>
  <script>

  function updatesum() {
	 var q1=parseInt($("#q1").val());
	 var q2=parseInt($("#q2").val());
	 var q3=parseInt($("#q3").val());
	 var q4=parseInt($("#q4").val());
	 var q5=parseInt($("#q5").val());
	 var q6=parseInt($("#q6").val());
	 var q7=parseInt($("#q7").val());
	 var q8=parseInt($("#q8").val());

	  var total=(q1+q2+q3+q4+q5+q6+q7+q8)/8;

         document.form.sum.value =total;

	 
  }
  
  </script>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($air2013_reports);
?>
