<?php require_once('../Connections/air2013.php'); error_reporting(0);
ini_set("max_execution_time",0);

ini_set('max_execution_time',0);
ini_set('memory_limit','-1');

 ?>
<?php
if (!isset($_SESSION)) {
  session_start();
}
$MM_authorizedUsers = "Admin,Supervisor";
$MM_donotCheckaccess = "false";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable MM_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && false) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "restricted.php";
if (!((isset($_SESSION['MM_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_SESSION['MM_Username'], $_SESSION['MM_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_userDets = "-1";
if (isset($_SESSION['MM_Username'])) {
  $colname_userDets = $_SESSION['MM_Username'];
}
mysql_select_db($database_air2013, $air2013);
$query_userDets = sprintf("SELECT * FROM air_users WHERE username = %s", GetSQLValueString($colname_userDets, "text"));
$userDets = mysql_query($query_userDets, $air2013) or die(mysql_error());
$row_userDets = mysql_fetch_assoc($userDets);
$totalRows_userDets = mysql_num_rows($userDets);

$fromdt = $_GET["fromdate"];
$fromth = '00';
$fromtm = '00';
$fromcl = ':';
$fromts = ':00';
$fromsp = ' ';
$fromdate = $fromdt.$fromsp.$fromth.$fromcl.$fromtm.$fromts;
$todt = $_GET["todate"];
$toth = '23';
$totm = '59';
$tocl = ':';
$tots = ':59';
$tosp = ' ';
$todate = $todt.$tosp.$toth.$tocl.$totm.$tots;
$phone=$_GET['phone'];
$campaign=$_GET['campn'];

if(isset($_GET['mdays']) && !empty($_GET['mdays']))
	{
		$dy=$_GET['mdays'];
		if($dy==30)
			$mqr='AND leads.No_Days_Disconnected>0 AND leads.No_Days_Disconnected<='.$dy.'';
		
			if($dy==60)
				$mqr='AND leads.No_Days_Disconnected>30 AND leads.No_Days_Disconnected<='.$dy.'';
			
				if($dy==90)
					$mqr='AND leads.No_Days_Disconnected>60 AND leads.No_Days_Disconnected<='.$dy.'';
	}
	else 
	{
		$mqr='';
	} 
mysql_select_db($database_air2013, $air2013);
$query_airtel_reports = "SELECT survey.id as `mid`,survey.lid,survey.*,leads.*  FROM survey INNER JOIN leads ON survey.lid=leads.id WHERE update_time between '". $fromdate . "' AND '". $todate . "' AND disposation<>'' AND dbphone LIKE '%$phone%' AND cmpaign LIKE '%$campaign%' $mqr";
$air2013_reports = mysql_query($query_airtel_reports, $air2013) or die(mysql_error());  //
$row_airtel_reports = mysql_fetch_assoc($air2013_reports);
$totalRows_airtel_reports = mysql_num_rows($air2013_reports);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DSTV/GoTV - <?php include "../cat.php";?></title>
<link href="../css/admin.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationRadio.css" rel="stylesheet" type="text/css" />
<script src="../SpryAssets/SpryValidationRadio.js" type="text/javascript"></script>
<link type="text/css" href="../jquery/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/table2CSV.js" ></script>
<script type="text/javascript">
	$(function(){
		$('#fromdt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
		
		$('#todt').datepicker({
			dateFormat: 'yy-mm-dd',
            firstDay: 1,
			changeMonth: true,
            changeYear: true,
		});
	});
</script>
</head>

<body>
<div class="header">
	<table align="center" width="100%">
    	<tr>
            <td align="left" rowspan="2"><img src="../images/logo_right_small.png" width="250" height="60" alt="Millward Airtel" /></td>
          	<td align="right" valign="top" colspan="2" nowrap="nowrap">
            <form>
           	 
             <input type=button onClick="location.href='admin.php'" value='HOME'>
           	</form>
            </td>
      	</tr>
        <tr>
        	<td align="center">
           
    		</td>
            
            <td align="right">
           
            </td>
     	</tr>
  	</table>
</div>

  <div class="content">
	<table id="csvdownload" align="center">
	<tr>
	<td> <form id='frmsearchbar' action='csv/csv.php' method="GET">
			<table align="right">
    			<tr>
                	<td>From:</td>
                	<td align="center"><input name='fromdate' type='textbox' value="<?php echo $_GET['fromdate'];?>" required class="dateselection" id="fromdt" size='20' /></td>
                    <td align="center">To:</td>
                    <td align="center"><input name='todate' type='textbox' value="<?php echo $_GET['todate'];?>" required class="dateselection" id="todt" size='20'/></td>
					
					</tr>
					
					<tr>
                      <td align="center">Phone No:</td>
                    <td align="center"><input name='phone' type='textbox' class="dateselection" id="" size='20'/></td>
             
                     <td align="center">Campaign:</td>
                    <td align="center"><select name="campn"  style="width:100%">
					
					<option value="<?php echo $campaign;?>"><?php echo $campaign;?></option>
					<option value="GoTV">GoTV</option>
					<option value="DSTV">DSTV</option>
					<option value="Welcome">Welcome</option>
					<option value="">All</option>
					</select></td>
					</tr>
					<tr>
<td align="center">Days:</td>
                    <td align="center">
                    
                    <select name="mdays"  style="width:100%">
                     <option value="<?php echo $_GET['mdays'];?>"><?php echo $_GET['mdays'];?></option>
                    <option value="30">30 days</option>
                    <option value="60">60 Days</option>
                    <option value="90">90 Days</option>
					 <option value="">All</option>
                   
                    </select>
                    
                    </td>
                   <td align="center">Reconnected</td>
                    <td align="center">
                    
                    <select name="rcon"  style="width:100%">
                     <option value="<?php echo $_GET['rcon'];?>"><?php echo $_GET['rcon'];?></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
					 <option value="">All</option>
                    
                   
                    </select>
                    
                    </td>
                </tr>
				<tr>
<td align="center">Promise to Pay:</td>
                    <td align="center">
                    
                    <select name="promisetopay"  style="width:100%">
                     <option value="<?php echo $_GET['promisetopay'];?>"><?php echo $_GET['promisetopay'];?></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                   
					 <option value="">All</option>
                   
                    </select>
                    
                    </td>
                   <td align="center">Inactive with Credit</td>
                    <td align="center">
                    
                   <select name="inactive"  style="width:100%">
                     <option value="<?php echo $_GET['inactive'];?>"><?php echo $_GET['inactive'];?></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                   
					 <option value="">All</option>
                   
                    </select>
                    
                    </td>
                </tr>
				<tr><td colspan="4" align="right"></td></tr>
				<tr><td colspan="4" align="right"><input type="submit" name="submit" value="GET MULTICHOICE REPORTS" /></td></tr>
			</table>
			
			</form></td>
	</tr>
	<tr><td></td></tr>
	</table>
  <!-- end .content --></div>
</body>
</html>
<?php
mysql_free_result($userDets);
mysql_free_result($air2013_reports);
?>
